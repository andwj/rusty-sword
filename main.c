// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "main.h"

#include <time.h>


struct Options opt =
{
	false,  // fullscreen
	true,   // quiet
	0,      // seed
	NULL,   // data_dir
	NULL,   // config_dir
};

#define DEFAULT_DATA_DIR    "./data"
#define DEFAULT_CONFIG_DIR  "./config"


bool want_quit = false;

static FILE * log_file = NULL;


void Fatal_Error (const char * msg, ...)
{
	static char buffer[2000];

	va_list arg_ptr;

	va_start (arg_ptr, msg);
	vsnprintf (buffer, sizeof(buffer), msg, arg_ptr);
	va_end (arg_ptr);

	fprintf (stderr, "\nERROR: %s", buffer);

	B_Shutdown ();

	exit (9);
}

void Fatal_Assert (const char * cond, const char * file, int line)
{
	Fatal_Error ("assertion `%s` failed at %s:%d\n", cond, file, line);
}

void M_ShowHelp (void)
{
	printf ("USAGE: rusty-sword [-f | -w] [-s #] [-c DIR] [-d DIR] [-q | -n]\n");
	fflush (stdout);
	exit (0);
}

void M_ShowVersion (void)
{
	printf ("Rusty Sword " VERSION_STR "\n");
	fflush (stdout);
	exit (0);
}

void M_LogPrint (const char * fmt, ...)
{
	char buffer[4096];

	va_list arg_ptr;

	va_start (arg_ptr, fmt);
	vsnprintf (buffer, sizeof(buffer)-1, fmt, arg_ptr);
	va_end (arg_ptr);

	buffer[sizeof(buffer)-1] = 0;

	if (log_file)
	{
		fputs (buffer, log_file);
		fflush (log_file);
	}

	if (! opt.quiet)
	{
		fputs (buffer, stdout);
		fflush (stdout);
	}
}

void M_ParseArguments (int argc, char * argv[])
{
	// skip program itself
	argv++; argc--;

	while (argc > 0)
	{
		char *arg = *argv;

		argv++; argc--;

		// just ignore non-options
		if (arg[0] != '-')
			continue;

		// handle arguments which do not have any parameter

		if (UT_MatchArg (arg, 'h', "help"))
		{
			M_ShowHelp ();
			exit (0);
		}
		if (UT_MatchArg (arg, 'v', "version"))
		{
			M_ShowVersion ();
			exit (0);
		}
		if (UT_MatchArg (arg, 'f', "fullscreen"))
		{
			opt.fullscreen = true;
			continue;
		}
		if (UT_MatchArg (arg, 'w', "windowed"))
		{
			opt.fullscreen = false;
			continue;
		}
		if (UT_MatchArg (arg, 'q', "quiet"))
		{
			opt.quiet = true;
			continue;
		}
		if (UT_MatchArg (arg, 'n', "noisy"))
		{
			opt.quiet = false;
			continue;
		}

		// handle arguments which need a parameter

		char * parm = NULL;
		if (argc > 0)
		{
			if ((*argv)[0] != '-')
			{
				parm = *argv++; argc--;

				if (strlen (parm) == 0)
					parm = NULL;
			}
		}

		if (UT_MatchArg (arg, 's', "seed"))
		{
			if (parm == NULL)
			{
				fprintf (stderr, "missing value for %s option\n", arg);
				exit (1);
			}
			opt.seed = atoi (parm);
			if (opt.seed <= 0)
			{
				fprintf (stderr, "illegal seed value: %d\n", opt.seed);
				exit (1);
			}
			continue;
		}
		if (UT_MatchArg (arg, 'c', "config"))
		{
			if (parm == NULL)
			{
				fprintf (stderr, "missing value for %s option\n", arg);
				exit (1);
			}

			opt.config_dir = UT_Strdup (parm);
			continue;
		}
		if (UT_MatchArg (arg, 'd', "data"))
		{
			if (parm == NULL)
			{
				fprintf (stderr, "missing value for %s option\n", arg);
				exit (1);
			}

			opt.data_dir = UT_Strdup (parm);
			continue;
		}

		fprintf (stderr, "unknown option: %s\n", arg);
		exit (1);
	}
}

//
// determine the locations for our data and where we place user
// settings and savegames.
//
bool M_InitDirectories (void)
{
	char * dir;

	if (opt.data_dir == NULL)
	{
		dir = getenv ("RUSTYSWORD_DATA");
		if (dir != NULL && dir[0] != 0)
		{
			opt.data_dir = UT_Strdup (dir);
		}
		else
		{
#ifdef _WIN32
			// get the executable path
			static char path[8192];

			int length = GetModuleFileName (GetModuleHandle (NULL), path, sizeof(path)-4);

			if (length > 0 && length < (int)sizeof(path)-4)
			{
				if (access (path, 0) == 0)  // sanity check
				{
					dir = UT_GetDir (path);
					opt.data_dir = UT_JoinPath (dir, "data");
				}
			}
#endif
		}
	}

	if (opt.config_dir == NULL)
	{
		dir = getenv ("RUSTYSWORD_CONFIG");
		if (dir != NULL && dir[0] != 0)
		{
			opt.config_dir = UT_Strdup (dir);
		}
		else
		{
#ifdef _WIN32
			dir = getenv ("USERPROFILE");
			if (dir != NULL)
			{
				opt.config_dir = UT_JoinPath (dir, "rusty-sword");
			}
#else
#ifndef __DOS__
			dir = getenv ("HOME");
			if (dir != NULL)
			{
				dir = UT_JoinPath (dir, ".config");
				UT_MakeDir (dir);

				opt.config_dir = UT_JoinPath (dir, "rusty-sword");
			}
#endif
#endif
		}
	}

	if (opt.data_dir == NULL)
		opt.data_dir = DEFAULT_DATA_DIR;

	if (opt.config_dir == NULL)
		opt.config_dir = DEFAULT_CONFIG_DIR;

	// ensure the config directory exists
	UT_MakeDir (opt.config_dir);

	// WISH: test the config dir is writable

	return true;  // ok
}

void M_CloseLog (void)
{
	if (log_file != NULL)
	{
		fflush (log_file);
		fclose (log_file);
		log_file = NULL;
	}
}

bool M_OpenLog (void)
{
	char * path = UT_JoinPath (opt.config_dir, "log.txt");

	log_file = fopen (path, "w");

	if (log_file == NULL)
	{
		M_LogPrint ("cannot open log file (bad config dir?)\n");
		return false;
	}

	atexit (M_CloseLog);
	return true;
}

void M_Main (int argc, char * argv[])
{
	M_ParseArguments (argc, argv);

	if (opt.seed == 0)
		opt.seed = (int)time (NULL) & 0x7FFFFFFF;

	if (! M_InitDirectories ())
		exit (2);

	if (! M_OpenLog ())
		exit (3);

	M_LogPrint ("=======================\n");
	M_LogPrint ("=== Rusty Sword Log ===\n");
	M_LogPrint ("=======================\n");

	M_LogPrint ("game version: %s\n", VERSION_STR);
	M_LogPrint ("    data_dir: %s\n", opt.data_dir);
	M_LogPrint ("  config_dir: %s\n", opt.config_dir);

	if (! B_Init ())
		exit (4);

	if (! V_InitVideo ())
		exit (5);

	if (! B_InitInput ())
		exit (6);

	if (! V_LoadGraphics ())
		exit (7);

	M_LogPrint ("\n");

	V_Print ("Welcome to Rusty Sword!");
	V_Print ("Press 'M' for the main menu.");

	if (L_HaveSaveGame ())
	{
		if (L_RestoreGame ())
		{
			V_Print ("^bRestored the previous game.");

			if (world.state == GS_Defeat)
				V_Print ("^rYou are still dead.");

			G_RunGame ();
			return;
		}

		// world failed to load, so clear out any junk
		L_ClearWorld ();

		if (! D_ConfirmDialog (30, "Previous game failed to load!", "Begin a fresh game?"))
			return;

		// we don't delete it when user selected N to above question, as
		// they might want to investigate the file or try it in a different
		// version of the game.

		// if the user selected Y, it gets clobbered by L_SaveGame() below
	}
	else
	{
		// while we *could* show a dialog here (or a modified main menu),
		// I think it is simpler to just begin a fresh game.
	}

	V_Print ("^bStarted a fresh game.");

	C_CreateWorld (opt.seed);
	L_SaveGame ();

	G_RunGame ();

	// allegro does not need explicit shutdown
}
