// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "main.h"

//
//  OVERALL PLANNING
//

// a Region is a part of the map with a distinct theme.
// Regions form an interconnected tree of nodes, and the root
// of that tree is where the player will start the game.
// game-play decisions, like what armor and weapons to provide,
// are stored (and propagated) in regions.
struct Region
{
	int  id;

	// one of the theme values: OUT_Grass, BRICK_Brown, etc...
	int  theme;

	// planned size of the region (maximum, can be smaller).
	// with CONN_Surround, must be large enough to hold children.
	int  width;
	int  height;

	// number of connections.
	int  num_conn;

	// true when only reachable via levitation (directly or indirectly).
	// such areas cannot have captured friends.
	bool  isolated;

	// true when the target of a CONN_Surround conn.
	bool  surrounded;

	// the area assigned to hold this region.
	// two or more region groups may share the same area.
	int  area;
};

enum RegionTheme
{
	INVALID_THEME = 0,

	// outdoor themes

	OUT_Grass,
	OUT_Sand,
	OUT_Mud,

	// cave themes

	CAVE_Normal,

	// building themes

	BRICK_Grey,
	BRICK_Brown,
	BRICK_Blue,
	BRICK_Red,
	BRICK_White,
};

#define MAX_REGIONS  1024

static struct Region all_regions[MAX_REGIONS];
static int num_regions = 0;
static int root_region = 0;

// a Connection joins two regions.
struct Connection
{
	int  id;
	int  kind;

	// source and destination regions
	struct Region * src;
	struct Region * dest;

	// where to place destination in relation to source.
	// not used for CONN_Stair or CONN_Surround.
	int  dir;

	// either zero (unlocked) or OB_Key + N, or OB_Boulder for a pit.
	int  key;

	// use doors for access between the regions
	bool  door;
};

enum ConnectionKind
{
	INVALID_CONNECTION = 0,

	// player can simply walk between the two regions.
	// the actual connection could be floor tiles, doors (locked or
	// unlocked), even pits which can be filled with boulders.
	CONN_Walk,

	// staircase leading DOWN from source --> dest.
	CONN_Stair,

	// player must levitate over water or lava.
	CONN_Levitate,

	// player must fill some water with boulders to make a bridge.
	CONN_Bridge,

	// the source region surrounds the destination.
	// the destination must be a single region with no same-area conns.
	CONN_Surround,
};

static struct Connection all_conns[MAX_REGIONS];
static int num_conns = 0;


struct Region * C_NewRegion (void)
{
	Assert (num_regions < MAX_REGIONS);

	struct Region * R = &all_regions[num_regions];
	memset (R, 0, sizeof(*R));

	R->id = num_regions;

	num_regions += 1;
	return R;
}

struct Connection * C_NewConn (int kind)
{
	Assert (num_conns < MAX_REGIONS);

	struct Connection * C = &all_conns[num_conns];
	memset (C, 0, sizeof(*C));

	C->id   = num_conns;
	C->kind = kind;

	num_conns += 1;
	return C;
}

void C_InitPlan (void)
{
	num_regions = 0;
	num_conns   = 0;
}

int C_CreateRegion (void)
{
	struct Region * R = C_NewRegion ();

	// TODO

	return 100;
}

void C_Planning (void)
{
	// this is main code for creating a plan (of the world).
	// we create a tree of connected regions, pick the starting one,
	// etc etc....

	C_InitPlan ();

	// pick a target size, create regions until we reach it
	// (or run out of regions).

	int want_tiles = MAX_AREAS * (AREA_MAX_W * AREA_MAX_H / 8);
	int got_tiles  = 0;

	while (got_tiles < want_tiles && num_regions < MAX_REGIONS/2)
	{
		got_tiles += C_CreateRegion ();
	}
}

//----------------------------------------------------------------------

void C_CreatePlayer (int x, int y, int z)
{
	player.x = x;
	player.y = y;
	player.z = z;

	player.face   = +1;
	player.health = START_HEALTH;
	player.damage = START_DAMAGE;

	player.enter_x = x;
	player.enter_y = y;

	world.areas[z].grid[x][y].mon = MO_Player;
}

void C_CreateWorld (int seed)
{
	M_LogPrint ("Creating world with seed: %d\n", seed);

	L_ClearWorld ();

	world.seed  = seed;
	world.state = GS_None;
	world.total = 1;

	Rand_Begin (seed);

	C_Planning ();

	// this is temporary stuff, just for testing

	struct Area * A = &world.areas[0];

	A->width  = 20;
	A->height = 10;
	A->view_x = 0;
	A->view_y = 0;

	int x, y;

	for (x = 0 ; x < A->width  ; x++)
	for (y = 0 ; y < A->height ; y++)
	{
		struct Tile * T = &A->grid[x][y];

		memset (T, 0, sizeof(*T));

		T->env = EN_Floor | 4;  // grass

		if (x == 4 && y == 4)
		{
			C_CreatePlayer (x, y, 0);
			T->env = EN_Floor | 0;
		}

		if (x == 12 && y == 1)
		{
			T->env = EN_Down;
			T->id  = 0;
		}

		if (x == 6 && (y >= 2 && y <= 10))
			T->obj = OB_Boulder;

		if (x == 7 && y == 9)
			T->env = EN_Water;

		if (x == 13 && y == 1)
		{
			T->mon = MO_Skeleton;
			T->hp  = 1;
			world.enemies += 1;
		}
	}

	// let's kick this puppy into gear...
	world.state = GS_Active;

	L_RecomputePlayer ();
	V_UpdateViewPos ();
}
