// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "main.h"

static unsigned char TGA_Read8 (FILE * fp)
{
	int ch = fgetc (fp);
	if (ch == EOF)
		return 0;
	return (unsigned char)ch;
}

unsigned short TGA_Read16 (FILE * fp)
{
	unsigned short a = TGA_Read8 (fp);
	unsigned short b = TGA_Read8 (fp);

	return a | (b << 8);
}

static void TGA_LoadColormap (FILE * fp, int count, int bits,
                              pixel_t * mapping, int font_color)
{
	int c;
	for (c = 0 ; c < count ; c++)
	{
		// the order here is: Blue, Green, Red, Alpha
		int b = TGA_Read8 (fp);
		int g = TGA_Read8 (fp);
		int r = TGA_Read8 (fp);
		int a = 255;

		if (bits == 32)
			a = TGA_Read8 (fp);

		// pure magenta also represents transparency
		if (r >= 252 && g == 0 && b >= 252)
			r = g = b = a = 0;

		// map the color to our own palette
		if (a < 128)
			mapping[c] = TRANS_PIXEL;
		else
			mapping[c] = V_MapColor (r, g, b, font_color);
	}
}

//
// load a Truevision TGA (TARGA) image file into memory.
// throws a fatal error if anything goes wrong.
//
// NOTE: this code has been written from scratch, based on information
//       on the internet, and by looking at hex dumps of TGA files.
//
pixel_t * TGA_Load (FILE * fp, const char * base_name,
                    int * img_w, int * img_h, int font_color)
{
	// read and validate the header...

	// Note that there is no "magic" which uniquely distinguishes a
	// TGA image from other file formats.

	int id_length = TGA_Read8 (fp);
	int has_cmap  = TGA_Read8 (fp);
	int img_type  = TGA_Read8 (fp);

	if (id_length == 'P' && has_cmap == 'N')
		Fatal_Error ("image file seems to be PNG format: %s\n", base_name);

	if (id_length == 'G' && has_cmap == 'I')
		Fatal_Error ("image file seems to be GIF format: %s\n", base_name);

	if (id_length == 0xFF && has_cmap == 0xD8)
		Fatal_Error ("image file seems to be JPEG format: %s\n", base_name);

	if (! (has_cmap == 0 || has_cmap == 1))
		Fatal_Error ("image file is not a valid TGA: %s\n", base_name);

	switch (img_type)
	{
		case 1: case 2: case 9: case 10:
			// supported formats
			break;

		case 0: case 3: case 11:
			// unsupported formats (e.g. grayscale)
			Fatal_Error ("unsupported type %d TGA file: %s\n", img_type, base_name);
			break;

		default:
			// illegal value
			Fatal_Error ("image file is not a valid TGA: %s\n", base_name);
			break;
	}

	// read the colormap info...

	int cmap_start = TGA_Read16 (fp);
	int cmap_count = TGA_Read16 (fp);
	int cmap_bits  = TGA_Read8  (fp);

	// this is ignored
	(void) cmap_start;

	// read the image specification...

	// skip the origin X and Y
	TGA_Read16 (fp);
	TGA_Read16 (fp);

	int width  = TGA_Read16 (fp);
	int height = TGA_Read16 (fp);
	int bpp    = TGA_Read8  (fp);
	int flags  = TGA_Read8  (fp);

	if (feof (fp) || ferror (fp))
		Fatal_Error ("image file is not a valid TGA: %s\n", base_name);

	// validate the above values...

	if (width == 0 || width > 2048 || height == 0 || height > 2048)
		Fatal_Error ("invalid size (%dx%d) in TGA file: %s\n", width, height, base_name);

	if (has_cmap > 0)
	{
		if (cmap_count == 0 || cmap_count > 256)
			Fatal_Error ("invalid cmap size (%d) in TGA file: %s\n", cmap_count, base_name);

		if (! (cmap_bits == 24 || cmap_bits == 32))
			Fatal_Error ("invalid cmap bits (%d) in TGA file: %s\n", cmap_bits, base_name);

		if (! (bpp == 8))
			Fatal_Error ("unsupported bpp (%d indexed) in TGA file: %s\n", bpp, base_name);
	}
	else
	{
		if (! (bpp == 24 || bpp == 32))
			Fatal_Error ("unsupported bpp (%d RGB) in TGA file: %s\n", bpp, base_name);
	}

	// skip any ID information
	int k;
	for (k = 0 ; k < id_length ; k++)
		TGA_Read8 (fp);

	// create the new image
	pixel_t * pixels = UT_Alloc (width * height);

	// read the colormap (if any)...
	// we create a mapping to our own palette.
	//
	// [ Note that the mere existence of a transparent color in the
	//   colormap does not mark the image as masked, we require any
	//   such color to be actually used. ]

	pixel_t mapping[256];
	memset (mapping, 0, sizeof(mapping));

	if (has_cmap)
	{
		TGA_LoadColormap (fp, cmap_count, cmap_bits, mapping, font_color);

		if (feof (fp) || ferror (fp))
			Fatal_Error ("failed to read colormap in TGA file: %s\n", base_name);
	}

	// read the pixel data...
	// NOTE: we ignore any file error, or premature EOF.

	bool is_RLE     = (img_type >= 9);
	bool is_indexed = (img_type == 1 || img_type == 9);
	bool is_masked  = false;

	bool right_to_left = ((flags & 0x10) != 0);
	bool top_to_bottom = ((flags & 0x20) != 0);

	int x = 0;
	int y = 0;

	pixel_t       cur_pixel  = 0;
	unsigned char cur_run    = 0;
	unsigned char cur_packet = 0;

	for (;;)
	{
		if (cur_run > 0)
		{
			cur_run--;
		}
		else
		{
			if (cur_packet > 0)
			{
				cur_packet--;
			}
			else if (is_RLE)
			{
				unsigned char val = TGA_Read8 (fp);

				// we don't add 1 to the count, since we consume a pixel below
				unsigned char count = (val & 0x7F);

				if (val & 0x80)
					cur_run = count;
				else
					cur_packet = count;
			}

			/* read a fresh pixel */

			if (is_indexed)
			{
				cur_pixel = mapping[TGA_Read8 (fp)];

				if (cur_pixel == TRANS_PIXEL)
					is_masked = true;
			}
			else
			{
				// the order here is: Blue, Green, Red, Alpha
				int b = TGA_Read8 (fp);
				int g = TGA_Read8 (fp);
				int r = TGA_Read8 (fp);
				int a = 255;

				if (bpp == 32)
					a = TGA_Read8 (fp);

				// pure magenta also represents transparency
				if (r == 255 && g == 0 && b == 255)
					r = g = b = a = 0;

				if (a < 128)
				{
					cur_pixel = TRANS_PIXEL;
					is_masked = true;
				}
				else
				{
					cur_pixel = V_MapColor (r, g, b, font_color);
				}
			}
		}

		/* store the current pixel */

		int dx = right_to_left ? (width  - 1 - x) : x;
		int dy = top_to_bottom ? y : (height - 1 - y);

		pixels[dy * width + dx] = cur_pixel;

		x = x + 1;
		if (x >= width)
		{
			x = 0;
			y = y + 1;

			// the terminating condition
			if (y >= height)
				break;
		}
	}

	(void) is_masked;

	*img_w = width;
	*img_h = height;

	return pixels;
}
