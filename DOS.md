
Compiling the DOS Binary
========================

The method used here assumes a Unix-like system (Linux, BSD, MacOS X).
It requires the **wget** program to download some DJGPP packages, the
**unzip** program to unpack those packages, and **DosBox** to simulate
a DOS machine for actually compiling the code (useful for testing too).

Firstly, there is a unix shell script "setup_djgpp.sh" which creates a
a `_dosbox` directory, then downloads the necessary DJGPP packages and
unpacks them into it.  This script only needs to be run once.

```
   > sh scripts/setup_djgpp.sh
```

Secondly, there is a shell script "compile_dos.sh" which copies the
source code and DOS makefile into a `rusty` directory (in `_dosbox`)
and invokes DosBox to compile the code.  This can be run any number of
times, though it compiles every file from scratch each time.

```
   > sh scripts/compile_dos.sh
```

In the `scripts/` directory is a makefile for building with DJGPP, and
the config settings for DosBox ("Build.conf").  The makefile sets the
`__DOS__` define, and scattered through the code are #ifdefs which
check for this define when doing OS-specific stuff.

