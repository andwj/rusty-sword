
PLAYING GUIDE
=============

by Andrew Apted

Table of Contents:

1. [Introduction](#1-introduction)
2. [Commands](#2-commands)
3. [Dying and Restarting](#3-dying-and-restarting)
4. [Navigating](#4-navigating)
5. [The Screen](#5-the-screen)
6. [Options](#6-options)
7. [Item Catalog](#7-item-catalog)
8. [Monster Database](#8-monster-database)


1\. Introduction
----------------

The goal of this game is to visit all parts of the dungeon, vanguish all
the enemies, rescue all friends from their cages and then escape from the
dungeon together.  You cannot leave the dungeon until both of these main
objectives have been achieved.

One thing which sets this game apart from most other Rogue-like games
is a complete lack of randomness during game-play.  For example, the
player always hits a target monster with a certain (knowable) damage,
and likewise for monsters hitting the player.  Movement of monsters is
as predictable as possible, though it can become hard to predict when
there are multiple monsters in a group.  The only random aspect is the
initial generation of the world.


2\. Commands
------------

Commands are entered by pressing a key on the keyboard.  All commands
have a corresponding letter, such as the `M` key which brings up the
main menu.  Some commands can be performed with other keys too: you can
use the arrow keys to move around, the `SPACE` and `.` (dot) keys can
be used to wait a turn, and the `ESC` (escape) key also brings up the
main menu.  The `PGUP`, `PGDN` keys allow scrolling the message window.

### Command Summary

```
	Q: quaff potion   I: inventory
	R: read object    O: open door
	T: teleport       P: place skull

	A: zap acid       H: move left
	S: zap sleep      J: move down
	D: zap death      K: move up
	F: zap fire       L: move right

	Z: wait a turn    M: main menu
	C: climb stairs
```


3\. Dying and Restarting
------------------------

Rusty Sword is a *permadeath* game, which means that once the player
has died then the game is effectively over.  However, you can replay
the same game from the very beginning, or create a fresh game.

Quitting the game means that the current world is saved, and it will
be restored exactly as it was the next time you run Rusty Sword.  The
main menu also provides the `Discard` option, which causes the game to
quit without saving it.  A fresh game will be started on the next run.


4\. Navigating
--------------

The player can move around the environment using the arrow keys, or
the `HJKL` keys on the middle row of the keyboard, which have the same
meaning as the editor *vi* and the game *Nethack*, and are shown in
the command summary above.

### Liquids

Some area in the dungeon can be water or lava.  Players which attempt
to enter one of these will be instantly killed, except when they are
levitating (for water) or fire-resistant (for lava).

Monsters which can swim are able to pass through water areas, and
monsters which are fire-resistant are able to pass over lava.
Also, all flying enemies (like the dragons) are able to pass over
these terrain types.  For the rest, these tiles are impassible.

### Doors

Doors block movement when closed.  They can be opened by pressing the
`O` key (the open command) when the player is directly next to them.
Some doors are locked, in which case the player must find the correct
key for that door (elsewhere in the dungeon).  Closed rooms usually
hide their contents from view until a door is opened.

A room may have multiple doors -- opening one door will open all the
other ones which connect the same two rooms.  Once a door is open,
it cannot be closed again.  Only the player can open doors, enemies
never will.

Bars are a special kind of door are usually used for cages.  These
bars can be opened by the player without requiring any key, and the
creatures inside are friends waiting to be rescued.  Bars can also
be used as separators between two areas.

### Stairs

There are two types of stairs: one leading downward, and one leading
upward.  Pressing the `C` key (the climb command) is used for both.
The player must be standing on the stair.

There is always a single upward stair in each level of the dungeon.
For the very first level, that stair is used to escape the dungeon,
however that is only allowed once all friend creatures have been
rescued and all enemies have been vanquished.

Rescued friends will move onto an upward stair when adjacent to it,
and climb it when standing directly on it.  For the very first level,
this lets them escape the dungeon.  For all other levels, they will
appear in the above level sometime after the player has gone back
to it.  Friends never go down a stair to a lower level.

### Traps

There is one type of trap: the spiked pit.  If the player walks onto
it, they will be damaged by the spikes (except when levitating).
Flying enemies can pass over pits, but all other enemies will avoid
them.

Spiked pits are always visible -- there are no hidden traps in this
game!

### Boulders

Boulders may be pushed by the player by attempting to walk onto them.
A boulder can only be pushed when the tile in front of them is empty
(even an item will block them).  A boulder which is pushed into a water
tile will fill up the pond, turning it into solid ground.  Similarly
they can fill a spiked pit.  Boulders may be pushed down a stair, but
cannot be pushed *up* one.  Monsters never push boulders.

### Items

Scattered around the dungeon are various items which the player can
pick up.  Picking them up is just a matter of walking onto the tile
containing the item.

For armor and weapons, if the item is better then your current one,
it will replace it, otherwise it is discarded.  For wands, the number
of charges is increased by a fixed amount, up to the limit of 99.
Skulls have the same limit.  For small health potions, your health
(`Hp`) will be increased by a fixed amount, up to a limit of 999.
Other items are stored in your inventory, which can hold 8 items.

When an item, such as wands and potions, cannot be picked up because
the player is already at the maximum, then that item is discarded.
Keeping track of the limits, and ensuring good items are not squandered,
is an important aspect of game-play in Rusty Sword.

### Monsters

A dungeon is a hostile place, and contains many dangerous enemies!

Each time the player makes a move, including the wait command, all the
monsters have their turn, and will generally approach the player or
try to attack.  All enemies have a short-range (or *melee*) attack
which they can only do when directly adjacent to the player.  Some
enemies also have a long-range attack, like the red dragon which can
breath fire at the player from several tiles away.

Monsters also block the player from moving, except for friends and
neutral creatures like the mouse.  When the player attemps to move
onto an enemy, it will instead attack it with their current weapon,
or bare-handed when none.  For non-hostile creatures, the player will
simply swap position with the other creature.

Some monsters are guards, they will not move away from a particular
item or doorway (unless that object is picked up).

Monsters never fight or hurt each other.  That is your job!

No monster can enter a square containing an item.  This can be used
for strategic game-play, either by avoiding picking up an item until
a later time, or by the careful placement of skulls.


### Decorations

Around the dungeon are various decorative objects.  These all block
movement for the player and enemies alike.  Most of them are purely
decorative, like trees, mushrooms and pillars.  Some of them, like
books, sign-posts and tombstones, can be read by pressing the `R` key
(the read command), but whatever it says is only for your amusement,
it has no effect on the game.


5\. The Screen
--------------

The screen is divided into three main areas: the largest area is the
map window, devoted to the display of the player and their environment.
In the bottom left is the message box, which shows messages about what
is happening in the game world.  The bottom right area is the player's
statistics: their health, weapons, armor, wands and inventory.

### Map Window

The map window often only shows a portion of the current level.  There
is no way of seeing the other parts except by moving around.  Monsters
which are off-screen are still active and can move, but will not use
any long-range attack against the player.

### Message Box

The message box shows four lines of text.  It will show messages about
significant game events, including when the player or an enemy is hurt
or killed.  Damage messages show the amount of damage done as a negative
value in red.  Healing messages show the amount of health given as a
positive number in green.

In a single turn there may be multiple messages, especially when using
a wand on a group of monsters.  Messages which the player missed may be
viewed by pressing the `PGUP` key to see previous messages.  The `PGDN`
key goes forward, plus the `HOME` and `END` keys return to the bottom.

Important messages are color coded: red is used for messages where the
player has died, as well as attempted actions which are not currently
possible.  Light blue is used for system messages, like restoring a
previous game, or starting a fresh game.

Messages are not saved when you quit a game and come back to it later.

### Statistics

The statistics box shows all the information about the player.

The first column shows the player's health (`Hp`), the number of skulls
being carried, the amount of protection provided by their armor, and
the amount of damage which their weapon will inflict on most monsters.

The second column shows four icon boxes: three are for the player's armor,
where `Shld` is their shield and `Helm` is their helmet.  The last icon
box is their current weapon.  At the beginning of the game, the player
is empty-handed, having no armor or weapon.

The third column shows the amount of charges for each kind of wand
which the player can carry.

The last section shows eight icons for the player's inventory, things
which are not shown in any other of the columns.  These items include
keys, rings, amulets, potions and scrolls.


6\. Options
-----------

Stuff about options...

TODO


7\. Item Catalog
----------------

| Weapon                      | Name           | Damage |
| --------------------------- | -------------- | ------ |
| ![item_40](pic/item_40.png) | Rusty sword    |   10   |
| ![item_41](pic/item_41.png) | Sabre          |   15   |
| ![item_42](pic/item_42.png) | Broadsword     |   22   |
| ![item_43](pic/item_43.png) | Battle-axe     |   30   |


| Armor                       | Name           | Protect |
| --------------------------- | -------------- | ------- |
| ![item_10](pic/item_10.png) | Leather armor  |   10%   |
| ![item_11](pic/item_11.png) | Mithral armor  |   20%   |
| ![item_12](pic/item_12.png) | Bronze armor   |   32%   |
| ![item_13](pic/item_13.png) | Steel armor    |   45%   |


| Shield                      | Name           | Protect |
| --------------------------- | -------------- | ------- |
| ![item_30](pic/item_30.png) | Wooden shield  |    5%   |
| ![item_31](pic/item_31.png) | Studded shield |   10%   |
| ![item_32](pic/item_32.png) | Plated shield  |   15%   |
| ![item_33](pic/item_33.png) | Battle shield  |   20%   |


| Helmet                      | Name           | Protect |
| --------------------------- | -------------- | ------- |
| ![item_20](pic/item_20.png) | Rusty helmet   |    2%   |
| ![item_21](pic/item_21.png) | Iron helmet    |    4%   |
| ![item_22](pic/item_22.png) | Horned helmet  |    7%   |
| ![item_23](pic/item_23.png) | Steel helmet   |   10%   |


| Wand                        | Name  | Damage |
| --------------------------- | ----- | ------ |
| ![item_60](pic/item_60.png) | Acid  |   10   |
| ![item_61](pic/item_61.png) | Sleep |        |
| ![item_62](pic/item_62.png) | Death |  999   |
| ![item_63](pic/item_63.png) | Fire  |   20   |


| Key                         | Name       |
| --------------------------- | ---------- |
| ![item_50](pic/item_50.png) | Rusty key  |
| ![item_51](pic/item_51.png) | Ivory key  |
| ![item_52](pic/item_52.png) | Silver key |
| ![item_53](pic/item_53.png) | Gold key   |


| Potion                      | Name          | Healing |
| --------------------------- | ------------- | ------- |
| ![item_70](pic/item_70.png) | Small potion  |   +20   |
| ![item_71](pic/item_71.png) | Medium potion |   +100  |
| ![item_72](pic/item_72.png) | Large potion  |   +300  |
| ![item_73](pic/item_73.png) | Love Potion   |         |


| Ring                        | Name                    |
| --------------------------- | ----------------------- |
| ![item_80](pic/item_80.png) | Ring of Acid resistance |
| ![item_81](pic/item_81.png) | Ring of Fire resistance |


| Amulet                      | Name             |
| --------------------------- | ---------------- |
| ![item_82](pic/item_82.png) | Amulet of Flight |
| ![item_83](pic/item_83.png) | Amulet of Power  |


| Misc                          | Name            |
| ----------------------------- | --------------- |
| ![decor_E5](pic/decor_E5.png) | Teleport scroll |
| ![decor_E4](pic/decor_E4.png) | Scrap of paper  |
| ![decor_E7](pic/decor_E7.png) | Skull           |


8\. Monster Database
--------------------

**NOTE** : Health and damage values are not decided yet...

| Animal                    | Name         | Health | Damage  | Movement   | Immunity |
| ------------------------- | ------------ | ------ | ------- | ---------- | -------- |
| ![mon_22](pic/mon_22.png) | Bug          |   xx   |  5      | 4-way      |          |
| ![mon_23](pic/mon_23.png) | Centipede    |   xx   |  5      | Diagonal   |          |
| ![mon_11](pic/mon_11.png) | Spider       |   xx   |  5      | 8-way      |          |
| ![mon_14](pic/mon_14.png) | Scorpion     |   xx   |  5      | 8-way      |  Fire    |
| ![mon_10](pic/mon_10.png) | Bat          |   xx   |  5      | 4-way Fly  |          |
| ![mon_21](pic/mon_21.png) | Lizard       |   xx   |  5      | 4-way Swim |          |
| ![mon_24](pic/mon_24.png) | Toad         |   xx   |  5      | 4-way Swim |          |
| ![mon_12](pic/mon_12.png) | Python       |   xx   |  5      | Diagonal   |          |
| ![mon_13](pic/mon_13.png) | Anaconda     |   xx   |  5      | Diagonal   |          |
| ![mon_15](pic/mon_15.png) | Octopus      |   xx   |  5      | 8-way Swim |          |
| ![mon_16](pic/mon_16.png) | Mammoth      |   xx   |  5      | 4-way      |          |

| Mythical                  | Name         | Health | Damage  | Movement   | Immunity |
| ------------------------- | ------------ | ------ | ------- | ---------- | -------- |
| ![mon_20](pic/mon_20.png) | Slime        |   xx   |  5      | 4-way      |  Acid    |
| ![mon_17](pic/mon_17.png) | Wererabbit   |   xx   |  5      | 4-way      |  Sleep   |
| ![mon_24](pic/mon_34.png) | Werelion     |   xx   |  5      | 4-way      |  Sleep   |
| ![mon_25](pic/mon_25.png) | Green Dragon |   xx   |  5 / 7  | 8-way Fly  |          |
| ![mon_26](pic/mon_26.png) | Blue Dragon  |   xx   |  5 / 15 | 8-way Fly  |  Acid    |
| ![mon_27](pic/mon_27.png) | Red Dragon   |   xx   |  5 / 25 | 8-way Fly  |  Fire    |
| ![mon_25](pic/mon_45.png) | Oculus       |   xx   |  5      | 4-way Fly  |  Sleep   |

| Undead                    | Name         | Health | Damage  | Movement   | Immunity |
| ------------------------- | ------------ | ------ | ------- | ---------- | -------- |
| ![mon_20](pic/mon_30.png) | Zombie       |   xx   |  5      | 4-way      |  Death   |
| ![mon_21](pic/mon_31.png) | Skeleton     |   xx   |  5      | 4-way      |  Death   |
| ![mon_22](pic/mon_42.png) | Ghoul        |   xx   |  5      | 4-way Fly  |  ALL     |
| ![mon_23](pic/mon_43.png) | Vampire      |   xx   |  5      | 4-way      |  Death   |

| Humanoid                  | Name         | Hpalth | Damage  | Movement   | Immunity |
| ------------------------- | ------------ | ------ | ------- | ---------- | -------- |
| ![mon_22](pic/mon_32.png) | Goblin       |   xx   |  5      | 4-way      |          |
| ![mon_20](pic/mon_40.png) | Archer       |   xx   |  5 / 5  | 4-way      |          |
| ![mon_21](pic/mon_41.png) | Knight       |   xx   |  5 / 10 | 4-way      |  Acid    |
| ![mon_24](pic/mon_44.png) | Mage         |   xx   |  5 / 20 | 4-way Fly  |  Sleep   |
| ![mon_23](pic/mon_33.png) | Cyclops      |   xx   |  5      | 4-way      |          |
| ![mon_25](pic/mon_35.png) | Ice Golem    |   xx   |  5      | 4-way Swim |          |
| ![mon_26](pic/mon_36.png) | Red Devil    |   xx   |  5      | 4-way      |  Fire    |
| ![mon_27](pic/mon_37.png) | Black Devil  |   xx   |  5      | 4-way      |  ALL     |

