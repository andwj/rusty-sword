
AUTHORS
=======

CODING:
-------

```
Andrew Apted             : All current code    : MIT
```

ARTWORK:
--------

```
Doudoulolita + Redshrike : Sorlo character     : CC-BY 3.0
Jetrel                   : Large font          : CC0
XCVG + andrewj           : GUI elements        : CC0
Jerom + PriorBlue        : All other gfx       : CC-BY-SA 3.0
```

LICENSE INFO
------------

MIT : https://en.wikipedia.org/wiki/MIT_License

CC0 : https://creativecommons.org/publicdomain/zero/1.0

CC-BY : https://creativecommons.org/licenses/by-sa/3.0

CC-BY-SA : https://creativecommons.org/licenses/by/3.0

