// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "main.h"

#include "SDL.h"

//----------------------------------------------------------------------------
//  INPUT HANDLING
//----------------------------------------------------------------------------

static int TranslateKey (SDL_Keycode code)
{
	// digits
	if (SDLK_0 <= code && code <= SDLK_9)
		return KEY_0 + (code - SDLK_0);

	// letters
	if (SDLK_a <= code && code <= SDLK_z)
		return KEY_A + (code - SDLK_a);

	// special keys
	switch (code)
	{
		case SDLK_BACKSPACE:   return KEY_BS;
		case SDLK_RETURN:      return KEY_ENTER;
		case SDLK_TAB:         return KEY_TAB;
		case SDLK_ESCAPE:      return KEY_ESC;

		case SDLK_INSERT:      return KEY_INS;
		case SDLK_DELETE:      return KEY_DEL;
		case SDLK_HOME:        return KEY_HOME;
		case SDLK_END:         return KEY_END;
		case SDLK_PAGEUP:      return KEY_PGUP;
		case SDLK_PAGEDOWN:    return KEY_PGDN;

		case SDLK_UP:          return KEY_UP;
		case SDLK_DOWN:        return KEY_DOWN;
		case SDLK_RIGHT:       return KEY_RIGHT;
		case SDLK_LEFT:        return KEY_LEFT;

		case SDLK_PAUSE:       return KEY_PAUSE;
		case SDLK_PRINTSCREEN: return KEY_PRINT;
		case SDLK_MENU:        return KEY_MENU;
		case SDLK_HELP:        return KEY_HELP;

		case SDLK_F1:          return KEY_F1;
		case SDLK_F2:          return KEY_F2;
		case SDLK_F3:          return KEY_F3;
		case SDLK_F4:          return KEY_F4;
		case SDLK_F5:          return KEY_F5;
		case SDLK_F6:          return KEY_F6;
		case SDLK_F7:          return KEY_F7;
		case SDLK_F8:          return KEY_F8;
		case SDLK_F9:          return KEY_F9;
		case SDLK_F10:         return KEY_F10;
		case SDLK_F11:         return KEY_F11;
		case SDLK_F12:         return KEY_F12;
	}

	// numeric keypad 0..9 : mapped to their function
	switch (code)
	{
		case SDLK_KP_0:        return KEY_INS;
		case SDLK_KP_1:        return KEY_END;
		case SDLK_KP_2:        return KEY_DOWN;
		case SDLK_KP_3:        return KEY_PGDN;
		case SDLK_KP_4:        return KEY_LEFT;
		case SDLK_KP_5:        return KEY_SPACE;
		case SDLK_KP_6:        return KEY_RIGHT;
		case SDLK_KP_7:        return KEY_HOME;
		case SDLK_KP_8:        return KEY_UP;
		case SDLK_KP_9:        return KEY_PGUP;

		case SDLK_KP_ENTER:    return KEY_ENTER;
		case SDLK_KP_PERIOD:   return KEY_DOT;
		case SDLK_KP_PLUS:     return KEY_PLUS;
		case SDLK_KP_MINUS:    return KEY_MINUS;
		case SDLK_KP_MULTIPLY: return KEY_STAR;
		case SDLK_KP_DIVIDE:   return KEY_SLASH;
	}

	// ASCII symbols
	switch (code)
	{
		case SDLK_SPACE:       return KEY_SPACE;

		case '`':  case '~':   return KEY_TILDE;
		case '-':  case '_':   return KEY_MINUS;
		case '+':  case '=':   return KEY_PLUS;
		case '[':  case '{':   return KEY_OPENBRACE;
		case ']':  case '}':   return KEY_CLOSEBRACE;
		case '\\': case '|':   return KEY_BACKSLASH;
		case ';':  case ':':   return KEY_SEMICOLON;
		case '\'': case '"':   return KEY_QUOTE;
		case ',':  case '<':   return KEY_COMMA;
		case '.':  case '>':   return KEY_DOT;
		case '/':  case '?':   return KEY_SLASH;
		case '*':              return KEY_PLUS;
		case '#':              return KEY_TILDE;
	}

	// key is unknown or unsupported
	return NO_KEY;
}

static int I_KeyPress (const SDL_Event * ev)
{
	SDL_Keycode sym = ev->key.keysym.sym;
	SDL_Keymod  mod = ev->key.keysym.mod;

	(void) mod;

	return TranslateKey (sym);
}

bool B_InitInput (void)
{
//	SDL_ShowCursor (0);
	return true;
}

int B_InputKey (void)
{
	SDL_Event event;

	while (SDL_PollEvent (&event))
	{
		switch (event.type)
		{
			case SDL_KEYDOWN:
				return I_KeyPress (&event);

			case SDL_KEYUP:
				// ignore key release
				break;

			case SDL_QUIT:
				return EVENT_CLOSE_WINDOW;

			case SDL_WINDOWEVENT:
				switch (event.window.event)
				{
					case SDL_WINDOWEVENT_CLOSE:
						return EVENT_CLOSE_WINDOW;

					case SDL_WINDOWEVENT_HIDDEN:
						// TODO
						break;

					case SDL_WINDOWEVENT_SHOWN:
						// TODO
						break;

					case SDL_WINDOWEVENT_EXPOSED:
						// nothing needed I think
						break;

					case SDL_WINDOWEVENT_SIZE_CHANGED:
						// TODO
						break;

					case SDL_WINDOWEVENT_FOCUS_GAINED:
						// TODO
						break;

					case SDL_WINDOWEVENT_FOCUS_LOST:
						// TODO
						break;

					default: break;
				}
				break;

			default:
				// ignore everything else
				break;
		}
	}

	return NO_KEY;
}

void B_Delay (int milliseconds)
{
	SDL_Delay (milliseconds);
}

//----------------------------------------------------------------------------
//  GRAPHICS
//----------------------------------------------------------------------------

int UNSCALED_W = 1024;
int UNSCALED_H = 768;

static SDL_Window   * window;
static SDL_Renderer * renderer;

// pixel format of the window
static Uint32 win_format;

// the true size of the window (esp. for fullscreen)
static int win_width, win_height;

static SDL_Surface  * surf_pal;  // the main 8-bit block of pixels
static SDL_Surface  * surf_rgb;  // the RGB version
static SDL_Texture  * texture;   // texture updated from surf_rgb

// the current palette
static SDL_Color win_palette[256];

// the buffer we draw into
static pixel_t * win_buffer;

// current clipping rectangle
static int clip_x, clip_y, clip_w, clip_h;

// whether our window is shown vs hidden
static bool is_shown;

// whether our window has the keyboard focus
static bool has_focus;


void B_SetPalette (void)
{
	int c;
	for (c = 0 ; c < 256 ; c++)
	{
		int r = rusty_palette[c * 3 + 0];
		int g = rusty_palette[c * 3 + 1];
		int b = rusty_palette[c * 3 + 2];

		win_palette[c].r = r;
		win_palette[c].g = g;
		win_palette[c].b = b;
		win_palette[c].a = 255;
	}

	SDL_SetPaletteColors (surf_pal->format->palette, win_palette, 0, 256);
}

void B_Update (void)
{
	SDL_Rect  src_rect = { 0, 0, surf_pal->w, surf_pal->h };
	SDL_Rect dest_rect = { 0, 0, surf_rgb->w, surf_rgb->h };

	SDL_BlitSurface (surf_pal, &src_rect, surf_rgb, &dest_rect);

	SDL_UpdateTexture (texture, NULL, surf_rgb->pixels, surf_rgb->pitch);

	SDL_RenderCopy    (renderer, texture, NULL, NULL);
	SDL_RenderPresent (renderer);
}

//----------------------------------------------------------------------------

void B_CreateRenderer (void)
{
	Uint32 r_flags = 0;

	if (true) // TODO conf_software > 0)
		r_flags |= SDL_RENDERER_SOFTWARE;

	renderer = SDL_CreateRenderer (window, -1, r_flags);
	if (renderer == NULL)
		Fatal_Error ("cannot create renderer: %s\n", SDL_GetError ());
}

void B_CreateBuffers (void)
{
#if 0
	// determine whether we are upscaling or not
	win_upscale = 1;

	if (win_width >= 1920 && win_height >= 1080 && false)
		win_upscale = 2;
#endif
	int up_w = UNSCALED_W;
	int up_h = UNSCALED_H;

	surf_pal = SDL_CreateRGBSurface (0, up_w, up_h, 8 /* depth */, 0,0,0,0);
	if (surf_pal == NULL)
		Fatal_Error ("cannot create 8-bit surface: %s\n", SDL_GetError ());

	win_buffer = surf_pal->pixels;

	int    bpp;
	Uint32 Rmask, Gmask, Bmask, Amask;

	SDL_PixelFormatEnumToMasks (win_format, &bpp, &Rmask, &Gmask, &Bmask, &Amask);

	surf_rgb = SDL_CreateRGBSurface (0, up_w, up_h, 32, Rmask, Gmask, Bmask, Amask);
	if (surf_rgb == NULL)
		Fatal_Error ("cannot create RGB surface: %s\n", SDL_GetError ());

	texture = SDL_CreateTexture (renderer, win_format, SDL_TEXTUREACCESS_STREAMING, up_w, up_h);
	if (texture == NULL)
		Fatal_Error ("cannot create RGB texture: %s\n", SDL_GetError ());

//  TODO:
//     this only works without SDL_RENDERER_SOFTWARE.
//     plus should have options for Nearest vs Linear.
//
//	SDL_SetTextureScaleMode (texture, SDL_ScaleModeLinear);
}

void B_OpenWindow (void)
{
	Uint32 flags = 0;

	if (false) // FIXME  conf_fullscreen
	{
		flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		flags |= SDL_WINDOW_BORDERLESS;
	}

	// these are probably arbitrary in fullscreen mode
	win_width  = UNSCALED_W;
	win_height = UNSCALED_H;

	window = SDL_CreateWindow (NULL,
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		win_width, win_height, flags);

	if (window == NULL)
		Fatal_Error ("cannot create window: %s\n", SDL_GetError ());

	win_format = SDL_GetWindowPixelFormat (window);

	SDL_GetWindowSize  (window, &win_width, &win_height);
	SDL_SetWindowTitle (window, "3D Game");

	M_LogPrint ("Window size is: %dx%d\n", win_width, win_height);

	B_CreateRenderer ();
	B_CreateBuffers  ();

	is_shown  = 1;
	has_focus = 1;
}

void B_CloseWindow (void)
{
	if (texture != NULL)
	{
		SDL_DestroyTexture (texture);
		texture = NULL;
	}

	if (surf_rgb != NULL)
	{
		SDL_FreeSurface (surf_rgb);
		surf_rgb = NULL;
	}

	if (surf_pal != NULL)
	{
		SDL_FreeSurface (surf_pal);
		surf_pal = NULL;
	}

	if (renderer != NULL)
	{
		SDL_DestroyRenderer (renderer);
		renderer = NULL;
	}

	if (window != NULL)
	{
		SDL_DestroyWindow (window);
		window = NULL;
	}
}

bool B_InitVideo (void)
{
	M_LogPrint ("Video is: SDL2\n");

	B_ClipRect (0, 0, UNSCALED_W, UNSCALED_H);

	B_OpenWindow ();

	return true;
}

//----------------------------------------------------------------------------
//  IMAGES and DRAWING
//----------------------------------------------------------------------------

struct Image
{
	int  w, h;
	pixel_t * pixels;
};

struct SubImage
{
	int  x, y, w, h;
	struct Image * base;
};

struct Image * B_CreateImage (int w, int h)
{
	size_t pix_size = (size_t) (w * h);

	struct Image * img = UT_Alloc (sizeof(struct Image));

	img->w = w;
	img->h = h;

	img->pixels = UT_Alloc (pix_size);

	return img;
}

static bool B_Clip (struct Image * dest, int * x, int * y, int * w, int * h)
{
	if (dest == NULL)
	{
		if (*x < clip_x)
		{
			*w -= (clip_x - *x);
			*x  = clip_x;
		}

		if (*y < clip_y)
		{
			*h -= (clip_y - *y);
			*y  = clip_y;
		}

		if (*w > clip_x + clip_w - *x)
			*w = clip_x + clip_w - *x;

		if (*h > clip_y + clip_h - *y)
			*h = clip_y + clip_h - *y;
	}
	else
	{
		if (*x < 0) { *w += *x; *x = 0; }
		if (*y < 0) { *h += *y; *y = 0; }

		if (*w > dest->w - *x)
			*w = dest->w - *x;

		if (*h > dest->h - *y)
			*h = dest->h - *y;
	}

	return (*w > 0) && (*h > 0);
}

void B_Clear (struct Image * dest, int color)
{
	if (dest == NULL)
	{
		B_Fill (NULL, 0, 0, UNSCALED_W, UNSCALED_H, color);
		return;
	}

	pixel_t * pix = dest->pixels;
	size_t length = (size_t) (dest->w * dest->h);

	memset (pix, (pixel_t)color, length);
}

void B_Fill (struct Image * dest, int x, int y, int w, int h, int color)
{
	if (B_Clip (dest, &x, &y, &w, &h))
	{
		pixel_t * pix = (dest != NULL) ? dest->pixels : win_buffer;
		int stride    = (dest != NULL) ? dest->w : UNSCALED_W; 

		pix = pix + y * stride + x;

		for (; h > 0 ; h--, pix += stride) 
		{
			memset (pix, (pixel_t)color, (size_t)w);
		}
	}
}

void B_ImageSize (struct Image * img, int * w, int * h)
{
	*w = img->w;
	*h = img->h;
}

void B_MaskedRow (pixel_t * dest, const pixel_t * src, int count)
{
	for (; count > 0 ; count--, src++, dest++)
	{
		pixel_t p = *src;
		if (p != TRANS_PIXEL)
			*dest = p;
	}
}

void B_DrawImage (struct Image * img, int x, int y, bool masked)
{
	int w = img->w;
	int h = img->h;

	int sx = x;
	int sy = y;

	if (B_Clip (NULL, &x, &y, &w, &h))
	{
		// determine true starting pos
		sx = x - sx;
		sy = y - sy;

		const pixel_t * src  = img->pixels + (sy * img->w) + sx;
		      pixel_t * dest = win_buffer + (y * UNSCALED_W) + x;

		if (masked)
		{
			for (; h > 0 ; h--, src += img->w, dest += UNSCALED_W)
				B_MaskedRow (dest, src, w);
		}
		else
		{
			for (; h > 0 ; h--, src += img->w, dest += UNSCALED_W)
				memcpy (dest, src, (size_t)w);
		}
	}
}

struct SubImage * B_CreateSubImage (struct Image * base, int x, int y, int w, int h)
{
	struct SubImage * sub = UT_Alloc (sizeof(struct SubImage));

	sub->x = x;
	sub->y = y;
	sub->w = w;
	sub->h = h;
	sub->base = base;

	return sub;
}

void B_DrawSubImage (struct SubImage * sub, int x, int y, struct Image * dest)
{
	int w = sub->w;
	int h = sub->h;

	int sx = x;
	int sy = y;

	struct Image * base = sub->base;

	if (B_Clip (dest, &x, &y, &w, &h))
	{
		// determine starting point, take clipping into account
		sx = sub->x + (x - sx);
		sy = sub->y + (y - sy);

		int d_stride = (dest != NULL) ? dest->w : UNSCALED_W;

		const pixel_t * src = base->pixels + (sy * base->w) + sx;
		      pixel_t * out = ((dest != NULL) ? dest->pixels : win_buffer) + (y * d_stride) + x;

		for (; h > 0 ; h--, src += base->w, out += d_stride)
		{
			B_MaskedRow (out, src, w);
		}
	}
}

void B_ClipRect (int x, int y, int w, int h)
{
	Assert (x >= 0);
	Assert (y >= 0);

	Assert (w <= UNSCALED_W);
	Assert (h <= UNSCALED_H);

	clip_x = x;
	clip_y = y;
	clip_w = w;
	clip_h = h;
}

struct Image * B_LoadImage (const char * name, int font_color)
{
	char * fullname = UT_JoinPath (opt.data_dir, name);

	FILE * fp = fopen (fullname, "rb");
	if (fp == NULL)
	{
		M_LogPrint ("failed to load image: %s\n", name);
		return NULL;
	}

	struct Image * img = UT_Alloc (sizeof(struct Image));

	img->pixels = TGA_Load (fp, name, &img->w, &img->h, font_color);

	fclose (fp);

	if (img->pixels == NULL)
	{
//FIXME		UT_Free (img);
		return NULL;
	}

	return img;
}

//----------------------------------------------------------------------------
//  MAIN PROGRAM
//----------------------------------------------------------------------------

bool B_Init (void)
{
	if (SDL_Init (SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0)
	{
		M_LogPrint ("cannot initialize SDL: %s\n", SDL_GetError ());
		return false;
	}

	return true;
}

void B_Shutdown (void)
{
	// TODO
}

int main (int argc, char * argv[])
{
	M_Main (argc, argv);
	return 0;
}
