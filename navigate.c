// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "main.h"

#define NAV_BLOCKED   255
#define NAV_MAX_DIST  (NAV_BLOCKED - 1)

struct NavigationInfo
{
	int  width;
	int  height;

	// the distance map.
	// indexed by X, Y, and travel method.
	unsigned char  dist[AREA_MAX_W][AREA_MAX_H][4];

	// state used while creating the map : one bit per column
	unsigned int  dirty_cells[AREA_MAX_W];
};

static struct NavigationInfo  nav;


char NAV_CharForDist (unsigned char d)
{
	if (d == NAV_BLOCKED)
		return ' ';

	if (d == NAV_MAX_DIST)
		return '.';

	if (d == 0)
		return '@';

	if (d < 10)
		return '0' + d;

	d -= 10;

	if (d < 26)
		return 'a' + d;

	d -= 26;

	if (d < 26)
		return 'A' + d;

	return '#';
}

void NAV_Dump (void)
{
	printf ("\n");
	printf ("Navigation grid:\n");

	int x, y;

	for (y = 0 ; y < nav.height ; y++)
	{
		for (x = 0 ; x < nav.width ; x++)
		{
			fputc (NAV_CharForDist (nav.dist[x][y][0]), stdout);

			if (x > 78) break;
		}

		printf ("\n");
	}

	printf ("\n");
	fflush (stdout);
}

void NAV_MarkBlocking (int travel)
{
	struct Area * A = &world.areas[player.z];

	int x, y;

	for (y = 0 ; y < A->height ; y++)
	for (x = 0 ; x < A->width  ; x++)
	{
		struct Tile * T = &A->grid[x][y];

		if (L_BlocksTravel (T, travel))
		{
			nav.dist[x][y][travel] = NAV_BLOCKED;
		}
	}
}

void NAV_SetCell (int x, int y, int travel, unsigned char dist)
{
	nav.dist[x][y][travel] = dist;

	unsigned int mask = 1u << y;

	nav.dirty_cells[x] |= mask;
}

bool NAV_ExpandCell (int x, int y, int travel)
{
	unsigned char here = nav.dist[x][y][travel];

	if (here >= NAV_MAX_DIST)
		return false;

	bool changes = false;

	// only need to check the four cardinal directions
	int dir;
	for (dir = 2 ; dir <= 8 ; dir += 2)
	{
		int nx = x + Dir_dx (dir);
		int ny = y + Dir_dy (dir);

		if (! L_ValidSquare (nx, ny))
			continue;

		unsigned char there = nav.dist[nx][ny][travel];

		if (there == NAV_BLOCKED)
			continue;

		if (here + 1 < there)
		{
			NAV_SetCell (nx, ny, travel, here + 1);
			changes = true;
		}
	}

	return changes;
}

bool NAV_Expand (int travel)
{
	int changes = 0;

	// visit all the cells marked dirty, expand into neighbors
	int x, y;

	for (x = 0 ; x < nav.width ; x++)
	{
		unsigned int dirty = nav.dirty_cells[x];

		if (dirty == 0)
			continue;

		nav.dirty_cells[x] = 0;

		for (y = 0 ; y < nav.height ; y++)
		{
			if (0 != (dirty & (1u << y)))
				if (NAV_ExpandCell (x, y, travel))
					changes += 1;
		}
	}

	return (changes > 0);
}

void NAV_Build (void)
{
	// this is a current limitation
	Assert (AREA_MAX_H <= 32);

	struct Area * A = &world.areas[player.z];

	nav.width  = A->width;
	nav.height = A->height;

	// clear all cells to maximum distance
	memset (nav.dist, NAV_MAX_DIST, sizeof(nav.dist));

	int travel;
	for (travel = 0 ; travel < 4 ; travel++)
	{
		memset (nav.dirty_cells, 0, sizeof(nav.dirty_cells));

		NAV_MarkBlocking (travel);

		// at the player coordinate, distance is zero
		NAV_SetCell (player.x, player.y, travel, 0);

		while (NAV_Expand (travel))
		{ }
	}

/* DEBUG
	NAV_Dump ();
*/
}

int NAV_Query (enum TravelMethod travel, int x, int y)
{
	Assert (0 <= travel && travel < 4);

	if (x < 0 || x >= AREA_MAX_W) return 255;
	if (y < 0 || y >= AREA_MAX_H) return 255;

	unsigned char dist = nav.dist[x][y][travel];

	if (dist >= NAV_MAX_DIST)
		return -1;

	return (int)dist;
}

bool NAV_IsBlocked (enum TravelMethod travel, int x, int y)
{
	if (! L_ValidSquare (x, y))
		return true;

	return (nav.dist[x][y][travel] == NAV_BLOCKED);
}

bool NAV_IsChokePoint (enum TravelMethod travel, int x, int y)
{
	if (nav.dist[x][y][travel] >= NAV_MAX_DIST)
		return false;

	if (NAV_IsBlocked (travel, x, y-1) && NAV_IsBlocked (travel, x, y+1))
		return true;

	if (NAV_IsBlocked (travel, x-1, y) && NAV_IsBlocked (travel, x+1, y))
		return true;

	return false;
}

bool NAV_IsBadCorner (enum TravelMethod travel, int x, int y)
{
	// detects a corner which a diagonal mover would get stuck on

	if (x == player.x || y == player.y)
		return false;

	int dx = (x < player.x) ? +1 : -1;
	int dy = (y < player.y) ? +1 : -1;

	if (! NAV_IsBlocked (travel, x + dx, y + dy))
		return false;

	return true;
}

//----------------------------------------------------------------------

/*
   Dir represents an eight-way direction using a small integer:

           7 8 9
            \|/
          4--+--6
            /|\
           1 2 3

   Zero can be used to mean "no value".
*/

int Dir_reverse (int dir)
{
	return 10 - dir;
}

int Dir_dx (int dir)
{
	switch (dir)
	{
		case 1: case 4: case 7: return -1;
		case 3: case 6: case 9: return +1;
		default: return 0;
	}
}

int Dir_dy (int dir)
{
	switch (dir)
	{
		case 1: case 2: case 3: return +1;
		case 7: case 8: case 9: return -1;
		default: return 0;
	}
}

int ManhattanDist (int dx, int dy)
{
	dx = abs (dx);
	dy = abs (dy);

	return dx + dy;
}

int ChebyshevDist (int dx, int dy)
{
	dx = abs (dx);
	dy = abs (dy);

	return MAX (dx, dy);
}

int ApproxDist (int dx, int dy)
{
	dx = abs (dx);
	dy = abs (dy);

	return 2 * MAX (dx, dy) + MIN (dx, dy);
}
