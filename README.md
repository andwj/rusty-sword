
Rusty Sword
===========

by Andrew Apted


Introduction
------------

Rusty Sword is a graphical rogue-like game for casual play,
where each game would last about an hour.  Your goal is to
kill all the monsters, find the Amulet of Power and escape
the level with it.

The code to Rusty Sword is under a permissive MIT license.

The artwork is under various CC licenses, which permit both
re-distribution and modification.
See [AUTHORS.md](AUTHORS.md) for all the details.


Screenshot
----------

TODO


Compiling
---------

TODO


Status
------

The current version is `0.6.5`

Most of the gameplay is implemented, the player can attack monsters,
the monsters will try to reach and attack the player, all the items
can be picked up and used, etc...

Primary thing lacking at the moment is the level generation code.
This will be a big task, which is why the version number is still far
away from `1.0.0`.  I hope to make the levels be highly varied and
produce interesting encounters.

Lesser tasks to be done include:

-  improve various graphics, especially the wand beams
-  have an options screen
-  an option for fullscreen mode
-  get the stats right: player damage, monster hp, etc...


How to Play
-----------

Briefly:

-  Goal is to rescue all friends and vanquish all enemies
-  Use `HJKL` keys or arrow keys to move and fight
-  Use `ASDF` keys to zap wands
-  Press `M` key for the main menu

For more details, see the [Playing Guide](GUIDE.md).

