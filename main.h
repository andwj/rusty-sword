// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* libc includes */

#include <stdbool.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

/* version */

#define VERSION_STR  "0.6.5"
#define VERSION_NUM  (0*100 + 6*10 + 5)

/* world defs */

#include "world.h"

/* level */

void L_ClearWorld (void);
bool L_SaveGame (void);
bool L_HaveSaveGame (void);
bool L_RestoreGame (void);
void L_DeleteSaveGame (void);

bool L_ValidSquare (int x, int y);
bool L_EnvIsBlocking (unsigned char env);
bool L_ObjIsBlocking (unsigned char obj);
bool L_BlocksTravel (const struct Tile * T, enum TravelMethod travel);
int  L_ObjPriority (unsigned char obj);

const char * L_ObjName (unsigned char obj);
const char * L_MonName (unsigned char mon);
const char * L_WandShortName (int wand);

int  L_MonStartHealth (unsigned char mon);
int  L_MonMeleeDamage (unsigned char mon);

bool L_MonCanFly (unsigned char mon);
bool L_MonCanSwim (unsigned char mon);
bool L_MonCanVisit (const struct Tile * T, unsigned char mon);
bool L_MonIsNeutral (unsigned char mon);
bool L_MonImmuneTo (unsigned char mon, int wand);
bool L_MonHasLongRangeAttack (unsigned char mon);
bool L_MonBeamIsBlocked (int x, int y, int dx, int dy, int dist);

enum MovementMode  L_MonMoveMode     (unsigned char mon);
enum TravelMethod  L_MonTravelMethod (unsigned char mon);

void L_DirtyTile (int x, int y);
void L_RemoveMonster (struct Tile * T);
void L_RawMoveMonster (int old_x, int old_y, int new_x, int new_y);
void L_RawMovePlayer (int new_x, int new_y);
void L_OpenDoor (int x, int y, bool is_cage);
void L_MakeRoomVisible (int rx, int ry);
void L_FindMatchingStair (struct Area * A, unsigned char old_area, int * out_x, int * out_y);

bool L_PlayerHasObj (unsigned char obj, bool remove_it);
bool L_PlayerCanVisit (const struct Tile * T);
void L_PlayerEnterArea (unsigned char new_area);
void L_RecomputePlayer (void);
void L_AddFriendToQueue (struct Tile * T);

const char * L_PlayerAttackWord (void);
const char * L_WandAttackWord (int wand);
const char * L_MonAttackWord (unsigned char mon);

/* navigate */

void NAV_Build (void);
int  NAV_Query (enum TravelMethod travel, int x, int y);

bool NAV_IsBlocked    (enum TravelMethod travel, int x, int y);
bool NAV_IsChokePoint (enum TravelMethod travel, int x, int y);
bool NAV_IsBadCorner  (enum TravelMethod travel, int x, int y);

int Dir_reverse (int dir);
int Dir_dx (int dir);
int Dir_dy (int dir);

int ManhattanDist (int dx, int dy);
int ChebyshevDist (int dx, int dy);
int ApproxDist    (int dx, int dy);

/* create */

void C_CreateWorld (int seed);

/* dialog */

char  D_MainMenuDialog (void);
bool  D_ConfirmDialog (int width, const char * msg1, const char * msg2);
void  D_InventoryDialog (void);
void  D_VictoryDialog (void);

char  D_HelpMenuDialog (void);
void  D_HelpGeneralInfo (void);
void  D_HelpShowKeys (void);
void  D_HelpCredits (void);

/* game */

void G_RunGame (void);

/* keys */

#include "keys.h"

/* palette */

// a raw 8-bit pixel
typedef unsigned char pixel_t;

#define TRANS_PIXEL  0
#define WHITE_COLOR  1
#define BLACK_COLOR  16

extern const int rusty_palette[256 * 3];

int PAL_FindColor (int r, int g, int b);

/* tga loader */

pixel_t * TGA_Load (FILE * fp, const char * base_name,
                    int * img_w, int * img_h, int font_color);

/* video */

bool V_InitVideo (void);
bool V_LoadGraphics (void);

void V_Refresh (bool idle);

void V_MarkAllDirty (void);
void V_MarkDirty_World (void);
void V_MarkDirty_Stats (void);
void V_MarkDirty_Dialog (void);

void V_AddEffect (int x, int y, int wand, int offset);
void V_RemoveEffect (int x, int y);
void V_ClearEffects (void);

void V_Print (const char * fmt, ...);
void V_ScrollMessages (int dy);
void V_ClearMessages (void);

void V_OpenDialog (int width, int height);
void V_CloseDialog (void);
void V_DialogText (int x, int y, const char * fmt, ...);
void V_DialogImage (int dx, int dy);

void V_UpdateViewPos (void);
bool V_TileOnScreen (int x, int y);
int  V_MapColor (int r, int g, int b, int font_color);

/* back-end (graphic and input) */

#include "back_end.h"

/* util */

#ifndef MAX
#define MAX(a,b)  ((a) > (b) ? (a) : (b))
#endif

#ifndef MIN
#define MIN(a,b)  ((a) < (b) ? (a) : (b))
#endif

void * UT_Alloc   (size_t size);
char * UT_Strdup  (const char * s);
int    UT_Compare (const char * A, const char * B);
bool   UT_MatchArg (const char * arg, char shortname, const char * longname);

bool   UT_MakeDir   (const char * name);
bool   UT_DeleteFile (const char * name);
char * UT_JoinPath  (const char * dir, const char * rest);
char * UT_GetDir    (const char * path);

void  Rand_Begin (int seed);
int   Rand_Value (int modulo);
int   Rand_Range (int low, int high);
bool  Rand_Odds  (int percent);

/* assertions */

#define Assert(cond)  \
	do { if (! (cond)) Fatal_Assert (#cond, __FILE__, __LINE__); } while (0)

/* main */

struct Options
{
	// whether to open in fullscreen mode or windowed mode.
	bool  fullscreen;

	// if true, disable printing log messages to stdout.
	bool  quiet;

	// a custom seed value, normally zero.
	int  seed;

	// directory containing the tilesets (etc).
	const char * data_dir;

	// directory for storing options and the savegame.
	const char * config_dir;
};

extern struct Options opt;

extern bool want_quit;

void M_Main (int argc, char * argv[]);
void M_LogPrint (const char * fmt, ...);

void Fatal_Error  (const char * msg, ...);
void Fatal_Assert (const char * cond, const char * file, int line);
