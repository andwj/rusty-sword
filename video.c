// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "main.h"


#define FONT_STEP  15

#define ENV_ROW   0
#define MON_ROW   5
#define OBJ_ROW  10
#define  FX_ROW  18


// how long (in milliseconds) to wait when no input is ready
#define IDLE_DELAY  50


// number of lines visible on screen
#define MSG_LINES  4

#define MAX_HISTORY   100
#define MAX_LINE_LEN  64

struct MessageList
{
	// the oldest line in the buffer.
	// when buffer is full, it wraps around the end.
	int last;

	// number of used lines
	int used;

	// > 0 when scrolling back
	int scrolled;

	// the actual lines.
	// each one is NUL-terminated.
	char lines[MAX_HISTORY][MAX_LINE_LEN];
};

static struct MessageList  message_list;


#define MAX_DIALOG_W  64
#define MAX_DIALOG_H  20

struct DialogWindow
{
	// size of the dialog (in characters).
	// only lines[0 .. height-1] will be displayed.
	// lines may be longer than width due to escape sequences.
	int width;
	int height;

	// position (top-left) of the first character.
	// a frame is drawn around the text area.
	int x;
	int y;

	// position of the top-left GUI tile.
	int tile_x;
	int tile_y;

	// number of 32x32 GUI tiles used to hold the characters.
	int tile_w;
	int tile_h;

	// draw an image (the trophy) if > 0
	int image;
	int image_dx;
	int image_dy;

	// a string for each line, NUL-terminated.
	char lines[MAX_DIALOG_H][MAX_DIALOG_W];
};

static struct DialogWindow  dialog_win;


// height of the HUD area
static int HUD_H;

// Y coordinate for top of HUD area (includes the separator)
static int HUD_Y;

// width of the stats area
static int STAT_W;

// X coordinate for left of statistics (includes the separator)
static int STAT_X;

// number of world tiles visible on-screen
#define VIS_TILES_X  16
#define VIS_TILES_Y  13

// off-screen images for the HUD (message box and stats).
// this is needed to prevent flicker when they are redrawn.
static struct Image * message_box;
static struct Image * stat_box;

// the trophy image
static struct Image * trophy_image;


enum FontColor
{
	F_White  = 0,  // ^w
	F_Gray   = 1,  // ^d   (d for dim)

	F_Red    = 2,  // ^r
	F_Green  = 3,  // ^g
	F_Blue   = 4,  // ^b

	F_Brown  = 5,  // ^m   (m for muddy)
	F_Orange = 6,  // ^o
	F_Yellow = 7,  // ^y
};


#define MAX_TILES  256

struct Tileset
{
	int cols;    // tiles across
	int rows;    // tiles down

	int width;   // tile size
	int height;  //

	struct SubImage * sprites[MAX_TILES];
};


static struct Tileset gui_tiles;
static struct Tileset world_tiles;

static struct Tileset fonts[8];

// is a dialog window active?
static bool dialog_active = false;


#define MAX_EFFECTS  (BEAM_DIST * 2)

struct EffectInfo
{
	// map coordinate
	int x, y;

	// tile coordinate, zero means free
	int tcoord;
};

static struct EffectInfo  effects[MAX_EFFECTS];


// which parts of the screen need a *full* redraw.
// [ map changes tend to use the FL_DIRTY flag instead ]
struct DirtyFlags
{
	bool world;
	bool messages;
	bool stats;
	bool dialog;
};

static struct DirtyFlags  dirty;


// set when switching back
volatile bool v_switched = false;

// set when user presses close button on the window
volatile bool v_want_close = false;


void V_MarkAllDirty (void)
{
	dirty.world    = true;
	dirty.messages = true;
	dirty.stats    = true;
	dirty.dialog   = true;
}

void V_MarkDirty_World (void)
{
	dirty.world = true;
}

void V_MarkDirty_Stats (void)
{
	dirty.stats = true;
}

void V_MarkDirty_Dialog (void)
{
	dirty.dialog = true;
}

//----------------------------------------------------------------------

bool V_InitVideo (void)
{
	if (! B_InitVideo ())
		return false;

	B_SetPalette ();

	HUD_H  = 4 * 32 + 16;
	HUD_Y  = UNSCALED_H - HUD_H;

	STAT_X = 522;
	STAT_W = UNSCALED_W - STAT_X;

	// create the off-screen HUD bitmaps
	message_box = B_CreateImage (STAT_X, HUD_H);
	stat_box    = B_CreateImage (STAT_W, HUD_H);

	if (message_box == NULL || stat_box == NULL)
	{
		M_LogPrint ("failed to allocate a bitmap.\n");
		return false;
	}

	V_MarkAllDirty ();

	return true;
}


int V_MapColor (int r, int g, int b, int font_color)
{
	// only check the upper 6 bits
	r = r >> 2;
	g = g >> 2;
	b = b >> 2;

	// magenta always becomes #0 (transparency).
	if (r == 63 && g == 0 && b == 63)
		return TRANS_PIXEL;

	// for fonts, everything else becomes the wanted color
	if (font_color > 0)
		return (unsigned char)font_color;

	// black always becomes #16
	if (r == 0 && g == 0 && b == 0)
		return BLACK_COLOR;

	// white always becomes #1
	if (r == 63 && g == 63 && b == 63)
		return WHITE_COLOR;

	// pure greyscale is mapped to the ramp
	if (abs (r - g) < 2 && abs (g - b) < 2 && abs (b - r) < 2)
		return 16 - (g + 8) / 17;

	// everything else requires a slow lookup
	return PAL_FindColor (r << 2, g << 2, b << 2);
}

bool V_LoadTiles (struct Tileset * T, const char * name, int width, int height, int font_color)
{
	// `width` and `height` are the size of each tile.

	// the `font_color` parameter should normally by -1, but when loading a font
	// it can be a valid color (a palette index) to colorize the font.

	T->width  = width;
	T->height = height;

	memset (T->sprites, 0, sizeof (T->sprites));

	struct Image * img = B_LoadImage (name, font_color);
	if (img == NULL)
		return false;

	// determine number of rows and columns
	int img_w, img_h;
	B_ImageSize (img, &img_w, &img_h);

	T->cols = img_w / width;
	T->rows = img_h / height;

	if (T->cols < 1 || T->rows < 1 || T->cols * T->rows > MAX_TILES)
	{
		M_LogPrint ("image has wrong size: %s\n", name);
		return false;
	}

	// extract each tile, create an RLE sprite....

	int x, y;

	for (y = 0 ; y < T->rows ; y++)
	for (x = 0 ; x < T->cols ; x++)
	{
		struct SubImage * sprite = B_CreateSubImage (img, x * width, y * height, width, height);

		if (sprite == NULL)
		{
			M_LogPrint ("failed to create sub-image.\n");
			return false;
		}

		T->sprites[y * T->cols + x] = sprite;
	}

	return true;
}

bool V_LoadGraphics (void)
{
	if (! V_LoadTiles (&gui_tiles, "gui.tga", 32, 32, -1))
		return false;

	if (! V_LoadTiles (&world_tiles, "tiles.tga", 64, 64, -1))
		return false;

	if (! V_LoadTiles (&fonts[0], "font.tga", 20, 32, PAL_FindColor (204, 204, 204)))
		return false;

	if (! V_LoadTiles (&fonts[1], "font.tga", 20, 32, PAL_FindColor (136, 136, 136)))
		return false;

	if (! V_LoadTiles (&fonts[2], "font.tga", 20, 32, PAL_FindColor (255,   0,   0)))
		return false;

	if (! V_LoadTiles (&fonts[3], "font.tga", 20, 32, PAL_FindColor (  0, 224,   0)))
		return false;

	if (! V_LoadTiles (&fonts[4], "font.tga", 20, 32, PAL_FindColor ( 80, 160, 255)))
		return false;

	if (! V_LoadTiles (&fonts[5], "font.tga", 20, 32, PAL_FindColor (184, 140,  96)))
		return false;

	if (! V_LoadTiles (&fonts[6], "font.tga", 20, 32, PAL_FindColor (240, 180, 128)))
		return false;

	if (! V_LoadTiles (&fonts[7], "font.tga", 20, 32, PAL_FindColor (224, 224,   0)))
		return false;

	trophy_image = B_LoadImage ("trophy.tga", -1);
	if (trophy_image == NULL)
		return false;

	return true;
}

//----------------------------------------------------------------------

#define MAKE_TCOORD(r, c)  (((r) << 8) | (c))

void V_DrawTile (struct Image * dest, int x, int y, const struct Tileset * T, int tcoord)
{
	int row = tcoord >> 8;
	int col = tcoord & 255;

	Assert (col >= 0 && col < T->cols);
	Assert (row >= 0 && row < T->rows);

	struct SubImage * sprite = T->sprites[row * T->cols + col];
	Assert (sprite != NULL);

	B_DrawSubImage (sprite, x, y, dest);
}

int V_CalcEnv (unsigned char env)
{
	int row = ENV_ROW + (env >> 4) - 1;
	int col = (env & 15);

	return MAKE_TCOORD (row, col);
}

int V_CalcMon (unsigned char mon)
{
	if (mon == MO_Player)
	{
		int col = 0;

		if (player.health == 0)
			col = 4;
		else if (player.zapping)
			col = 2;

		if (player.face < 0)
			col += 1;

		return MAKE_TCOORD (MON_ROW, col);
	}

	int row = MON_ROW + (mon >> 4);
	int col = mon & 15;

	return MAKE_TCOORD (row, col);
}

int V_CalcObj (unsigned char obj)
{
	if (obj >= 0x90)
		obj -= 0x80;

	int row = OBJ_ROW + (obj >> 4) - 1;
	int col = obj & 15;

	return MAKE_TCOORD (row, col);
}

int V_CalcBadge (unsigned char flag, unsigned char time)
{
	if (time > 0)
	{
		int col = 6;

		if (time > SLEEP_TIME*1/3) col--;
		if (time > SLEEP_TIME*2/3) col--;

		return MAKE_TCOORD (FX_ROW + 1, col);
	}

	if (flag & FL_FRIEND)
	{
		return MAKE_TCOORD (FX_ROW + 1, 7);
	}

	// none
	return -1;
}

void V_RenderMapTile (int ax, int ay, int x, int y, bool do_fx)
{
	struct Area * A = &world.areas[player.z];

	// screen tile is outside of the area?
	if (! L_ValidSquare (ax, ay))
	{
		B_Fill (NULL, x, y+16, 64, 48, BLACK_COLOR);
		return;
	}

	const struct Tile * T = &A->grid[ax][ay];

	if ((T->flag & FL_UNSEEN) != 0)
	{
		B_Fill (NULL, x, y+16, 64, 48, BLACK_COLOR);
		return;
	}

	// draw the background
	if (T->env != 0)
	{
		V_DrawTile (NULL, x, y, &world_tiles, V_CalcEnv (T->env));
	}
	else
	{
		B_Fill (NULL, x, y+16, 64, 48, BLACK_COLOR);
	}

	// draw the foreground (monster or object).
	// [ when player has won, don't draw them or their friends ]
	if (T->mon != 0 && (world.state != GS_Victory))
	{
		V_DrawTile (NULL, x, y, &world_tiles, V_CalcMon (T->mon));

		int badge = V_CalcBadge (T->flag, T->time);

		if (badge > 0)
			V_DrawTile (NULL, x, y, &world_tiles, badge);
	}
	else if (T->obj != 0)
	{
		V_DrawTile (NULL, x, y, &world_tiles, V_CalcObj (T->obj));
	}

	if (do_fx)
	{
		int i;
		for (i = 0 ; i < MAX_EFFECTS ; i++)
		{
			if (effects[i].tcoord != 0 &&
				effects[i].x == ax &&
				effects[i].y == ay)
			{
				V_DrawTile (NULL, x, y, &world_tiles, effects[i].tcoord);
			}
		}
	}
}

void V_RenderClippedTile (int x, int y)
{
	struct Area * A = &world.areas[player.z];

	int ax = A->view_x + x;
	int ay = A->view_y + y;

	x = x * 64;
	y = y * 48 - 16;

	bool is_dirty = false;

	if (L_ValidSquare (ax, ay))
	{
		// check and clear the tile's dirty flag
		struct Tile * T = &A->grid[ax][ay];

		if ((T->flag & FL_DIRTY) != 0)
		{
			is_dirty = true;
			T->flag &= ~FL_DIRTY;
		}
	}

	if (dirty.world)
	{
		V_RenderMapTile (ax, ay, x, y, true);
		return;
	}

	// check whether the below tile is dirty, since monsters
	// and objects often extend into the current tile.
	bool below_dirty = false;

	if (L_ValidSquare (ax, ay + 1))
	{
		if ((A->grid[ax][ay + 1].flag & FL_DIRTY) != 0)
			below_dirty = true;
	}

	if (! (is_dirty || below_dirty))
		return;

	// set the clipping rect to *just* this tile
	B_ClipRect (x, y+16, 64, 48);

	V_RenderMapTile (ax, ay,   x, y,    true);
	V_RenderMapTile (ax, ay+1, x, y+48, false);
}

void V_RenderWorld (void)
{
	int x, y;

	for (y = 0 ; y < VIS_TILES_Y ; y++)
	for (x = 0 ; x < VIS_TILES_X ; x++)
	{
		V_RenderClippedTile (x, y);
	}

	B_ClipRect (0, 0, UNSCALED_W, UNSCALED_H);

	dirty.world = false;
}

bool V_TileOnScreen (int x, int y)
{
	struct Area * A = &world.areas[player.z];

	if (x < A->view_x) return false;
	if (y < A->view_y) return false;

	x = (x - A->view_x) * 64 + 32;
	y = (y - A->view_y) * 48 + 24;

	if (x >= UNSCALED_W) return false;
	if (y >= HUD_Y)    return false;

	return true;
}

void V_AddEffect (int x, int y, int wand, int offset)
{
	// look for existing effect at same coordinate
	int slot = -1;

	int i;
	for (i = 0 ; i < MAX_EFFECTS ; i++)
	{
		if (effects[i].x == x && effects[i].y == y)
		{
			slot = i;
			break;
		}

		if (effects[i].tcoord == 0)
			slot = i;
	}

	if (slot >= 0)
	{
		int tcoord = 0;

		if (wand <= WAND_Fire)
		{
			// wand beams
			int col = wand * 2 + ((offset - 1) & 1);
			tcoord = MAKE_TCOORD (18, col);
		}
		else
		{
			// projectiles
			tcoord = MAKE_TCOORD (19, offset);
		}

		effects[slot].x = x;
		effects[slot].y = y;
		effects[slot].tcoord = tcoord;

		L_DirtyTile (x, y);
	}
}

void V_RemoveEffect (int x, int y)
{
	int i;
	for (i = 0 ; i < MAX_EFFECTS ; i++)
	{
		if (effects[i].x == x && effects[i].y == y)
		{
			effects[i].tcoord = 0;
			L_DirtyTile (x, y);
			return;
		}
	}
}

void V_ClearEffects (void)
{
	int i;
	for (i = 0 ; i < MAX_EFFECTS ; i++)
	{
		if (effects[i].tcoord != 0)
		{
			effects[i].tcoord = 0;
			L_DirtyTile (effects[i].x, effects[i].y);
		}
	}
}

void V_UpdateViewPos (void)
{
	struct Area * A = &world.areas[player.z];

	int VISIBLE_X = UNSCALED_W / 64;
	int VISIBLE_Y = HUD_Y    / 48;

	int new_x = player.x - VISIBLE_X/2;
	int new_y = player.y - VISIBLE_Y/2;

	if (A->width <= VISIBLE_X)
	{
		// area is no wider than screen
		new_x = (A->width - VISIBLE_X) / 2;
	}
	else
	{
		if (new_x < 0)
			new_x = 0;

		if (new_x > A->width - VISIBLE_X)
			new_x = A->width - VISIBLE_X;
	}

	if (A->height <= VISIBLE_Y)
	{
		// area is no higher than the screen
		new_y = (A->height - 1 - VISIBLE_Y) / 2;
	}
	else
	{
		if (new_y < 0)
			new_y = 0;

		if (new_y > A->height - VISIBLE_Y)
			new_y = A->height - VISIBLE_Y;
	}

	if (A->view_x == new_x && A->view_y == new_y)
		return;

	A->view_x = new_x;
	A->view_y = new_y;

	// map has scrolled, need to redraw all the map tiles
	V_MarkDirty_World ();
}

//----------------------------------------------------------------------

int V_LetterToFont (char ch)
{
	switch (ch)
	{
		case 'd': return F_Gray;
		case 'r': return F_Red;
		case 'g': return F_Green;
		case 'b': return F_Blue;
		case 'm': return F_Brown;
		case 'o': return F_Orange;
		case 'y': return F_Yellow;
		default : return F_White;
	}
}

const char * V_FormatNumber (int num, int digits)
{
	// the number will be right-aligned, padded with spaces.

	// negative values are NOT supported
	if (num < 0)
		num = 0;

	static char buffer[64];

	char * p = &buffer[sizeof(buffer)-1];

	// ensure result is NUL-terminated
	*p = 0;

	if (num == 0)
	{
		*--p = '0';
		digits--;
	}

	while (digits > 0 && num > 0)
	{
		*--p = '0' + (num % 10);
		num  = num / 10;
		digits--;
	}

	while (digits > 0)
	{
		*--p = ' ';
		digits--;
	}

	return p;
}

int V_WriteChar (struct Image * dest, int x, int y, struct Tileset * font, int ch)
{
	// whitespace is easy
	if (ch == 32)
	{
		return x + FONT_STEP;
	}

	// remap characters which don't exist in the font
	if (ch < 32 || ch >= 127)
	{
		ch = '?';
	}

	int col = (ch - 32) & 15;
	int row = (ch - 32) / 16;

	V_DrawTile (dest, x, y, font, MAKE_TCOORD (row, col));

	return x + FONT_STEP;
}

int V_WriteText (struct Image * dest, int x, int y, const char * s, int number)
{
	// string may contain escapes beginning with '^' (caret).
	// a letter escape selects font color, a digit shows an integer.
	// the return value is the new X position.

	struct Tileset * font = &fonts[F_White];

	bool is_escaped = false;

	while (*s != 0)
	{
		int ch = (unsigned char) *s++;

		if (ch == '^' && is_escaped)
		{
			// draw as a normal character
			is_escaped = false;
		}
		else if (ch == '^')
		{
			is_escaped = true;
			continue;
		}
		else if (is_escaped)
		{
			is_escaped = false;

			if (ch == '_')
			{
				// a narrower than normal space
				x = x + 10;
			}
			else if ('1' <= ch && ch <= '9')
			{
				int digits = ch - '0';
				const char * buf = V_FormatNumber (number, digits);

				while (*buf != 0)
				{
					ch = (unsigned char) *buf++;
					x  = V_WriteChar (dest, x, y, font, ch);
					digits--;
				}
			}
			else
			{
				font = &fonts[V_LetterToFont (ch)];
			}
			continue;
		}

		x = V_WriteChar (dest, x, y, font, ch);
	}

	return x;
}

void V_DrawStatObj (int x, int y, int obj)
{
	// draw an empty box
	V_DrawTile (stat_box, x, y, &gui_tiles, MAKE_TCOORD (1, 7));

	if (obj != 0)
	{
		int row = (obj >> 4) - 1;
		int col = (obj & 15);

		if (row >  8)
			row -= 8;

		V_DrawTile (stat_box, x, y, &gui_tiles, MAKE_TCOORD (row, col));
	}
}

void V_RenderStats (void)
{
	// clear background to black
	B_Clear (stat_box, BLACK_COLOR);

	// horizontal separator (between map view and HUD)
	B_Fill (stat_box, 0, 0, STAT_W, 2, PAL_FindColor (152, 127, 11));
	B_Fill (stat_box, 0, 2, STAT_W, 2, PAL_FindColor (104,  87,  9));

	// vertical separator (between stats panel and message box)
	B_Fill (stat_box, 0, 0, 2, HUD_H,   PAL_FindColor (152, 127, 11));
	B_Fill (stat_box, 2, 2, 2, HUD_H-2, PAL_FindColor (104,  87,  9));

	int x = 12;
	int y = 12;

	int lh = 32;

	V_WriteText (stat_box, x, y + lh*0, "^mHp:^_^w^4",   player.health);
	V_WriteText (stat_box, x, y + lh*1, "^mSkul:^_^w^2", player.skulls);
	V_WriteText (stat_box, x, y + lh*2, "^mProt:^_^w^2", player.protection);
	V_WriteText (stat_box, x, y + lh*3, "^mDamg:^_^w^2", player.damage);

	x = x + 134;

	V_WriteText (stat_box, x, y + lh*0, "^mArm: ", 0);
	V_WriteText (stat_box, x, y + lh*1, "^mShld:", 0);
	V_WriteText (stat_box, x, y + lh*2, "^mHelm:", 0);
	V_WriteText (stat_box, x, y + lh*3, "^mWeap:", 0);

	x = x + 80;

	V_DrawStatObj (x, y+2 + 0*30, player.armor);
	V_DrawStatObj (x, y+2 + 1*30, player.shield);
	V_DrawStatObj (x, y+2 + 2*30, player.helmet);
	V_DrawStatObj (x, y+2 + 3*30, player.weapon);

	x = x + 48;

	V_WriteText (stat_box, x, y + lh*0, "^mAcid: ^_^w^2", player.wands[0]);
	V_WriteText (stat_box, x, y + lh*1, "^mSleep:^_^w^2", player.wands[1]);
	V_WriteText (stat_box, x, y + lh*2, "^mDeath:^_^w^2", player.wands[2]);
	V_WriteText (stat_box, x, y + lh*3, "^mFire: ^_^w^2", player.wands[3]);

	x = x + 142;

//	V_WriteText (stat_box, x, y + lh*0, "^mInv:", 0);

	x = x + 4;

	int i;
	for (i = 0 ; i < 4 ; i++)
	{
		V_DrawStatObj (x +  0, y+2 + i*30, player.inventory[i*2+0]);
		V_DrawStatObj (x + 40, y+2 + i*30, player.inventory[i*2+1]);
	}

	// copy the off-screen buffer to the screen
	B_DrawImage (stat_box, STAT_X, HUD_Y, false);
}

void V_RenderMessages (void)
{
	B_Clear (message_box, BLACK_COLOR);

	// horizontal separator (between map view and HUD)
	B_Fill (message_box, 0, 0, STAT_X, 2, PAL_FindColor (152, 127, 11));
	B_Fill (message_box, 0, 2, STAT_X, 2, PAL_FindColor (104,  87,  9));

	// determine range of lines to show
	int pos = message_list.last;
	int len = message_list.used;

	if (len > MSG_LINES)
		len = MSG_LINES;

	if (message_list.used > MSG_LINES)
		pos += (message_list.used - MSG_LINES);

	pos = pos - message_list.scrolled;

	int i;
	for (i = 0 ; i < len ; i++)
	{
		const char * line = &message_list.lines[(pos + i) % MAX_HISTORY][0];

		V_WriteText (message_box, 4, i * 32 + 12, line, 0);
	}

	// show indication that we have scrolled back
	if (message_list.scrolled > 0)
	{
		int x;
		for (x = 27 ; x < STAT_X-20 ; x += 45)
		{
			V_WriteChar (message_box, x, HUD_H-14, &fonts[F_Blue], '^');
		}
	}

	// copy the off-screen buffer to the screen
	B_DrawImage (message_box, 0, HUD_Y, false);
}

void V_RenderHUD (void)
{
	if (dirty.messages)
		V_RenderMessages ();

	if (dirty.stats)
		V_RenderStats ();

	dirty.messages = false;
	dirty.stats    = false;
}

void V_PrintToLog (const char * line)
{
	char buf[200];
	int  pos = 0;

	// strip out any escape sequences
	while (*line != 0 && pos < (int)sizeof(buf)-2)
	{
		char ch = *line++;

		if (ch == '^' && *line != 0)
		{
			if (*line == '^')
				buf[pos++] = ch;

			line++;
			continue;
		}

		buf[pos++] = ch;
	}

	buf[pos] = 0;

	M_LogPrint ("%s\n", buf);
}

void V_ClearMessages (void)
{
	message_list.last = 0;
	message_list.used = 0;
	message_list.scrolled = 0;

	dirty.messages = true;

	M_LogPrint ("\n");
}

void V_Print (const char * fmt, ...)
{
	if (message_list.used < MAX_HISTORY)
		message_list.used++;
	else
		message_list.last = (message_list.last + 1) % MAX_HISTORY;

	// get pointer to the corresponding line
	int idx = (message_list.last + message_list.used - 1) % MAX_HISTORY;
	char * line = &message_list.lines[idx][0];

	va_list arg_ptr;

	va_start (arg_ptr, fmt);
	vsnprintf (line, MAX_LINE_LEN-1, fmt, arg_ptr);
	va_end (arg_ptr);

	line[MAX_LINE_LEN-1] = 0;

	V_PrintToLog (line);

	// if user scrolled up, jump to bottom
	message_list.scrolled = 0;

	dirty.messages = true;
}

void V_ScrollMessages (int dy)
{
	// dy is < 0 to scroll back, > 0 to scroll forward

	if (message_list.used <= MSG_LINES)
		return;

	int max = message_list.used - MSG_LINES;

	int new_scrolled = message_list.scrolled - dy;

	if (new_scrolled < 0)   new_scrolled = 0;
	if (new_scrolled > max) new_scrolled = max;

	if (new_scrolled != message_list.scrolled)
	{
		message_list.scrolled = new_scrolled;
		dirty.messages = true;
	}
}

//----------------------------------------------------------------------

void V_RenderDialog (void)
{
	if (! (dialog_active && dirty.dialog))
		return;

	// render the frame
	int tx, ty;
	for (ty = 0 ; ty < dialog_win.tile_h ; ty++)
	for (tx = 0 ; tx < dialog_win.tile_w ; tx++)
	{
		int dx = (tx == 0) ? 0 : (tx == dialog_win.tile_w-1) ? 2 : 1;
		int dy = (ty == 0) ? 0 : (ty == dialog_win.tile_h-1) ? 2 : 1;

		int x = dialog_win.tile_x + tx * 32;
		int y = dialog_win.tile_y + ty * 32;

		V_DrawTile (NULL, x, y, &gui_tiles, MAKE_TCOORD (dy, 4 + dx));
	}

	// render the text
	for (ty = 0 ; ty < dialog_win.height ; ty++)
	{
		const char * line = &dialog_win.lines[ty][0];

		int x = dialog_win.x;
		int y = dialog_win.y + ty * 32;

		V_WriteText (NULL, x, y, line, 0);
	}

	// render the image
	if (dialog_win.image > 0)
	{
		int x = dialog_win.x + dialog_win.image_dx;
		int y = dialog_win.y + dialog_win.image_dy;

		B_DrawImage (trophy_image, x, y, true);
	}

	dirty.dialog = false;
}

void V_OpenDialog (int width, int height)
{
	// Note: it is okay to do this when a dialog is already active.

	Assert (width  <= MAX_DIALOG_W);
	Assert (height <= MAX_DIALOG_H);

	dialog_win.width  = width;
	dialog_win.height = height;
	dialog_win.image  = 0;

	memset (dialog_win.lines, 0, sizeof (dialog_win.lines));

	dialog_win.tile_w = (width * FONT_STEP) / 32 + 3;
	dialog_win.tile_h = height + 2;

	dialog_win.tile_x = (UNSCALED_W - dialog_win.tile_w * 32) / 2;
	dialog_win.tile_y = (UNSCALED_H - dialog_win.tile_h * 32) / 2;

	dialog_win.x = (UNSCALED_W - width  * FONT_STEP) / 2;
	dialog_win.y = (UNSCALED_H - height * 32) / 2;

	dialog_active = true;

	dirty.dialog = true;
}

void V_CloseDialog (void)
{
	dialog_active = false;

	V_MarkAllDirty ();
}

void V_DialogText (int x, int y, const char * fmt, ...)
{
	Assert (dialog_active);
	Assert (y >= 0 && y < dialog_win.height);
	Assert (x >= 0 && x < dialog_win.width);

	char * line = &dialog_win.lines[y][0];

	int i;
	for (i = 0 ; i < x ; i++)
		*line++ = ' ';

	va_list arg_ptr;

	va_start (arg_ptr, fmt);
	vsnprintf (line, MAX_DIALOG_W-1-x, fmt, arg_ptr);
	va_end (arg_ptr);

	line[MAX_DIALOG_W-1-x] = 0;

	dirty.dialog = true;
}

void V_DialogImage (int dx, int dy)
{
	Assert (dialog_active);

	dialog_win.image    = 1;
	dialog_win.image_dx = dx;
	dialog_win.image_dy = dy;
}

//----------------------------------------------------------------------

void V_Refresh (bool idle)
{
	if (v_switched)
	{
		v_switched = false;

		/*
		B_Clear (NULL, BLACK_COLOR);
		*/

		V_MarkAllDirty ();
	}

	// if there is no world yet, fill whole screen with black
	if (world.total == 0)
	{
		if (dirty.world)
			B_Fill (NULL, 0, 0, UNSCALED_W, UNSCALED_H, BLACK_COLOR);

		dirty.world = false;
	}
	else
	{
		V_RenderWorld ();
		V_RenderHUD ();
	}

	V_RenderDialog ();

	if (idle)
		B_Delay (IDLE_DELAY);

	B_Update ();
}
