// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "main.h"


bool D_ConfirmDialog (int width, const char * msg1, const char * msg2)
{
	int height = 3 + (msg2 ? 1 : 0);

	V_OpenDialog (width, height);

	V_DialogText (0, 0, "%s", msg1);

	if (msg2)
		V_DialogText (0, 1, "%s", msg2);

	// center the choice string
	int x = (width - 6) / 2;
	if (x < 0) x = 0;

	V_DialogText (x, height-1, "^yY^n or ^yN^n");

	for (;;)
	{
		int key = B_InputKey ();

		if (key == KEY_Y)
		{
			V_CloseDialog ();
			return true;
		}

		if (key == KEY_N || key == KEY_ESC || key == EVENT_CLOSE_WINDOW)
		{
			V_CloseDialog ();
			return false;
		}

		V_Refresh (key == NO_KEY);
	}
}

//----------------------------------------------------------------------

char D_MainMenuDialog (void)
{
	// returns an upper case letter of selection, e.g. 'F' for fresh game.
	// returns 0 when user presses ESC key (to dismiss the window).

	bool will_save = (world.state != GS_Victory);

	const char * saving = will_save ? "(save)" : "";

	V_OpenDialog (19, 11);

	V_DialogText (0, 1, "^_--- MAIN MENU ---");

	V_DialogText (0, 3, "   ^yF^oresh Game");
	V_DialogText (0, 4, "   ^yR^oeplay Game");
	V_DialogText (0, 5, "   ^yD^oiscard Game");

	V_DialogText (0, 7, "   ^yO^options");
	V_DialogText (0, 8, "   ^yH^oelp");
	V_DialogText (0, 9, "   ^yQ^ouit %s", saving);

	for (;;)
	{
		int key = B_InputKey ();

		switch (key)
		{
			case KEY_M:
			case KEY_ESC:
			case KEY_ENTER:
			case EVENT_CLOSE_WINDOW:
				V_CloseDialog ();
				return 0;

			case KEY_F:
				V_CloseDialog ();
				return 'F';

			case KEY_R:
				V_CloseDialog ();
				return 'R';

			case KEY_D:
				V_CloseDialog ();
				return 'D';

			case KEY_O:
				V_CloseDialog ();
				return 'O';

			case KEY_H:
				V_CloseDialog ();
				return 'H';
/*
			case KEY_S:
				if (will_save) {
					V_CloseDialog ();
					return 'Q';
				}
				break;
*/
			case KEY_Q:
				V_CloseDialog ();
				return 'Q';

			default:
				break;
		}

		V_Refresh (key == NO_KEY);
	}
}

//----------------------------------------------------------------------

void  D_InventoryDialog (void)
{
	// count number of items
	int i;
	int count = 0;

	for (i = 0 ; i < INVENT_SIZE ; i++)
	{
		if (player.inventory[i] > 0)
			count += 1;
	}

	V_OpenDialog (25, (count == 0) ? 10 : 9 + count);

	V_DialogText (0, 1, "  ---- INVENTORY ----");

	const char * armor  = "no armor";
	const char * shield = "no shield";
	const char * helmet = "no helmet";
	const char * weapon = "no weapon";

	if (player.armor  > 0) armor  = L_ObjName (player.armor);
	if (player.shield > 0) shield = L_ObjName (player.shield);
	if (player.helmet > 0) helmet = L_ObjName (player.helmet);
	if (player.weapon > 0) weapon = L_ObjName (player.weapon);

	V_DialogText (0, 3, "  ^o%s", armor);
	V_DialogText (0, 4, "  ^o%s", shield);
	V_DialogText (0, 5, "  ^o%s", helmet);
	V_DialogText (0, 6, "  ^o%s", weapon);

	if (count == 0)
		V_DialogText (0, 8, "  ^ono items");

	int y = 8;

	for (i = 0 ; i < INVENT_SIZE ; i++)
	{
		if (player.inventory[i] > 0)
		{
			const char * item = L_ObjName (player.inventory[i]);
			V_DialogText (0, y, "  ^o%s", item);
			y += 1;
		}
	}

	for (;;)
	{
		int key = B_InputKey ();

		switch (key)
		{
			case KEY_I:
			case KEY_ESC:
			case KEY_ENTER:
			case KEY_SPACE:
			case EVENT_CLOSE_WINDOW:
				V_CloseDialog ();
				return;
		}

		V_Refresh (key == NO_KEY);
	}
}

//----------------------------------------------------------------------

void D_HelpGeneralInfo (void)
{
	V_OpenDialog (39, 17);

	V_DialogText (0, 1,  "   ----  GAMEPLAY  INFO  ----  ");

	// TODO gameplay info

	for (;;)
	{
		int key = B_InputKey ();

		switch (key)
		{
			case KEY_ESC:
			case KEY_ENTER:
			case KEY_SPACE:
			case EVENT_CLOSE_WINDOW:
				V_CloseDialog ();
				return;
		}

		V_Refresh (key == NO_KEY);
	}
}

void D_HelpShowKeys (void)
{
	V_OpenDialog (39, 15);

	V_DialogText (0, 1,  "   -----  KEYBOARD  COMMANDS  ----- ");

	V_DialogText (0, 3,  "   ^yQ^o: quaff potion   ^yI^o: inventory  ");
	V_DialogText (0, 4,  "   ^yR^o: read object    ^yO^o: open door  ");
	V_DialogText (0, 5,  "   ^yT^o: teleport       ^yP^o: place skull");

	V_DialogText (0, 7,  "   ^yA^o: zap acid       ^yH^o: move left  ");
	V_DialogText (0, 8,  "   ^yS^o: zap sleep      ^yJ^o: move down  ");
	V_DialogText (0, 9,  "   ^yD^o: zap death      ^yK^o: move up    ");
	V_DialogText (0, 10, "   ^yF^o: zap fire       ^yL^o: move right ");

//	V_DialogText (0, 13, "   ^yX^o: examine        ^yG^o: go far");
	V_DialogText (0, 12, "   ^yZ^o: wait a turn    ^yM^o: main menu  ");
	V_DialogText (0, 13, "   ^yC^o: climb stairs                     ");

	for (;;)
	{
		int key = B_InputKey ();

		switch (key)
		{
			case KEY_ESC:
			case KEY_ENTER:
			case KEY_SPACE:
			case EVENT_CLOSE_WINDOW:
				V_CloseDialog ();
				return;
		}

		V_Refresh (key == NO_KEY);
	}
}

void D_HelpCredits (void)
{
	V_OpenDialog (39, 17);

	V_DialogText (0, 1,  "   --------  CREDITS  --------  ");

	// TODO credits screen

	for (;;)
	{
		int key = B_InputKey ();

		switch (key)
		{
			case KEY_ESC:
			case KEY_ENTER:
			case KEY_SPACE:
			case EVENT_CLOSE_WINDOW:
				V_CloseDialog ();
				return;
		}

		V_Refresh (key == NO_KEY);
	}
}

char D_HelpMenuDialog (void)
{
	// returns an upper case letter of selection.
	// returns 0 when user presses ESC key (to dismiss the window).

	V_OpenDialog (24, 7);

	V_DialogText (0, 1, "   --- HELP MENU ---");

	V_DialogText (0, 3, "   ^y1.^o General Info");
	V_DialogText (0, 4, "   ^y2.^o Key Commands");
	V_DialogText (0, 5, "   ^y3.^o Credits");

	for (;;)
	{
		int key = B_InputKey ();

		switch (key)
		{
			case KEY_H:
			case KEY_ESC:
			case KEY_ENTER:
			case EVENT_CLOSE_WINDOW:
				V_CloseDialog ();
				return 0;

			case KEY_1:
			case KEY_G:
				V_CloseDialog ();
				return 'G';

			case KEY_2:
			case KEY_K:
				V_CloseDialog ();
				return 'K';

			case KEY_3:
			case KEY_C:
				V_CloseDialog ();
				return 'C';

			default:
				break;
		}

		V_Refresh (key == NO_KEY);
	}
}

//----------------------------------------------------------------------

void D_VictoryDialog (void)
{
	V_OpenDialog (15, 9);

	V_DialogText (0, 1, " ^yYou have won!");

	V_DialogImage (45, 100);

	for (;;)
	{
		int key = B_InputKey ();

		switch (key)
		{
			case KEY_ESC:
			case KEY_ENTER:
			case KEY_SPACE:
			case EVENT_CLOSE_WINDOW:
				V_CloseDialog ();
				return;
		}

		V_Refresh (key == NO_KEY);
	}
}
