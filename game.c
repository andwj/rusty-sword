// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "main.h"


#define SPIKE_DAMAGE  20


int G_CalcDamage (int dmg)
{
	// apply the player's protection (via armor) to the input damage.
	// input must be > 0.  output will be > 0.  output is usually less
	// than input when the player has *some* armor.
	Assert (dmg > 0);

	dmg = dmg * (100 - player.protection) / 100;

	if (dmg < 1)
		dmg = 1;

	return dmg;
}

void G_HurtPlayer (int dmg)
{
	if (player.health >  dmg)
	{
		player.health -= dmg;
		V_MarkDirty_Stats ();
		return;
	}

	player.health = 0;
	world.state   = GS_Defeat;

	V_MarkDirty_Stats ();
	V_Print ("^rYou die.");

	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[player.x][player.y];

	// replace the player avatar with a tombstone
	T->mon  = 0;
	T->obj  = OB_Tombstone;
	T->flag = FL_DIRTY;
}

bool G_ProjectileAttack (int x, int y, unsigned char mon)
{
	int dx   = 0;
	int dy   = 0;
	int dist = 0;

	if (! (x == player.x || y == player.y))
		return false;

	if (x > player.x)
	{
		dx   = -1;
		dist = x - player.x;
	}
	else if (x < player.x)
	{
		dx   = +1;
		dist = player.x - x;
	}
	else if (y > player.y)
	{
		dy   = -1;
		dist = y - player.y;
	}
	else
	{
		dy   = +1;
		dist = player.y - y;
	}

	// too far away?
	if (dist > BEAM_DIST)
		return false;

	// something solid in the way?
	if (L_MonBeamIsBlocked (x, y, dx, dy, dist))
		return false;

	const char * what = "A shuriken";
	int effect = 2;
	int dmg    = 10;

	// the archer only shoots horizontally
	if (mon == MO_Archer)
	{
		if (dx == 0)
			return false;

		what   = "An arrow";
		effect = (dx > 0) ? 0 : 1;
		dmg    = 5;
	}

	int i;
	for (i = 1 ; i <= dist ; i++)
	{
		x += dx;
		y += dy;

		V_AddEffect (x, y, 0x10, effect);
		V_Refresh (false);

		B_Delay (50);

		V_RemoveEffect (x, y);
	}

	// hurt the player
	dmg = G_CalcDamage (dmg);

	V_Print ("%s hits you! ^r%d", what, -dmg);
	G_HurtPlayer (dmg);
	return true;
}

bool G_DragonAttack (int x, int y, unsigned char mon)
{
	// dragon must be in same row as player
	if (y != player.y)
		return false;

	int dx   = 0;
	int dist = 0;

	if (x > player.x)
	{
		dx   = -1;
		dist = x - player.x;
	}
	else
	{
		//-- dx   = +1;
		//-- dist = player.x - x;
		return false;
	}

	// too far away?
	if (dist > BEAM_DIST)
		return false;

	// something solid in the way?
	if (L_MonBeamIsBlocked (x, y, dx, 0, dist))
		return false;

	/* animate a beam */

	// FIXME: four different kinds: GREEN / Acid / MAGIC / Fire
	int wand = (mon == MO_RedDragon) ? WAND_Fire : WAND_Acid;
	int dmg  = (mon == MO_RedDragon) ? 25 : (mon == MO_BlueDragon) ? 15 : 7;

	int length;

	for (length = 1 ; length <= dist ; length++)
	{
		int px = x + length * dx;
		int py = y;

		V_AddEffect (px, py, wand, length);
		V_Refresh (false);

		B_Delay (30);
	}

	B_Delay (100);

	V_ClearEffects ();

	// produce effect on player, if any

	unsigned char resist_obj = (mon == MO_RedDragon) ? OB_Ring+1 : OB_Ring+0;
	const char *  beam_name  = (mon == MO_RedDragon) ? "fire" : "acid";

	bool immune = L_PlayerHasObj (resist_obj, false);

	if (immune)
	{
		struct Area * A = &world.areas[player.z];
		struct Tile * T = &A->grid[x][y];

		// the dragon notices your resistance, won't try again
		T->flag |= FL_LEARNT;

		V_Print ("You resist a beam of %s.", beam_name);
		return true;
	}

	dmg = G_CalcDamage (dmg);

	V_Print ("A beam of %s hits you! ^r%d", beam_name, -dmg);
	G_HurtPlayer (dmg);
	return true;
}

bool G_MonsterAttack (int x, int y)
{
	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[x][y];

	// neutral creatures do not attack
	if (L_MonIsNeutral (T->mon))
		return false;

	if ((T->flag & FL_FRIEND) != 0)
		return false;

	int ax = abs (x - player.x);
	int ay = abs (y - player.y);

	// adjacent to the player?
	if ((ax == 0 && ay == 1) || (ax == 1 && ay == 0))
	{
		// TODO perform short-range attack

		V_Print ("something attacks!");
		return true;
	}

	/* check for a long-range attack */

	// require monster to be on-screen
	if (! V_TileOnScreen (x, y))
		return false;

	// don't bother if we have learnt of the player's resistance
	if ((T->flag & FL_LEARNT) != 0)
		return false;

	switch (T->mon)
	{
		case MO_Archer:
			return G_ProjectileAttack (x, y, T->mon);

		case MO_Knight:
			return G_ProjectileAttack (x, y, T->mon);

		case MO_BlueDragon:
			return G_DragonAttack (x, y, T->mon);

		case MO_RedDragon:
			return G_DragonAttack (x, y, T->mon);

		default:
			break;
	}

	return false;
}

void G_GuardMove (int x, int y)
{
	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[x][y];

	T->flag |= FL_MOVED;

	// when we are next to the player, don't move
	int dx = abs (x - player.x);
	int dy = abs (y - player.y);

	if ((dx == 0 && dy == 1) || (dy == 0 && dx == 1))
		return;

	int dist1 = dx + dy;

	// require movement in squares with the same ID number.
	// when zero, the monster stays in the same place.
	if (T->id == 0)
		return;

	int best_dir = -1;

	int dir;
	for (dir = 2 ; dir <= 8 ; dir += 2)
	{
		int nx = x + Dir_dx (dir);
		int ny = y + Dir_dy (dir);

		if (! L_ValidSquare (nx, ny))
			continue;

		struct Tile * T2 = &A->grid[nx][ny];
		if (T2->id != T->id)
			continue;

		if (! L_MonCanVisit (T2, T->mon))
			continue;

		dx = abs (player.x - nx);
		dy = abs (player.y - ny);

		// can move directly next to the player?
		if ((dx == 0 && dy == 1) || (dy == 0 && dx == 1))
		{
			best_dir = dir;
			break;
		}

		int dist2 = dx + dy;

		// moving closer?  if so, prefer a vertical direction
		if (dist2 < dist1)
		{
			if (best_dir < 0 || ny != y)
				best_dir = dir;
		}
	}

	if (best_dir > 0)
	{
		int nx = x + Dir_dx (best_dir);
		int ny = y + Dir_dy (best_dir);

		L_RawMoveMonster (x, y, nx, ny);
	}
}

bool G_FriendMove (int x, int y)
{
	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[x][y];

	if (T->env == EN_Up)
	{
		// move up to next area, or escape from the dungeons

		if (player.z == 0)
			L_RemoveMonster (T);
		else
			L_AddFriendToQueue (T);

		return true;
	}

	// can we move onto a nearby staircase?
	int dir;
	for (dir = 1 ; dir <= 9 ; dir++)
	{
		if (dir == 5)
			continue;

		int nx = x + Dir_dx (dir);
		int ny = y + Dir_dy (dir);

		if (L_ValidSquare (nx, ny))
		{
			struct Tile * T2 = &A->grid[nx][ny];

			if (T2->env == EN_Up && T2->mon == 0)
			{
				L_RawMoveMonster (x, y, nx, ny);
				return true;
			}
		}
	}

	// if touching the player, stay put
	if (ChebyshevDist (x - player.x, y - player.y) == 1)
	{
		T->flag |= FL_MOVED;
		return true;
	}

	// use normal movement (toward the player)
	return false;
}

bool G_DragonMove (int x, int y)
{
	// monsters with long-range attacks should move vertically when
	// they can, in order to use their attack as much as possible.

	int dx   = 0;
	int dist = 0;

	// already on the same row?
	if (y == player.y)
		return false;

	// on the same column?
	if (x == player.x)
		return false;

	struct Area * A = &world.areas[player.z];

#if 0
	// actual dragons must be to the right of the player
	if (x <= player.x)
	{
		switch (A->grid[x][y].mon)
		{
			case MO_GreenDragon:
			case MO_BlueDragon:
			case MO_RedDragon:
				return false;
		}
	}
#endif

	int dy = (y < player.y) ? +1 : -1;

	// move is blocked by another monster?
	if (A->grid[x][y + dy].mon != 0)
		return false;

	if (x > player.x)
	{
		dx   = -1;
		dist = x - player.x;
	}
	else if (x < player.x)
	{
		dx   = +1;
		dist = player.x - x;
	}

	// too far away?
	if (dist > BEAM_DIST)
		return false;

	// something would block the beam?
	if (L_MonBeamIsBlocked (x, player.y, dx, 0, dist))
		return false;

	// the way is clear vertically?
	// check all rows in-between, as well as the player's row.

	int cy = y;

	do
	{
		cy += dy;
		Assert (L_ValidSquare (x, cy));

		struct Tile * T = &A->grid[x][cy];

		if (! L_BlocksTravel (T, TRAVEL_Normal))
			return false;

	} while (cy != player.y);

	// okay, move it!
	L_RawMoveMonster (x, y, x, y + dy);
	return true;
}

bool G_PathingMove (int x, int y)
{
	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[x][y];

	enum MovementMode mode   = L_MonMoveMode     (T->mon);
	enum TravelMethod travel = L_MonTravelMethod (T->mon);

	int cur_dist = NAV_Query (travel, x, y);

	if (cur_dist < 0)
		return false;

	int best_dir   = -1;
	int best_score = -1;

	int dir;
	for (dir = 1 ; dir <= 9 ; dir++)
	{
		if (dir == 5)
			continue;

		int dx = Dir_dx (dir);
		int dy = Dir_dy (dir);

		int nx = x + dx;
		int ny = y + dy;

		if (! L_ValidSquare (nx, ny))
			continue;

		if (mode == MOVE_Straight && (dir & 1) != 0)
			continue;

		// allow a diagonal mover to move into doorways (etc)
		if (mode == MOVE_Diagonal && (dir & 1) == 0)
			if (! NAV_IsChokePoint (travel, nx, ny))
				continue;

		int next_dist = NAV_Query (travel, nx, ny);
		if (next_dist < 0)
			continue;

		struct Tile * N = &A->grid[nx][ny];
		if (N->mon != 0)
			continue;

		if (next_dist > cur_dist)
			continue;

		int score = 300;

		// this only affects diagonal movers (I think)...
		if (next_dist == cur_dist)
		{
			int cur_approx  = ApproxDist ( x - player.x,  y - player.y);
			int next_approx = ApproxDist (nx - player.x, ny - player.y);

			if (next_approx < cur_approx)
			{
				score = 200;
			}
			else
			{
				// prevent getting stuck on certain corners.
				if (! NAV_IsBadCorner (travel,  x,  y)) continue;
				if (  NAV_IsBadCorner (travel, nx, ny)) continue;

				score = 100;
			}
		}

		/* --- tie breakers --- */

		// prefer a diagonal move
		if ((dir & 1) == 1)
			score += 80;

		// prefer a vertical move
		if (dy != 0)
			score += 40;

		// prefer moving vertically toward the player
		if ((dy > 0 && y < player.y) || (dy < 0 && y > player.y))
			score += 20;

		// prefer moving horizontally toward the player
		if ((dx > 0 && x < player.x) || (dx < 0 && x > player.x))
			score += 10;

		if (best_score < score)
		{
			best_score = score;
			best_dir   = dir;
		}
	}

	if (best_dir < 0)
		return false;

	int new_x = x + Dir_dx (best_dir);
	int new_y = y + Dir_dy (best_dir);

#if 0
	if (dir < 0)
		return false;

	int new_x = x + Dir_dx (dir);
	int new_y = y + Dir_dy (dir);

	if (new_x == player.x && new_y == player.y)
	{
		// for diagonal/8-way movers, we reach here when the monster
		// is trying to move diagonally onto the player's square.

		int dx = Dir_dx (dir);
		int dy = Dir_dy (dir);

		if (dx * dy == 0)
			return false;

		if ((move_mode & MOVE_Diagonal) == 0)
			return false;

		const struct Tile * T1 = &A->grid[x][y + dy];
		const struct Tile * T2 = &A->grid[x + dx][y];

		if (L_MonCanVisit (T1, T->mon))
		{
			new_x = x;
			new_y = y + dy;
		}
		else if (L_MonCanVisit (T2, T->mon))
		{
			new_x = x + dx;
			new_y = y;
		}
		else
		{
			return false;
		}
	}
#endif

	L_RawMoveMonster (x, y, new_x, new_y);
	return true;
}

void G_MoveMonster (int x, int y)
{
	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[x][y];

	if (T->mon == 0 || T->mon == MO_Player)
		return;

	// skip hidden rooms
	if ((T->flag & FL_UNSEEN) != 0)
		return;

	// already moved?
	if ((T->flag & FL_MOVED) != 0)
		return;

	// do not act when the player is dead
	if (player.health <= 0)
		return;

	// when sleeping, lose a turn
	if (T->time > 0)
	{
		T->time--;
		T->flag |= FL_MOVED;
		T->flag |= FL_DIRTY;
		return;
	}

	// attempt to attack the player
	if (G_MonsterAttack (x, y))
	{
		T->flag |= FL_MOVED;
		return;
	}

	// guards stay near the thing they are guarding
	if ((T->flag & FL_GUARD) != 0)
	{
		G_GuardMove (x, y);
		return;
	}

	if ((T->flag & FL_FRIEND) != 0)
	{
		if (G_FriendMove (x, y))
			return;
	}

	if (L_MonHasLongRangeAttack (T->mon))
	{
		if (G_DragonMove (x, y))
			return;
	}

	if (G_PathingMove (x, y))
		return;

	// stay put
}

void G_ProcessMonColumn (int x)
{
	if (x < 0 || x >= AREA_MAX_W)
		return;

	int y;
	for (y = player.y ; y >= 0 ; y--)
	{
		G_MoveMonster (x, y);
	}

	for (y = player.y + 1 ; y < AREA_MAX_H ; y++)
	{
		G_MoveMonster (x, y);
	}
}

void G_ProcessMonsters (void)
{
	// visit all the cells of the current area, and attempt to move
	// any monster there (unless it has already moved).
	//
	// our visit strategy ensures that monsters closer to the player
	// get to move before other ones, more-or-less.

	int dx;
	for (dx = 0 ; dx < AREA_MAX_W ; dx++)
	{
		// do not continue if the player has been killed
		if (player.health <= 0)
			return;

		G_ProcessMonColumn (player.x - dx);
		G_ProcessMonColumn (player.x + dx + 1);
	}
}

void G_ClearMovedFlags ()
{
	struct Area * A = &world.areas[player.z];

	int max_dist = 20;

	int x1 = player.x - max_dist;
	int y1 = player.y - max_dist;

	int x2 = player.x + max_dist;
	int y2 = player.y + max_dist;

	if (x1 < 0) x1 = 0;
	if (y1 < 0) y1 = 0;

	if (x2 >= A->width)  x2 = A->width  - 1;
	if (y2 >= A->height) y2 = A->height - 1;

	int x, y;

	for (x = x1 ; x <= x2 ; x++)
	for (y = y1 ; y <= y2 ; y++)
	{
		A->grid[x][y].flag &= ~FL_MOVED;
	}
}

void G_ManifestFriend (struct QueuedFriend * F)
{
	int x = (int)F->x;
	int y = (int)F->y;

	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[x][y];

	// something in the way?
	if (T->mon != 0)
		return;

	T->mon  = F->mon;
	T->obj  = 0;
	T->flag = FL_FRIEND | FL_DIRTY;

	// clear queue entry
	F->mon = 0;
}

void G_ProcessFriendQueue (void)
{
	// this handles friendly creatures coming up from a lower level

	int i;
	for (i = 0 ; i < MAX_QUEUE ; i++)
	{
		struct QueuedFriend * F = &world.queue[i];

		if (F->mon != 0 && (int)F->z == player.z)
		{
			G_ManifestFriend (F);
		}
	}
}

void G_WorldStep (void)
{
	// ensure the screen is up-to-date after moving the player
	V_Refresh (false);

	// create the navigation map
	NAV_Build ();

	// process monsters a few times, since one monster moving may
	// allow another to move, and so forth...
	G_ProcessMonsters ();
	G_ProcessMonsters ();
	G_ProcessMonsters ();

	G_ProcessFriendQueue ();

	G_ClearMovedFlags ();
}

//----------------------------------------------------------------------

void G_HurtMonster (struct Tile * T, unsigned short dmg, const char * verb)
{
	if (T->hp > dmg)
	{
		T->hp -= dmg;
		V_Print ("You %s the %s. ^b%d", verb, L_MonName (T->mon), (int)T->hp);
		return;
	}

	verb = "kill";

	if (L_MonImmuneTo (T->mon, WAND_Death))
		verb = "destroy";

	V_Print ("You %s the %s!", verb, L_MonName (T->mon));

	// killed skeleton leave a skull behind
	unsigned char leave_obj = 0;
	if (T->mon == MO_Skeleton)
		leave_obj = OB_Skull;

	L_RemoveMonster (T);

	if (leave_obj != 0 && (T->env & 0xF0) != EN_Special)
		T->obj = leave_obj;
}

void G_PlayerAttack (int x, int y)
{
	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[x][y];

	Assert (T->mon != 0);

	G_HurtMonster (T, player.damage, L_PlayerAttackWord ());

	player.acted = true;
}

void G_WandHitMonster (int wand, int x, int y)
{
	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[x][y];

	if (T->mon == 0)
		return;

	// always miss a neutral creature
	if (L_MonIsNeutral (T->mon))
		return;

	// wand beams do not hit friends
	if ((T->flag & FL_FRIEND) != 0)
		return;

	if (L_MonImmuneTo (T->mon, wand))
	{
		V_Print ("The %s is unaffected.", L_MonName (T->mon));
		return;
	}

	switch (wand)
	{
		case WAND_Acid:
			G_HurtMonster (T, 15, "melt");
			break;

		case WAND_Fire:
			G_HurtMonster (T, 25, "burn");
			break;

		case WAND_Sleep:
			if (T->time == 0)
				V_Print ("The %s falls asleep.", L_MonName (T->mon));
			else
				V_Print ("The %s gets sleepier.", L_MonName (T->mon));

			T->time  = SLEEP_TIME;
			T->flag |= FL_DIRTY;
			break;

		case WAND_Death:
			G_HurtMonster (T, T->hp, "kill");
			break;
	}
}

//----------------------------------------------------------------------

void Action_Wait (void)
{
	player.acted = true;
}

void Action_FillPit (int tx, int ty)
{
	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[tx][ty];

	T->flag |= FL_DIRTY;

	// determine the new floor, pick majority when possible
	unsigned char N = 0;
	unsigned char S = 0;
	unsigned char E = 0;
	unsigned char W = 0;

	if (L_ValidSquare (tx, ty-1)) N = A->grid[tx][ty-1].env;
	if (L_ValidSquare (tx, ty+1)) S = A->grid[tx][ty+1].env;
	if (L_ValidSquare (tx+1, ty)) E = A->grid[tx+1][ty].env;
	if (L_ValidSquare (tx-1, ty)) W = A->grid[tx-1][ty].env;

	if ((N & 0xF0) == EN_Floor && (N == S || N == E || N == W))
	{
		T->env = N;
	}
	else if ((S & 0xF0) == EN_Floor && (S == N || S == E || S == W))
	{
		T->env = S;
	}
	else if ((E & 0xF0) == EN_Floor && (E == W || E == N || E == S))
	{
		T->env = E;
	}
	else
	{
		T->env = W;
	}
}

void Action_BoulderDownStairs (unsigned char new_area, int dir)
{
	unsigned char old_area = player.z;

	struct Area * A2 = &world.areas[new_area];

	// find the matching stair
	int stair_x = 0;
	int stair_y = 0;

	L_FindMatchingStair (A2, old_area, &stair_x, &stair_y);

	// find a place the boulder can go
	int best = -1;
	int i;

	for (i = 0 ; i < 4 ; i++, dir += 2)
	{
		if (dir > 8)
			dir = 2;

		int bx = stair_x + Dir_dx (dir);
		int by = stair_y + Dir_dy (dir);

		if (! L_ValidSquare (bx, by))
			continue;

		struct Tile * T = &A2->grid[bx][by];

		if (L_EnvIsBlocking (T->env))
			continue;

		if ((T->env & 0xF0) == EN_Special)
			continue;

		if (T->obj != 0)
			continue;

		if (T->mon == MO_Player || L_MonIsNeutral (T->mon) || (T->flag & FL_FRIEND))
			continue;

		if (best < 0 || T->mon != 0)
			best = dir;
	}

	if (best < 0)
	{
		V_Print ("You hear it smash to pieces!");
	}
	else
	{
		int bx = stair_x + Dir_dx (best);
		int by = stair_y + Dir_dy (best);

		struct Tile * T = &A2->grid[bx][by];

		if (T->mon != 0)
		{
			V_Print ("You squish a %s!", L_MonName (T->mon));
			L_RemoveMonster (T);
		}

		T->obj   = OB_Boulder;
		T->flag |= FL_DIRTY;
	}
}

void Action_PushBoulder (int nx, int ny, int dir)
{
	// boulder destination
	int bx = nx + Dir_dx (dir);
	int by = ny + Dir_dy (dir);

	if (! L_ValidSquare (bx, by))
		return;

	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[bx][by];

	if (L_EnvIsBlocking (T->env))
		return;

	// IDEA: crush skulls

	if (T->mon != 0 || T->obj != 0)
		return;

	// handle water, lava, spiked pits, etc...

	switch (T->env)
	{
		case EN_Down:
			V_Print ("The boulder rolls down!");
			Action_BoulderDownStairs (T->id, dir);
			break;

		case EN_Spikes:
			V_Print ("The boulder fills the pit!");
			Action_FillPit (bx, by);
			break;

		case EN_Water:
			V_Print ("The boulder fills the pond!");
			Action_FillPit (bx, by);
			break;

		case EN_Lava:
			V_Print ("Glomp! Lava ate the boulder!");
			break;

		default:
			// a normal push
			T->obj   = OB_Boulder;
			T->flag |= FL_DIRTY;
			break;
	}

	// this erases the existing boulder
	L_RawMovePlayer (nx, ny);

	player.acted = true;
}

void Action_InventoryItem (unsigned char obj)
{
	// running out of inventory slots should not happen, the world
	// generation logic should take care to prevent it.  nevertheless
	// we prioritize items and replace a lower one when full.

	int slot = -1;
	int lowest_pri = L_ObjPriority (obj);

	int i;
	for (i = 0 ; i < INVENT_SIZE ; i++)
	{
		if (player.inventory[i] == 0)
		{
			slot = i;
			break;
		}

		int pri = L_ObjPriority (player.inventory[i]);

		if (pri < lowest_pri)
		{
			slot = i;
			lowest_pri = pri;
		}
	}

	if (slot < 0)
	{
		V_Print ("You break %s.", L_ObjName (obj));
		return;
	}

	player.inventory[slot] = obj;

	const char * verb = "find";

	if ((obj & 0xF0) == OB_Ring)
		verb = "wear";

	V_Print ("You %s %s.", verb, L_ObjName (obj));
}

bool Action_GiveArmor (unsigned char obj)
{
	if (player.armor < obj)
		player.armor = obj;

	// always pick it up (never say it was broken)
	return true;
}

bool Action_GiveHelmet (unsigned char obj)
{
	if (player.helmet < obj)
		player.helmet = obj;

	// always pick it up (never say it was broken)
	return true;
}

bool Action_GiveShield (unsigned char obj)
{
	if (player.shield < obj)
		player.shield = obj;

	// always pick it up (never say it was broken)
	return true;
}

bool Action_GiveWeapon (unsigned char obj)
{
	if (player.weapon < obj)
		player.weapon = obj;

	// always pick it up (never say it was broken)
	return true;
}

bool Action_GiveWand (unsigned char obj)
{
	int kind = obj & 0x0F;

	if (kind >= 4)
		return false;

	if (player.wands[kind] >= MAX_CHARGES)
		return false;

	player.wands[kind] += 10;

	if (player.wands[kind] > MAX_CHARGES)
		player.wands[kind] = MAX_CHARGES;

	return true;
}

void Action_HealthPotion (unsigned char obj)
{
	int kind = obj & 0x0F;

	int health = 1;

	switch (kind)
	{
		case 0: health =  20; break;
		case 1: health = 100; break;
		case 2: health = 300; break;
		default: break;
	}

	if (player.health >= MAX_HEALTH)
	{
		V_Print ("You spill %s.", L_ObjName (obj));
		return;
	}

	player.health += health;

	if (player.health > MAX_HEALTH)
	{
		health -= (player.health - MAX_HEALTH);
		player.health = MAX_HEALTH;
	}

	V_Print ("The potion heals you! ^g+%d", health);
}

void Action_GetItem (unsigned char obj)
{
	bool got = false;

	// handle some special stuff first

	switch (obj)
	{
		case OB_Skull:
			if (player.skulls >= MAX_CHARGES)
				break;

			player.skulls += 1;

			got = true;
			break;

		case OB_Scroll:
		case OB_Paper:
		case OB_Potion+3:
			Action_InventoryItem (obj);
			return;

		default:
			break;
	}

	// handle the weapons, armor, wands here
	switch (obj & 0xF0)
	{
		case OB_Armor:
			got = Action_GiveArmor (obj);
			break;

		case OB_Helmet:
			got = Action_GiveHelmet (obj);
			break;

		case OB_Shield:
			got = Action_GiveShield (obj);
			break;

		case OB_Weapon:
			got = Action_GiveWeapon (obj);
			break;

		case OB_Wand:
			got = Action_GiveWand (obj);
			break;

		case OB_Potion:
			Action_HealthPotion (obj);
			return;

		case OB_Key:
			Action_InventoryItem (obj);
			return;

		case OB_Ring:
			Action_InventoryItem (obj);
			return;

		default:
			break;
	}

	if (got)
	{
		V_Print ("You get %s.", L_ObjName (obj));
	}
	else
	{
		// only get here when reach excessive numbers of skulls, wands (etc),
		// which the world generation logic will avoid.
		V_Print ("You break %s.", L_ObjName (obj));
	}
}

void Action_Move (int dir)
{
	int nx = player.x + Dir_dx (dir);
	int ny = player.y + Dir_dy (dir);

	if (! L_ValidSquare (nx, ny))
		return;

	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[nx][ny];

	if (T->mon != 0)
	{
		if (L_MonIsNeutral (T->mon) || (T->flag & FL_FRIEND) != 0)
		{
			// we will swap position with a friend (or neutral creature)
		}
		else
		{
			G_PlayerAttack (nx, ny);
			return;
		}
	}

	if (T->obj == OB_Boulder)
	{
		Action_PushBoulder (nx, ny, dir);
		return;
	}

	if (! L_PlayerCanVisit (T))
		return;

	// OK, we will move...
	// Note that L_RawMovePlayer() clears any object at new spot.

	unsigned char obj = T->obj;

	L_RawMovePlayer (nx, ny);

	// handle hazards
	bool player_levitating  = L_PlayerHasObj (OB_Ring+2, false);
	bool player_fire_resist = L_PlayerHasObj (OB_Ring+1, false);

	if (T->env == EN_Spikes && ! player_levitating)
	{
		// armor does not help with spikes
		V_Print ("You step on sharp spikes! ^r%d", -SPIKE_DAMAGE);
		G_HurtPlayer (SPIKE_DAMAGE);
	}

	if (T->env == EN_Water && ! player_levitating)
	{
		V_Print ("^rYou sink like a stone!");
		player.health = 0;
		world.state   = GS_Defeat;
		L_DirtyTile (player.x, player.y);
		return;
	}

	if (T->env == EN_Lava && ! player_fire_resist)
	{
		V_Print ("^rYou burn up in the lava!");
		player.health = 0;
		world.state   = GS_Defeat;
		L_DirtyTile (player.x, player.y);
		return;
	}

	if (player.health <= 0)
		return;

	if (obj != 0)
	{
		Action_GetItem (obj);

		L_RecomputePlayer ();
		V_MarkDirty_Stats ();
	}

	player.acted = true;
}

void Action_LeaveWorld (void)
{
	if (world.captives > 0)
	{
		V_Print ("^rNot all captives are released.");
		return;
	}

	// require all enemies to be vanquished
	if (world.enemies > 0)
	{
		V_Print ("^rNot all enemies are vanquished.");
		return;
	}

	// require all friends to have escaped
	if (world.friends > 0)
	{
		V_Print ("^rNot all friends have escaped.");
		return;
	}

	V_Print ("You escape the dungeons!");

	world.state = GS_Victory;
	V_MarkDirty_World ();

	D_VictoryDialog ();
}

void Action_Climb (void)
{
	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[player.x][player.y];

	switch (T->env)
	{
		case EN_Down:
			V_Print ("You climb down the stairs.");
			break;

		case EN_Up:
			if (player.z == 0)
			{
				Action_LeaveWorld ();
				return;
			}

			V_Print ("You climb up the stairs.");
			break;

		default:
			V_Print ("^rThere are no stairs here.");
			return;
	}

	L_PlayerEnterArea (T->id);

	player.acted = true;
}

void Action_Zap (int wand, const char * name)
{
	if (player.wands[wand] <= 0)
	{
		V_Print ("^rNo %s charges!", L_WandShortName (wand));
		return;
	}

	// don't waste a charge if beam would be zero-length
	int adj_x = player.x + player.face;

	if (! L_ValidSquare (adj_x, player.y))
	{
		V_Print ("^rNo room to zap a wand.");
		return;
	}

	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[adj_x][player.y];

	if (L_EnvIsBlocking (T->env) || L_ObjIsBlocking (T->obj))
	{
		V_Print ("^rNo room to zap a wand.");
		return;
	}

	player.zapping = true;
	L_DirtyTile (player.x, player.y);
	V_Refresh (false);
	B_Delay (30);

	int length;

	for (length = 1 ; length <= BEAM_DIST ; length++)
	{
		int px = player.x + length * player.face;
		int py = player.y;

		if (! L_ValidSquare (px, py))
			break;

		if (! V_TileOnScreen (px, py))
			break;

		G_WandHitMonster (wand, px, py);

		V_AddEffect (px, py, wand, length);
		V_Refresh (false);
		B_Delay (30);
	}

	B_Delay (200);

	V_ClearEffects ();

	player.zapping = false;
	L_DirtyTile (player.x, player.y);
	V_Refresh (false);

	// update stats
	player.wands[wand] -= 1;
	V_MarkDirty_Stats ();

	player.acted = true;
}

bool Action_TryOpen (int dx, int dy)
{
	int x = player.x + dx;
	int y = player.y + dy;

	if (! L_ValidSquare (x, y))
		return false;

	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[x][y];

	// IDEA: chests

	if (T->obj == OB_Bars)
	{
		V_Print ("The blocking bars open.");

		L_OpenDoor (x, y, true);

		player.acted = true;
		return true;
	}

	if ((T->env & 0xF1) == EN_Door)
	{
		unsigned char key = 0;
		const char *  adj = "";

		switch (T->env)
		{
			case EN_Door+2:
				key = OB_Key+3;
				adj = "gold";
				break;

			case EN_Door+4:
				key = OB_Key+2;
				adj = "silver";
				break;

			case EN_Door+6:
				key = OB_Key+1;
				adj = "ivory";
				break;

			default:
				// no key needed
				break;
		}

		if (key == 0)
		{
			V_Print ("You open the door.");
		}
		else if (L_PlayerHasObj (key, true))
		{
			V_Print ("The %s key unlocks the door.", adj);
		}
		else
		{
			V_Print ("^rLocked! You need a %s key.", adj);
			return true;
		}

		L_OpenDoor (x, y, false);
		L_MakeRoomVisible (x + dx, y + dy);

		player.acted = true;
		return true;
	}

	// nothing to open here
	return false;
}

void Action_Open (void)
{
	int dx = player.face;

	if (Action_TryOpen ( dx,   0)) return;
	if (Action_TryOpen (  0,  dx)) return;
	if (Action_TryOpen (-dx,   0)) return;
	if (Action_TryOpen (  0, -dx)) return;

	V_Print ("^rNothing nearby to open.");
}

void Action_PlaceSkull (void)
{
	if (player.skulls <= 0)
	{
		V_Print ("^rYou have no skull to place.");
		return;
	}

	int nx = player.x + player.face;
	int ny = player.y;

	if (! L_ValidSquare (nx, ny))
	{
		V_Print ("^rCannot place a skull there.");
		return;
	}

	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[nx][ny];

	if (T->mon != 0 || T->obj != 0 ||
		L_EnvIsBlocking (T->env) || (T->env & 0xF0) == EN_Special)
	{
		V_Print ("^rCannot place a skull there.");
		return;
	}

	T->obj   = OB_Skull;
	T->flag |= FL_DIRTY;

	V_Print ("You put down a skull.");

	player.skulls -= 1;
	player.acted   = true;
}

void Action_Read (void)
{
	// TODO Action_Read
}

void Action_Quaff (void)
{
	if (! L_PlayerHasObj (OB_Potion+3, true))
	{
		V_Print ("^rNo love potions!");
		return;
	}

	V_Print ("You quaff a love potion.");

	player.acted = true;

	int count = 0;
	int dir;

	for (dir = 1 ; dir <= 9 ; dir++)
	{
		if (dir == 5)
			continue;

		int nx = player.x + Dir_dx (dir);
		int ny = player.y + Dir_dy (dir);

		if (! L_ValidSquare (nx, ny))
			continue;

		struct Area * A = &world.areas[player.z];
		struct Tile * T = &A->grid[nx][ny];

		if ((T->flag & FL_FRIEND) != 0)
			continue;

		if (T->mon != 0)
		{
			T->flag |= FL_FRIEND | FL_DIRTY;
			count += 1;
		}
	}

	world.friends += count;
	world.enemies -= count;
}

void Action_Teleport (void)
{
	if (! L_PlayerHasObj (OB_Scroll, true))
	{
		V_Print ("^rNo teleport scrolls!");
		return;
	}

	player.acted = true;

	if (player.x == player.enter_x && player.y == player.enter_y)
	{
		V_Print ("You teleport... a few inches!");
		return;
	}

	// if a hostile enemy is at the destination, telefrag it
	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[player.enter_x][player.enter_y];

	bool did_msg = false;

	if (T->mon != 0)
	{
		if ((T->flag & FL_FRIEND) != 0 || L_MonIsNeutral (T->mon))
		{
			// friends and neutral creatures will swap places
		}
		else
		{
			V_Print ("You telefrag a %s!", L_MonName (T->mon));
			L_RemoveMonster  (T);
			did_msg = true;
		}
	}

	L_RawMovePlayer (player.enter_x, player.enter_y);

	if (! did_msg)
		V_Print ("You teleport...");
}

//----------------------------------------------------------------------

void G_SaveAndExit (int delay)
{
	if (world.state == GS_Victory)
	{
		V_Print ("^bQuitting game...");
		L_DeleteSaveGame ();
	}
	else
	{
		V_Print ("^bSaving game...");
		L_SaveGame ();
	}

	V_Refresh (true);
	B_Delay (delay);

	want_quit = true;
}

void G_DiscardAndQuit (void)
{
	if (world.state == GS_Victory ||
		D_ConfirmDialog (20, "Discard and Quit?", NULL))
	{
		V_Print ("^bDiscarding game...");
		L_DeleteSaveGame ();

		V_Refresh (true);
		B_Delay (1000);

		want_quit = true;
	}
}

void G_FreshGame (void)
{
	if (world.state == GS_Victory ||
		D_ConfirmDialog (32, "Begin fresh game?", "(the current game will be lost)"))
	{
		V_ClearMessages ();
		V_Print ("^bStarted a fresh game.");

		C_CreateWorld (world.seed + 1);
		L_SaveGame ();
	}
}

void G_ReplayGame (void)
{
	// no need to confirm when player is dead
	if (world.state != GS_Active ||
		D_ConfirmDialog (33, "Replay the game?", "(existing progress will be lost)"))
	{
		V_ClearMessages ();
		V_Print ("^bRestarted the game.");

		C_CreateWorld (world.seed);
		L_SaveGame ();
	}
}

void G_HelpMenu (void)
{
	char choice = D_HelpMenuDialog ();

	switch (choice)
	{
		case 'G':
			D_HelpGeneralInfo ();
			break;

		case 'K':
			D_HelpShowKeys ();
			break;

		case 'C':
			D_HelpCredits ();
			break;

		default:
			// user dismissed the menu
			break;
	}
}

void G_MainMenu (void)
{
	char choice = D_MainMenuDialog ();

	switch (choice)
	{
		case 'H':  // help
			G_HelpMenu ();
			break;

		case 'O':  // options
			// TODO: option screen
			break;

		case 'F':  // fresh game
			G_FreshGame ();
			break;

		case 'R':  // replay game
			G_ReplayGame ();
			break;

		case 'D':  // discard and exit
			G_DiscardAndQuit ();
			break;

		case 'Q':  // quit (and save)
			G_SaveAndExit (1000);
			break;

		default:
			// user dismissed the menu
			break;
	}
}

void G_KeyPress (int key)
{
	player.acted = false;

	switch (key)
	{
		/* general stuff (game state does not matter) */

		case EVENT_CLOSE_WINDOW:
			// keep the delay short when closing via the OS
			G_SaveAndExit (500);
			break;

		case KEY_PGUP:
			V_ScrollMessages (-4);
			break;

		case KEY_PGDN:
			V_ScrollMessages (+4);
			break;

		case KEY_HOME:
		case KEY_END:
			V_ScrollMessages (+999);
			break;

		case KEY_M:
		case KEY_ESC:
		case KEY_ENTER:
			G_MainMenu ();
			break;

		default:
			break;
	}

	// prevent making moves when game is over
	if (world.state != GS_Active)
		return;

	switch (key)
	{
		/* movement */

		case KEY_H:
		case KEY_LEFT:
			Action_Move (4);
			break;

		case KEY_J:
		case KEY_DOWN:
			Action_Move (2);
			break;

		case KEY_K:
		case KEY_UP:
			Action_Move (8);
			break;

		case KEY_L:
		case KEY_RIGHT:
			Action_Move (6);
			break;

		/* wands */

		case KEY_A:
			Action_Zap (0, "acid");
			break;

		case KEY_S:
			Action_Zap (1, "sleep");
			break;

		case KEY_D:
			Action_Zap (2, "death");
			break;

		case KEY_F:
			Action_Zap (3, "fire");
			break;

		/* other actions */

		case KEY_Z:
		case KEY_DOT:
		case KEY_SPACE:
			Action_Wait ();
			break;

		case KEY_C:
			Action_Climb ();
			break;

		case KEY_I:
			D_InventoryDialog ();
			break;

		case KEY_O:
			Action_Open ();
			break;

		case KEY_P:
			Action_PlaceSkull ();
			break;

		case KEY_Q:
			Action_Quaff ();
			break;

		case KEY_R:
			Action_Read ();
			break;

		case KEY_T:
			Action_Teleport ();
			break;

		default:
			break;
	}

	// if the player made a valid move, give monsters their turn
	if (player.acted && world.state == GS_Active)
	{
		G_WorldStep ();
	}
}

void G_RunGame (void)
{
	while (! want_quit)
	{
		int key = B_InputKey ();

		G_KeyPress (key);

		V_Refresh (key == NO_KEY);
	}
}
