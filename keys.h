// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.


//
// RawKey represents a physical key on the keyboard.
//
// Letters are the ASCII codes 'A' to 'Z' (uppercase).
// Digits  are the ASCII codes '0' to '9'.
// Symbols are their ASCII code, like '[', '/', etc...
// Space-bar is ' ', Enter is '\n', Tab is '\t', etc...
// Other keys use a value between 0x80 and 0xFF.
// Mouse buttons get mapped to KEY_BUTTON1..KEY_BUTTON9.
// Zero is never a valid key.
//
enum RawKey
{
	NO_KEY         = 0,

	KEY_SPACE      = ' ',
	KEY_BS         = '\b',
	KEY_ENTER      = '\n',
	KEY_TAB        = '\t',
	KEY_ESC        = '\033',

	KEY_0          = '0',
	KEY_1          = '1',
	KEY_2          = '2',
	KEY_3          = '3',
	KEY_4          = '4',
	KEY_5          = '5',
	KEY_6          = '6',
	KEY_7          = '7',
	KEY_8          = '8',
	KEY_9          = '9',

	KEY_A          = 'A',
	KEY_B          = 'B',
	KEY_C          = 'C',
	KEY_D          = 'D',
	KEY_E          = 'E',
	KEY_F          = 'F',
	KEY_G          = 'G',
	KEY_H          = 'H',
	KEY_I          = 'I',
	KEY_J          = 'J',
	KEY_K          = 'K',
	KEY_L          = 'L',
	KEY_M          = 'M',
	KEY_N          = 'N',
	KEY_O          = 'O',
	KEY_P          = 'P',
	KEY_Q          = 'Q',
	KEY_R          = 'R',
	KEY_S          = 'S',
	KEY_T          = 'T',
	KEY_U          = 'U',
	KEY_V          = 'V',
	KEY_W          = 'W',
	KEY_X          = 'X',
	KEY_Y          = 'Y',
	KEY_Z          = 'Z',

	KEY_TILDE      = '~',
	KEY_MINUS      = '-',
	KEY_PLUS       = '+',
	KEY_BACKSLASH  = '\\',
	KEY_OPENBRACE  = '[',
	KEY_CLOSEBRACE = ']',
	KEY_SEMICOLON  = ';',
	KEY_QUOTE      = '\'',
	KEY_COMMA      = ',',
	KEY_DOT        = '.',
	KEY_SLASH      = '/',
	KEY_STAR       = '*',

	KEY_INS        = 0x90,
	KEY_DEL        = 0x91,
	KEY_HOME       = 0x92,
	KEY_END        = 0x93,
	KEY_PGUP       = 0x94,
	KEY_PGDN       = 0x95,

	KEY_UP         = 0x9A,
	KEY_DOWN       = 0x9B,
	KEY_RIGHT      = 0x9C,
	KEY_LEFT       = 0x9D,

	KEY_PAUSE      = 0xE0,
	KEY_PRINT      = 0xE1,
	KEY_MENU       = 0xE2,
	KEY_HELP       = 0xE3,

	KEY_F1         = 0xF1,
	KEY_F2         = 0xF2,
	KEY_F3         = 0xF3,
	KEY_F4         = 0xF4,
	KEY_F5         = 0xF5,
	KEY_F6         = 0xF6,
	KEY_F7         = 0xF7,
	KEY_F8         = 0xF8,
	KEY_F9         = 0xF9,
	KEY_F10        = 0xFA,
	KEY_F11        = 0xFB,
	KEY_F12        = 0xFC,
};

enum ModifierMask
{
	SHIFT_MOD    = (1 << 0),
	CTRL_MOD     = (1 << 1),
	ALT_MOD      = (1 << 2),
	META_MOD     = (1 << 3),

	CAPSLOCK_MOD = (1 << 4),
	NUMLOCK_MOD  = (1 << 5),
	SCRLOCK_MOD  = (1 << 6),
};
