// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

bool B_Init      (void);
bool B_InitInput (void);
bool B_InitVideo (void);
void B_Shutdown  (void);

enum SpecialEvent
{
	EVENT_CLOSE_WINDOW = 0x101
};

// read a key from the keyboard.
// produces NO_KEY when none.
// may also produce a special event (`EVENT_XXX`).
int B_InputKey (void);

// delay for a short while
void B_Delay (int milliseconds);

// size of unscaled drawing area
extern int UNSCALED_W;
extern int UNSCALED_H;

// opaque types for images
struct Image;
struct SubImage;

void B_SetPalette (void);

void B_ClipRect (int x, int y, int w, int h);

struct Image * B_CreateImage (int w, int h);
struct Image * B_LoadImage (const char * name, int font_color);
void B_ImageSize (struct Image * img, int * w, int * h);

void B_Clear (struct Image * dest, int color);
void B_Fill  (struct Image * dest, int x, int y, int w, int h, int color);

struct SubImage * B_CreateSubImage (struct Image * base, int x, int y, int w, int h);

void B_DrawImage (struct Image * img, int x, int y, bool masked);
void B_DrawSubImage (struct SubImage * sub, int x, int y, struct Image * dest);

void B_Update (void);
