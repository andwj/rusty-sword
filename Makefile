#
#  --- RUSTY SWORD ---
#
#  Makefile for Linux and BSD systems.
#  Requires GNU make.
#

PROGRAM=rusty-sword

# CC=gcc
# CC=clang
# CC=x86_64-w64-mingw32-gcc

# set this to 1 to enable the Allegro 4.x back-end
BACKEND_ALLEGRO ?= 0

BASEFLAG=-std=c99
OPTIMISE=-O0 -g3
WARNINGS=-Wall -Wextra -Wno-unused-parameter

CFLAGS   ?= $(BASEFLAG) $(OPTIMISE) $(WARNINGS)
CPPFLAGS ?=
LDFLAGS  ?= $(OPTIMISE)
LIBS     ?=

OBJ_DIR=_build

DUMMY=$(OBJ_DIR)/__dummy


#----- Object files ----------------------------------------------

OBJS = \
	$(OBJ_DIR)/create.o   \
	$(OBJ_DIR)/dialog.o   \
	$(OBJ_DIR)/game.o     \
	$(OBJ_DIR)/level.o    \
	$(OBJ_DIR)/navigate.o \
	$(OBJ_DIR)/palette.o  \
	$(OBJ_DIR)/util.o     \
	$(OBJ_DIR)/video.o    \
	$(OBJ_DIR)/main.o

$(OBJ_DIR)/%.o: ./%.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<


#----- Libraries ----------------------------------------------

ifeq ($(strip $(BACKEND_ALLEGRO)),1)
# Allegro...
OBJS   += $(OBJ_DIR)/allegro.o
CFLAGS += $(shell allegro-config --cflags)
LIBS   += $(shell allegro-config --static)
else
# SDL2...
OBJS   += $(OBJ_DIR)/sdl.o
OBJS   += $(OBJ_DIR)/tga.o
CFLAGS += $(shell sdl2-config --cflags)
LIBS   += $(shell sdl2-config --libs)
endif

#----- Targets -----------------------------------------------

all: $(DUMMY) $(PROGRAM)

clean:
	rm -f $(PROGRAM) $(OBJ_DIR)/*.[oa]
	rm -f ERRS

$(PROGRAM): $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS) $(LIBS)

# this is used to create the OBJ_DIR directory
$(DUMMY):
	mkdir -p $(OBJ_DIR)
	@touch $@

.PHONY: all clean


#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
