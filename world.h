// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* player info */

#define START_HEALTH   500
#define START_DAMAGE   4

#define MAX_HEALTH     999
#define MAX_CHARGES    99

#define INVENT_SIZE    8

struct Player
{
	// location, z = area number
	int  x, y, z;

	// facing direction: -1 for left, +1 for right
	int  face;

	// health, zero means you're dead
	int  health;

	// these are zero for none, otherwise OB_Weapon (etc) + 0..3
	int  armor;
	int  shield;
	int  helmet;
	int  weapon;

	int  wands[4];
	int  skulls;

	// inventory (keys, rings, etc...)
	// zero is a free slot, otherwise the OB_XXX number
	int  inventory[INVENT_SIZE];

	// protection from hits, a percentage 0..100
	int  protection;

	// damage from melee weapon
	int  damage;

	// location that player entered the current area
	int  enter_x;
	int  enter_y;

	// player performed a valid action (moved, etc)
	bool  acted;

	// player is currently zapping a wand
	bool  zapping;
};

#define BEAM_DIST  8

enum
{
	WAND_Acid  = 0,
	WAND_Sleep = 1,
	WAND_Death = 2,
	WAND_Fire  = 3,
};

/* world defs */

struct Tile
{
	unsigned char  env;   // floors and walls
	unsigned char  obj;   // objects, decorations
	unsigned char  mon;   // monsters, friends

	unsigned char  id;    // door id, destination of stairs
	unsigned char  flag;  // monster state, dirty bit
	unsigned char  time;  // monster is sleeping if > 0

	unsigned short hp;    // health of the monster
};

#define MAX_AREAS   32
#define MAX_QUEUE   128

#define AREA_MAX_W  128
#define AREA_MAX_H  32


struct Area
{
	int  width;
	int  height;

	// tile to show at top-left corner of map view.
	// can be outside the map, e.g. negative X values.
	int  view_x;
	int  view_y;

	// contents of the area
	struct Tile  grid[AREA_MAX_W][AREA_MAX_H];
};

struct QueuedFriend
{
	unsigned char mon;
	unsigned char x;
	unsigned char y;
	unsigned char z;  // area number
};

enum GameState
{
	GS_None,      // no actual level yet
	GS_Active,    // a game is in progress
	GS_Victory,   // the game is over, the player won
	GS_Defeat,    // the game is over, the player died
};

struct World
{
	// random seed used to create the level
	int  seed;

	// one of the GS_XXX gamestate values
	int  state;

	// number of creatures in captivity
	int  captives;

	// number of hostile monsters
	int  enemies;

	// number of friends
	int  friends;

	// friends which have gone up a staircase, but not yet appeared
	// in the new area.
	struct QueuedFriend  queue[MAX_QUEUE];

	// all the areas
	int          total;
	struct Area  areas[MAX_AREAS];
};

extern struct World   world;
extern struct Player  player;

/* environment kinds */

enum EnvKind
{
	// the five rows
	EN_Floor   = 0x10,
	EN_Special = 0x20,
	EN_Wall    = 0x30,
	EN_Door    = 0x40,
	EN_Extra   = 0x50,

	// significant tiles
	EN_Down    = (EN_Special | 0),
	EN_Spikes  = (EN_Special | 1),
	EN_Up      = (EN_Special | 2),
	EN_Water   = (EN_Special | 4),
	EN_Lava    = (EN_Special | 5),
};

/* object kinds */

enum ObjectKind
{
	// general pick-ups
	OB_Armor  = 0x10,
	OB_Helmet = 0x20,
	OB_Shield = 0x30,
	OB_Weapon = 0x40,
	OB_Key    = 0x50,
	OB_Wand   = 0x60,
	OB_Potion = 0x70,
	OB_Ring   = 0x80,  // includes the amulets

	// decorations
	OB_Tree       = 0x95,
	OB_Mushroom   = 0x96,
	OB_Pillar     = 0x97,
	OB_Tombstone  = 0xA5,
	OB_Altar      = 0xA6,
	OB_Fountain   = 0xA7,
	OB_Banner     = 0xB5,
	OB_Sign       = 0xB6,
	OB_Mummy      = 0xB7,

	OB_Boulder    = 0xC5,
	OB_Bars       = 0xC6,
	OB_Orb        = 0xC7,
	OB_Chest      = 0xD5,
	OB_Skeleton   = 0xD7,
	OB_Bomb       = 0xE5,
	OB_Crystals   = 0xE6,
	OB_Checkpoint = 0xE7,
	OB_Paper      = 0xF4,
	OB_Scroll     = 0xF5,
	OB_Book       = 0xF6,
	OB_Skull      = 0xF7,
};

/* monster kinds */

enum MonsterKind
{
	// this is used to reserve the spot where the player is
	MO_Player = 1,

	// friendly or neutral
	MO_Rabbit      = 0x06,
	MO_Mouse       = 0x07,

	// hostile folk
	MO_Bat         = 0x10,
	MO_Spider      = 0x11,
	MO_Python      = 0x12,
	MO_Anaconda    = 0x13,
	MO_Scorpion    = 0x14,
	MO_Octopus     = 0x15,
	MO_Mammoth     = 0x16,
	MO_WereRabbit  = 0x17,

	MO_Slime       = 0x20,
	MO_Lizard      = 0x21,
	MO_Bug         = 0x22,
	MO_Centipede   = 0x23,
	MO_Toad        = 0x24,
	MO_GreenDragon = 0x25,
	MO_BlueDragon  = 0x26,
	MO_RedDragon   = 0x27,

	MO_Zombie      = 0x30,
	MO_Skeleton    = 0x31,
	MO_Goblin      = 0x32,
	MO_Cyclops     = 0x33,
	MO_WereLion    = 0x34,
	MO_IceGolem    = 0x35,
	MO_RedDevil    = 0x36,
	MO_BlackDevil  = 0x37,

	MO_Archer      = 0x40,
	MO_Knight      = 0x41,
	MO_Ghoul       = 0x42,
	MO_Vampire     = 0x43,
	MO_Mage        = 0x44,
	MO_Oculus      = 0x45,
};

/* monster movement */

enum MovementMode
{
	MOVE_Straight = 1,  // can move N/S/E/W
	MOVE_Diagonal = 2,  // can move NE/NW/SE/SW
	MOVE_8_Way    = 3,  // can move in all directions
};

enum TravelMethod
{
	TRAVEL_Normal = 0,  // can only move on solid ground
	TRAVEL_Swim   = 1,  // can swim in water too
	TRAVEL_Lava   = 2,  // can wade through lava
	TRAVEL_Fly    = 3,  // can fly over water/lava/pits
};

/* tile flags */

#define FL_GUARD   (1 << 1)   // mon is guarding a nearby object
#define FL_FRIEND  (1 << 2)   // mon is a friend of the player
#define FL_LEARNT  (1 << 3)   // mon has learnt your resistance
#define FL_MOVED   (1 << 4)   // mon has moved in current turn
#define FL_UNSEEN  (1 << 5)   // tile is in a closed/locked room
#define FL_DIRTY   (1 << 7)   // tile has changed recently

#define SLEEP_TIME  24

