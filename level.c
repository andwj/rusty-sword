// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "main.h"

struct World   world;
struct Player  player;


void L_ClearWorld (void)
{
	memset (&player, 0, sizeof(player));
	memset (&world,  0, sizeof(world));
}

const char * L_GetSaveFilename (void)
{
	return UT_JoinPath (opt.config_dir, "world.dat");
}

//----------------------------------------------------------------------

void L_Write8 (FILE * fp, int v)
{
	fputc (v & 255, fp);
}

void L_Write16 (FILE * fp, int v)
{
	L_Write8 (fp, v);
	L_Write8 (fp, v >> 8);
}

void L_Write32 (FILE * fp, int v)
{
	L_Write8 (fp, v);
	L_Write8 (fp, v >>  8);
	L_Write8 (fp, v >> 16);
	L_Write8 (fp, v >> 24);
}

void L_WritePlayer (FILE * fp)
{
	L_Write32 (fp, player.x);
	L_Write32 (fp, player.y);
	L_Write32 (fp, player.z);
	L_Write32 (fp, player.face);

	L_Write16 (fp, player.health);

	L_Write16 (fp, player.weapon);
	L_Write16 (fp, player.armor);
	L_Write16 (fp, player.helmet);
	L_Write16 (fp, player.shield);

	L_Write16 (fp, player.wands[0]);
	L_Write16 (fp, player.wands[1]);
	L_Write16 (fp, player.wands[2]);
	L_Write16 (fp, player.wands[3]);

	L_Write16 (fp, player.skulls);

	int i;
	for (i = 0 ; i < INVENT_SIZE ; i++)
	{
		L_Write16 (fp, player.inventory[i]);
	}

	L_Write16 (fp, player.enter_x);
	L_Write16 (fp, player.enter_y);

	// protection and damage do not need to be saved
}

void L_WriteTile (FILE * fp, struct Tile * T)
{
	// to save space, we only store fields which are non-zero.
	// the first byte contains a bitmask of these fields.

	int mask = 0;

	// don't store the dirty bit (etc)
	T->flag &= ~(FL_DIRTY | FL_MOVED);

	if (T->env  != 0) mask |= 0x01;
	if (T->obj  != 0) mask |= 0x02;
	if (T->mon  != 0) mask |= 0x04;

	if (T->id   != 0) mask |= 0x10;
	if (T->flag != 0) mask |= 0x20;
	if (T->time != 0) mask |= 0x40;
	if (T->hp   != 0) mask |= 0x80;

	L_Write8 (fp, mask);

	if (mask & 0x01) L_Write8 (fp, T->env);
	if (mask & 0x02) L_Write8 (fp, T->obj);
	if (mask & 0x04) L_Write8 (fp, T->mon);

	if (mask & 0x10) L_Write8 (fp, T->id);
	if (mask & 0x20) L_Write8 (fp, T->flag);
	if (mask & 0x40) L_Write8 (fp, T->time);

	if (mask & 0x80) L_Write16 (fp, T->hp);
}

void L_WriteArea (FILE * fp, struct Area * A)
{
	L_Write16 (fp, A->width);
	L_Write16 (fp, A->height);

	L_Write32 (fp, A->view_x);
	L_Write32 (fp, A->view_y);

	int x, y;
	for (y = 0 ; y < A->height ; y++)
	for (x = 0 ; x < A->width  ; x++)
	{
		L_WriteTile (fp, &A->grid[x][y]);
	}
}

void L_WriteWorld (FILE * fp)
{
	L_Write32 (fp, world.seed);

	L_Write32 (fp, world.state);
	L_Write32 (fp, world.captives);
	L_Write32 (fp, world.enemies);
	L_Write32 (fp, world.friends);

	L_Write32 (fp, world.total);

	int i;
	for (i = 0 ; i < MAX_QUEUE ; i++)
	{
		L_Write8 (fp, world.queue[i].mon);
		L_Write8 (fp, world.queue[i].x);
		L_Write8 (fp, world.queue[i].y);
		L_Write8 (fp, world.queue[i].z);
	}

	int a;
	for (a = 0 ; a < world.total ; a++)
	{
		L_WriteArea (fp, &world.areas[a]);
	}
}

bool L_SaveGame (void)
{
	// a blank world should never be saved
	Assert (world.total > 0);

	const char * path = L_GetSaveFilename ();

	FILE * fp = fopen (path, "wb");

	if (fp == NULL)
		return false;

	// write the magic marker
	fputc ('R',  fp);
	fputc ('X',  fp);
	fputc ('8',  fp);
	fputc (0x1A, fp);

	L_WritePlayer (fp);
	L_WriteWorld (fp);

	fflush (fp);
	fclose (fp);

	return true;
}

//----------------------------------------------------------------------

static bool read_error;

int L_Read8 (FILE * fp)
{
	// result is 0..255

	if (read_error)
		return 0;

	int ch = fgetc (fp);
	if (ch == EOF)
	{
		read_error = true;
		return 0;
	}

	return ch & 255;
}

int L_Read16 (FILE * fp)
{
	// result is 0..65535

	int a = L_Read8 (fp);
	int b = L_Read8 (fp);

	return (b << 8) | a;
}

int L_Read32 (FILE * fp)
{
	// result is -2147483648..2147483647

	int a = L_Read8 (fp);
	int b = L_Read8 (fp);
	int c = L_Read8 (fp);
	int d = L_Read8 (fp);

	if (d >= 128)
		d = d - 256;

	return (d << 24) | (c << 16) | (b << 8) | a;
}

bool L_ReadPlayer (FILE * fp)
{
	player.x    = L_Read32 (fp);
	player.y    = L_Read32 (fp);
	player.z    = L_Read32 (fp);
	player.face = L_Read32 (fp);

	player.health = L_Read16 (fp);

	player.weapon = L_Read16 (fp);
	player.armor  = L_Read16 (fp);
	player.helmet = L_Read16 (fp);
	player.shield = L_Read16 (fp);

	player.wands[0] = L_Read16 (fp);
	player.wands[1] = L_Read16 (fp);
	player.wands[2] = L_Read16 (fp);
	player.wands[3] = L_Read16 (fp);

	player.skulls = L_Read16 (fp);

	int i;
	for (i = 0 ; i < INVENT_SIZE ; i++)
	{
		player.inventory[i] = L_Read16 (fp);
	}

	player.enter_x = L_Read16 (fp);
	player.enter_y = L_Read16 (fp);

	L_RecomputePlayer ();

	return ! read_error;
}

void L_ReadTile (FILE * fp, struct Tile * T)
{
	// to save space, we only store fields which are non-zero.
	// the first byte contains a bitmask of these fields.

	int mask = L_Read8 (fp);

	if (mask & 0x01) T->env  = L_Read8 (fp);
	if (mask & 0x02) T->obj  = L_Read8 (fp);
	if (mask & 0x04) T->mon  = L_Read8 (fp);

	if (mask & 0x10) T->id   = L_Read8 (fp);
	if (mask & 0x20) T->flag = L_Read8 (fp);
	if (mask & 0x40) T->time = L_Read8 (fp);

	if (mask & 0x80) T->hp   = L_Read16 (fp);
}

bool L_ReadArea (FILE * fp, struct Area * A)
{
	A->width  = L_Read16 (fp);
	A->height = L_Read16 (fp);

	A->view_x = L_Read32 (fp);
	A->view_y = L_Read32 (fp);

	if (read_error)
		return false;

	if (A->width  > AREA_MAX_W) return false;
	if (A->height > AREA_MAX_H) return false;

	int x, y;
	for (y = 0 ; y < A->height ; y++)
	for (x = 0 ; x < A->width  ; x++)
	{
		L_ReadTile (fp, &A->grid[x][y]);

		if (read_error)
			return false;
	}

	return true;
}

bool L_ReadWorld (FILE * fp)
{
	world.seed = L_Read32 (fp);

	world.state    = L_Read32 (fp);
	world.captives = L_Read32 (fp);
	world.enemies  = L_Read32 (fp);
	world.friends  = L_Read32 (fp);

	world.total = L_Read32 (fp);

	if (read_error)
		return false;

	// a blank world should never be saved
	if (world.total == 0)
		return false;

	if (world.total > MAX_AREAS)
		return false;

	int i;
	for (i = 0 ; i < MAX_QUEUE ; i++)
	{
		world.queue[i].mon = L_Read8 (fp);
		world.queue[i].x   = L_Read8 (fp);
		world.queue[i].y   = L_Read8 (fp);
		world.queue[i].z   = L_Read8 (fp);
	}

	if (read_error)
		return false;

	int a;
	for (a = 0 ; a < world.total ; a++)
	{
		if (! L_ReadArea (fp, &world.areas[a]))
			return false;
	}

	return true;
}

bool L_HaveSaveGame (void)
{
	const char * path = L_GetSaveFilename ();

	FILE * fp = fopen (path, "rb");

	if (fp == NULL)
		return false;

	fclose (fp);

	return true;
}

void L_DeleteSaveGame (void)
{
	const char * path = L_GetSaveFilename ();

	UT_DeleteFile (path);
}

bool L_RestoreGame (void)
{
	// Note: only call this after checking that a valid savegame
	//       exists (by calling L_HaveSaveGame).

	// Note 2: if this fails, the world could be in a completely broken
	//         state, so caller should do a L_ClearWorld().

	const char * path = L_GetSaveFilename ();

	FILE * fp = fopen (path, "rb");

	// unlikely to fail (due to L_HaveSaveGame check), but it could happen...
	if (fp == NULL)
		return false;

	// validate the magic marker.
	// the main point of this marker is versioning of the file, to prevent
	// newer versions of the game from trying to load an old format.
	int a = fgetc (fp);
	int b = fgetc (fp);
	int c = fgetc (fp);
	int d = fgetc (fp);

	if (! (a == 'R' && b == 'X' && c == '8' && d == 0x1A))
	{
		fclose (fp);
		return false;
	}

	// clear everything
	L_ClearWorld ();

	read_error = false;

	if (! L_ReadPlayer (fp))
	{
		fclose (fp);
		return false;
	}

	if (! L_ReadWorld (fp))
	{
		fclose (fp);
		return false;
	}

	fclose (fp);
	return true;  // ok!
}

//----------------------------------------------------------------------

int L_ObjPriority (unsigned char obj)
{
	// keys are most important.
	// rings and amulets may be needed to cross water/lava.
	// skulls are least important

	if (obj == OB_Skull)
		return 1;

	switch (obj & 0xF0)
	{
		case OB_Key:    return 5000 + obj;
		case OB_Ring:   return 4000 + obj;
		case 0xF0:      return 3000 + obj;  // OB_Scroll (etc)
		case OB_Wand:   return 2000 + obj;
		case OB_Potion: return 1000 + obj;

		default: return obj;
	}
}

const char * L_ObjName (unsigned char obj)
{
	switch (obj)
	{
		/* gettable items */

		case OB_Armor+0: return "leather armor";
		case OB_Armor+1: return "mithral armor";
		case OB_Armor+2: return "bronze armor";
		case OB_Armor+3: return "steel armor";

		case OB_Helmet+0: return "a rusty helmet";
		case OB_Helmet+1: return "an iron helmet";
		case OB_Helmet+2: return "a horned helmet";
		case OB_Helmet+3: return "a steel helmet";

		case OB_Shield+0: return "a wooden shield";
		case OB_Shield+1: return "a studded shield";
		case OB_Shield+2: return "a plated shield";
		case OB_Shield+3: return "a battle shield";

		case OB_Weapon+0: return "a rusty sword";
		case OB_Weapon+1: return "a sabre";
		case OB_Weapon+2: return "a broadsword";
		case OB_Weapon+3: return "a battle-axe";

		case OB_Key+0: return "a rusty key";
		case OB_Key+1: return "an ivory key";
		case OB_Key+2: return "a silver key";
		case OB_Key+3: return "a gold key";

		case OB_Potion+0: return "a small potion";
		case OB_Potion+1: return "a medium potion";
		case OB_Potion+2: return "a large potion";
		case OB_Potion+3: return "a love potion";

		case OB_Wand+0: return "a wand of acid";
		case OB_Wand+1: return "a wand of sleep";
		case OB_Wand+2: return "a wand of death";
		case OB_Wand+3: return "a wand of fire";

		case OB_Ring+0: return "a ring of acid-resist";
		case OB_Ring+1: return "a ring of fire-resist";
		case OB_Ring+2: return "an amulet of flight";
		case OB_Ring+3: return "an amulet of power";

		case OB_Paper:  return "a scrap of paper";
		case OB_Scroll: return "a teleport scroll";
		case OB_Book:   return "a book of poems";
		case OB_Skull:  return "a skull";

		/* decorations */

		case OB_Tree      : return "a tree";
		case OB_Mushroom  : return "a mushroom";
		case OB_Pillar    : return "a pillar";
		case OB_Tombstone : return "a tombstone";
		case OB_Altar     : return "an altar";
		case OB_Fountain  : return "a fountain";
		case OB_Banner    : return "a banner";
		case OB_Sign      : return "a sign";
		case OB_Mummy     : return "a sarcophagus";

		case OB_Boulder   : return "a boulder";
		case OB_Bars      : return "metal bars";
		case OB_Orb       : return "a large orb";
		case OB_Chest     : return "a chest";
		case OB_Chest+1   : return "an open chest";
		case OB_Skeleton  : return "a skeleton";
		case OB_Bomb      : return "a bomb";
		case OB_Crystals  : return "crystals";

		default:
			return "something";
	}
}

const char * L_MonName (unsigned char mon)
{
	switch (mon)
	{
		case MO_Rabbit      : return "rabbit";
		case MO_Mouse       : return "mouse";

		case MO_Bug         : return "bug";
		case MO_Centipede   : return "centipede";
		case MO_Spider      : return "spider";
		case MO_Scorpion    : return "scorpion";

		case MO_Bat         : return "bat";
		case MO_Lizard      : return "lizard";
		case MO_Toad        : return "toad";
		case MO_Python      : return "python";
		case MO_Anaconda    : return "anaconda";
		case MO_Octopus     : return "octopus";
		case MO_Mammoth     : return "mammoth";

		case MO_Slime       : return "slime";
		case MO_WereRabbit  : return "wererabbit";
		case MO_WereLion    : return "werelion";
		case MO_GreenDragon : return "green dragon";
		case MO_BlueDragon  : return "blue dragon";
		case MO_RedDragon   : return "red dragon";
		case MO_Oculus      : return "oculus";

		case MO_Zombie      : return "zombie";
		case MO_Skeleton    : return "skeleton";
		case MO_Ghoul       : return "ghoul";
		case MO_Vampire     : return "vampire";

		case MO_Goblin      : return "goblin";
		case MO_Archer      : return "archer";
		case MO_Knight      : return "knight";
		case MO_Mage        : return "mage";
		case MO_Cyclops     : return "cyclops";
		case MO_IceGolem    : return "ice golem";
		case MO_RedDevil    : return "red devil";
		case MO_BlackDevil  : return "black devil";

		default:
			return "monster";
	}
}

const char * L_WandShortName (int wand)
{
	switch (wand)
	{
		case WAND_Acid:  return "acid";
		case WAND_Sleep: return "sleep";
		case WAND_Death: return "death";
		case WAND_Fire:  return "fire";
		default:         return "other";
	}
}

bool L_ValidSquare (int x, int y)
{
	const struct Area * A = &world.areas[player.z];

	if (x < 0 || x >= A->width)  return false;
	if (y < 0 || y >= A->height) return false;

	return true;
}

bool L_EnvIsBlocking (unsigned char env)
{
	if ((env & 0xF0) == EN_Wall)
		return true;

	if ((env & 0xF0) == EN_Door)
		return ((env & 1) == 0);

	// NOTE: monsters will avoid hazards like water and pits,
	//       but that is not handled here.

	return false;
}

bool L_ObjIsBlocking (unsigned char obj)
{
	if (obj <= 0x8F)
		return false;

	if (obj >= 0xF4)
		return false;

	return true;
}

int L_MonStartHealth (unsigned char mon)
{
	switch (mon)
	{
		case MO_Bug         : return 15;
		case MO_Centipede   : return 20;
		case MO_Spider      : return 10;
		case MO_Scorpion    : return 60;

		case MO_Bat         : return 20;
		case MO_Lizard      : return 40;
		case MO_Toad        : return 50;
		case MO_Python      : return 30;
		case MO_Anaconda    : return 45;
		case MO_Octopus     : return 80;
		case MO_Mammoth     : return 135;

		case MO_Slime       : return 5;
		case MO_WereRabbit  : return 40;
		case MO_WereLion    : return 115;
		case MO_GreenDragon : return 150;
		case MO_BlueDragon  : return 160;
		case MO_RedDragon   : return 170;
		case MO_Oculus      : return 50;

		case MO_Zombie      : return 30;
		case MO_Skeleton    : return 20;
		case MO_Ghoul       : return 70;
		case MO_Vampire     : return 225;

		case MO_Goblin      : return 25;
		case MO_Archer      : return 35;
		case MO_Knight      : return 100;
		case MO_Mage        : return 90;
		case MO_Cyclops     : return 100;
		case MO_IceGolem    : return 200;
		case MO_RedDevil    : return 300;
		case MO_BlackDevil  : return 400;

		default: return 1;
	}
}

int L_MonMeleeDamage (unsigned char mon)
{
	switch (mon)
	{
		case MO_Bug         : return 3;
		case MO_Centipede   : return 6;
		case MO_Spider      : return 20;
		case MO_Scorpion    : return 15;

		case MO_Bat         : return 3;
		case MO_Lizard      : return 5;
		case MO_Toad        : return 2;
		case MO_Python      : return 7;
		case MO_Anaconda    : return 9;
		case MO_Octopus     : return 24;
		case MO_Mammoth     : return 9;

		case MO_Slime       : return 12;
		case MO_WereRabbit  : return 18;
		case MO_WereLion    : return 11;
		case MO_GreenDragon : return 15;
		case MO_BlueDragon  : return 17;
		case MO_RedDragon   : return 19;
		case MO_Oculus      : return 5;

		case MO_Zombie      : return 5;
		case MO_Skeleton    : return 8;
		case MO_Ghoul       : return 14;
		case MO_Vampire     : return 25;

		case MO_Goblin      : return 12;
		case MO_Archer      : return 10;
		case MO_Knight      : return 14;
		case MO_Mage        : return 5;
		case MO_Cyclops     : return 17;
		case MO_IceGolem    : return 20;
		case MO_RedDevil    : return 30;
		case MO_BlackDevil  : return 40;

		default: return 1;
	}
}

bool L_MonCanFly (unsigned char mon)
{
	switch (mon)
	{
		case MO_Bat:
		case MO_Mage:
		case MO_GreenDragon:
		case MO_BlueDragon:
		case MO_RedDragon:
		case MO_Ghoul:
		case MO_Oculus:
			return true;

		default:
			return false;
	}
}

bool L_MonCanSwim (unsigned char mon)
{
	switch (mon)
	{
		case MO_Toad:
		case MO_Lizard:
		case MO_Octopus:
		case MO_IceGolem:
			return true;

		default:
			return false;
	}
}

bool L_MonIsNeutral (unsigned char mon)
{
	switch (mon)
	{
		case MO_Rabbit:
		case MO_Mouse:
			return true;

		default:
			return false;
	}
}

bool L_MonImmuneTo (unsigned char mon, int wand)
{
	switch (wand)
	{
		case WAND_Acid:
		{
			switch (mon)
			{
				case MO_Slime:
				case MO_Knight:
				case MO_BlueDragon:
				case MO_Ghoul:
				case MO_BlackDevil:
					return true;

				default:
					return false;
			}
		}

		case WAND_Fire:
		{
			switch (mon)
			{
				case MO_Scorpion:
				case MO_RedDragon:
				case MO_RedDevil:
				case MO_Ghoul:
				case MO_BlackDevil:
					return true;

				default:
					return false;
			}
		}

		case WAND_Sleep:
		{
			switch (mon)
			{
				case MO_WereRabbit:
				case MO_WereLion:
				case MO_Mage:
				case MO_Oculus:
				case MO_Ghoul:
				case MO_BlackDevil:
					return true;

				default:
					return false;
			}
		}

		case WAND_Death:
		{
			switch (mon)
			{
				case MO_Zombie:
				case MO_Skeleton:
				case MO_Vampire:
				case MO_Ghoul:
				case MO_BlackDevil:
					return true;

				default:
					return false;
			}
		}

		default:
			return false;
	}
}

bool L_MonHasLongRangeAttack (unsigned char mon)
{
	// Note: the green dragon lacks a a long-range attack (atm), but
	//       we pretend it does so it moves like the other dragons.

	switch (mon)
	{
		case MO_Archer:
		case MO_Knight:

		case MO_GreenDragon:
		case MO_BlueDragon:
		case MO_RedDragon:
			return true;

		default:
			return false;
	}
}

bool L_BlocksTravel (const struct Tile * T, enum TravelMethod travel)
{
	if (L_EnvIsBlocking (T->env))
		return true;

	// monsters can never occupy a square with an item
	if (T->obj != 0)
		return true;

	// flying monsters can pass over water, lava and spikes
	if (travel == TRAVEL_Fly)
		return false;

	switch (T->env)
	{
		case EN_Spikes:
			return true;

		case EN_Water:
			return (travel != TRAVEL_Swim);

		case EN_Lava:
			return (travel != TRAVEL_Lava);

		default:
			// okay!
			return false;
	}
}

bool L_MonCanVisit (const struct Tile * T, unsigned char mon)
{
	if (T->mon != 0)
		return false;

	return ! L_BlocksTravel (T, L_MonTravelMethod (mon));
}

bool L_MonBeamIsBlocked (int x, int y, int dx, int dy, int dist)
{
	struct Area * A = &world.areas[player.z];

	for (; dist >= 1 ; dist--)
	{
		x += dx;
		y += dy;

		Assert (L_ValidSquare (x, y));

		struct Tile * T = &A->grid[x][y];

		if (L_EnvIsBlocking (T->env) || L_ObjIsBlocking (T->obj))
			return true;
	}

	return false;
}

enum MovementMode L_MonMoveMode (unsigned char mon)
{
	// this returns a MOVE_XXX constant.

	// most monsters can move like the player: N/S/E/W.
	// a few monsters can move in all eight directions.
	// a few others can only move diagonally.

	switch (mon)
	{
		case MO_Bat:
		case MO_Python:
		case MO_Anaconda:
		case MO_Centipede:
			return MOVE_Diagonal;

		case MO_Spider:
		case MO_Scorpion:
		case MO_Octopus:

		case MO_GreenDragon:
		case MO_BlueDragon:
		case MO_RedDragon:
			return MOVE_8_Way;

		default:
			return MOVE_Straight;
	}
}

enum TravelMethod L_MonTravelMethod (unsigned char mon)
{
	if (L_MonCanFly (mon))
		return TRAVEL_Fly;

	if (L_MonCanSwim (mon))
		return TRAVEL_Swim;

	if (L_MonImmuneTo (mon, WAND_Fire))
		return TRAVEL_Lava;

	return TRAVEL_Normal;
}

void L_DirtyTile (int x, int y)
{
	world.areas[player.z].grid[x][y].flag |= FL_DIRTY;
}

void L_RemoveMonster (struct Tile * T)
{
	if ((T->flag & FL_FRIEND) != 0)
	{
		Assert (world.friends > 0);
		world.friends -= 1;
	}
	else if (! L_MonIsNeutral (T->mon))
	{
		Assert (world.enemies > 0);
		world.enemies -= 1;
	}

	T->mon  = 0;
	T->obj  = 0;
	T->hp   = 0;
	T->time = 0;
	T->flag = FL_DIRTY;
}

void L_RawMoveMonster (int old_x, int old_y, int new_x, int new_y)
{
	struct Area * A = &world.areas[player.z];

	struct Tile * O = &A->grid[old_x][old_y];
	struct Tile * N = &A->grid[new_x][new_y];

	struct Tile prev = *N;

	N->obj  = 0;  // game code relies on this!
	O->obj  = 0;  //

	N->mon  = O->mon;
	N->flag = O->flag | FL_DIRTY | FL_MOVED;
	N->time = O->time;
	N->hp   = O->hp;

	O->mon  = prev.mon;
	O->flag = prev.flag | FL_DIRTY;
	O->time = prev.time;
	O->hp   = prev.hp;
}

void L_RawMovePlayer (int new_x, int new_y)
{
	L_RawMoveMonster (player.x, player.y, new_x, new_y);

	player.x = new_x;
	player.y = new_y;

	V_UpdateViewPos ();
}

bool L_PlayerHasObj (unsigned char obj, bool remove_it)
{
	int i;
	for (i = 0 ; i < INVENT_SIZE ; i++)
	{
		if (player.inventory[i] == obj)
		{
			if (remove_it)
			{
				player.inventory[i] = 0;
				V_MarkDirty_Stats ();
			}
			return true;
		}
	}

	return false;
}

bool L_PlayerCanVisit (const struct Tile * T)
{
	// we ignore monsters here

	if (L_EnvIsBlocking (T->env))
		return false;

	if (L_ObjIsBlocking (T->obj))
		return false;

	return true;
}

void L_FindMatchingStair (struct Area * A, unsigned char old_area, int * out_x, int * out_y)
{
	int x, y;

	for (x = 0 ; x < A->width  ; x++)
	for (y = 0 ; y < A->height ; y++)
	{
		const struct Tile * T = &A->grid[x][y];

		if (T->env == EN_Down || T->env == EN_Up)
		{
			if (T->id == old_area)
			{
				(*out_x) = x;
				(*out_y) = y;
				return;
			}
		}
	}
}

void L_PlayerEnterArea (unsigned char new_area)
{
	unsigned char old_area = player.z;

	struct Area * A2 = &world.areas[new_area];

	// find the matching stair
	int new_x = 0;
	int new_y = 0;

	L_FindMatchingStair (A2, old_area, &new_x, &new_y);

	// place the player into new location
	player.x = new_x;
	player.y = new_y;
	player.z = new_area;

	player.enter_x = new_x;
	player.enter_y = new_y;

	struct Tile * T = &A2->grid[player.x][player.y];

	T->mon  = MO_Player;
	T->obj  = 0;
	T->flag = 0;
	T->time = 0;

	V_MarkDirty_World ();
}

void L_OpenMatchingDoors (unsigned char id, bool is_cage)
{
	struct Area * A = &world.areas[player.z];

	int x, y;

	for (x = 0 ; x < A->width  ; x++)
	for (y = 0 ; y < A->height ; y++)
	{
		struct Tile * T = &A->grid[x][y];

		if (T->id != id)
			continue;

		if ((T->env & 0xF1) == EN_Door)
		{
			T->env  |= 1;
			T->flag |= FL_DIRTY;
		}
		else if (T->obj == OB_Bars)
		{
			T->obj = 0;
			T->flag |= FL_DIRTY;
		}
		else if (is_cage && T->mon != 0)
		{
			// released monsters become friendly
			T->flag |= FL_FRIEND | FL_DIRTY;

			Assert (world.captives > 0);

			world.captives -= 1;
			world.friends  += 1;
		}
	}
}

void L_OpenDoor (int x, int y, bool is_cage)
{
	struct Area * A = &world.areas[player.z];
	struct Tile * T = &A->grid[x][y];

	// we assume all doors have a valid ID number
	L_OpenMatchingDoors (T->id, is_cage);
}

void L_RecomputePlayer (void)
{
	static const int  weapon_dmg[5] = { 5, 10, 15, 22, 30 };
	static const int  armor_prot[5] = { 0, 10, 20, 32, 45 };
	static const int shield_prot[5] = { 0,  5, 10, 15, 20 };
	static const int helmet_prot[5] = { 0,  2,  4,  7, 10 };

	int w = (player.weapon == 0) ? 0 : (1 + player.weapon % 4);
	int a = (player.armor  == 0) ? 0 : (1 + player.armor  % 4);
	int s = (player.shield == 0) ? 0 : (1 + player.shield % 4);
	int h = (player.helmet == 0) ? 0 : (1 + player.helmet % 4);

	player.damage     = weapon_dmg[w];
	player.protection = armor_prot[a] + helmet_prot[h] + shield_prot[s];

	// never underestimate the power of an amulet!
	if (L_PlayerHasObj (OB_Ring+3, false))
	{
		player.damage = player.damage * 3 / 2 + 5;
	}
}

//----------------------------------------------------------------------

void L_AddFriendToQueue (struct Tile * T)
{
	unsigned char old_area = player.z;
	unsigned char new_area = T->id;

	struct Area * A2 = &world.areas[new_area];

	int new_x = 0;
	int new_y = 0;

	L_FindMatchingStair (A2, old_area, &new_x, &new_y);

	int i;
	for (i = 0 ; i < MAX_QUEUE ; i++)
	{
		struct QueuedFriend * F = &world.queue[i];

		if (F->mon == 0)
		{
			F->mon = T->mon;
			F->x   = (unsigned char)new_x;
			F->y   = (unsigned char)new_y;
			F->z   = new_area;

			T->mon  = 0;
			T->obj  = 0;
			T->flag = FL_DIRTY;

			break;
		}
	}
}

//----------------------------------------------------------------------

const char * L_PlayerAttackWord (void)
{
	switch (player.weapon)
	{
		case OB_Weapon+3:  return "smite";
		case OB_Weapon+2:  return "stab";
		case OB_Weapon+1:  return "slash";
		case OB_Weapon+0:  return "cut";
		default: return "hit";
	}
}

const char * L_WandAttackWord (int wand)
{
	switch (wand)
	{
		case WAND_Acid:  return "melt";
		case WAND_Sleep: return "stun";
		case WAND_Death: return "kill";
		case WAND_Fire:  return "burn";
		default:         return "hit";
	}
}

const char * L_MonAttackWord (unsigned char mon)
{
	switch (mon)
	{
		case MO_Rabbit:
		case MO_Mouse:
			return "nibbles";

		case MO_Slime:
			return "scolds";

		case MO_Scorpion:
		case MO_Octopus:
			return "stings";

		case MO_Anaconda:
		case MO_Python:
			return "crushes";

		case MO_Mammoth:
			return "kicks";

		case MO_WereRabbit:
		case MO_WereLion:
			return "scratches";

		case MO_IceGolem:
			return "freezes";

		case MO_Ghoul:
			return "drains";

		case MO_Archer:
		case MO_Goblin:
		case MO_Cyclops:
			return "hits";

		case MO_Knight:
		case MO_Skeleton:
			return "cuts";

		case MO_Mage:
		case MO_Oculus:
			return "enchants";

		default:
			return "bites";
	}
}

//----------------------------------------------------------------------

bool L_VisFloodFill (void)
{
	int changes = 0;

	struct Area * A = &world.areas[player.z];

	int x, y;

	for (x = 0 ; x < A->width  ; x++)
	for (y = 0 ; y < A->height ; y++)
	{
		if ((A->grid[x][y].flag & FL_DIRTY) == 0)
			continue;

		int dir;
		for (dir = 2 ; dir <= 8 ; dir += 2)
		{
			int nx = x + Dir_dx (dir);
			int ny = y + Dir_dy (dir);

			if (! L_ValidSquare (nx, ny))
				continue;

			if ((A->grid[nx][ny].flag & FL_UNSEEN) == 0)
				continue;

			A->grid[nx][ny].flag &= ~FL_UNSEEN;

			changes += 1;

			// don't flood past a solid wall or closed door
			int env = A->grid[nx][ny].env;

			if ((env & 0xF0) == EN_Wall)
				continue;

			if ((env & 0xF0) == EN_Door && (env & 1) == 0)
				continue;

			A->grid[nx][ny].flag |= FL_DIRTY;
		}
	}

	return (changes > 0);
}

void L_MakeRoomVisible (int rx, int ry)
{
	// the coordinate must be inside the room (beyond a door which
	// was just opened).  if the room is marked unseen, then collect
	// all hidden tiles and mark them as visible now.

	struct Area * A = &world.areas[player.z];

	if ((A->grid[rx][ry].flag & FL_UNSEEN) == 0)
		return;

	// firstly, clear all FL_DIRTY bits
	int x, y;

	for (x = 0 ; x < A->width  ; x++)
	for (y = 0 ; y < A->height ; y++)
	{
		A->grid[x][y].flag &= ~FL_DIRTY;
	}

	// begin algorithm at the first square
	A->grid[rx][ry].flag &= ~FL_UNSEEN;
	A->grid[rx][ry].flag |=  FL_DIRTY;

	// perform flood-fill passes until finished
	while (L_VisFloodFill ())
	{ }

	// redraw all tiles
	V_MarkDirty_World ();
}
