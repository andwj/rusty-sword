#!/bin/sh
set -e

test -e scripts/djgpp_makefile || {
	echo "ERROR: this script must be run from the top level."
	exit 1
}

OUTPUT="_dosbox"
SUBDIR="rusty"

mkdir -p $OUTPUT/$SUBDIR
mkdir -p $OUTPUT/$SUBDIR/data
mkdir -p $OUTPUT/$SUBDIR/_build

rm -f $OUTPUT/$SUBDIR/_build/*.*

cp *.c *.h $OUTPUT/$SUBDIR
cp scripts/djgpp_makefile $OUTPUT/$SUBDIR/makefile
cp data/*.* $OUTPUT/$SUBDIR/data

dosbox -conf scripts/Build.conf
