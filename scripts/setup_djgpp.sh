#!/bin/sh
set -e

test -e scripts/djgpp_makefile || {
	echo "ERROR: this script must be run from the top level."
	exit 1
}

DL_SITE="http://www.delorie.com/pub/djgpp/current"
OUTPUT="_dosbox"

# rm -f -r $OUTPUT
mkdir -p $OUTPUT

cd $OUTPUT
mkdir -p tmp

download () {
	test -e tmp/$2 || wget "${DL_SITE}/$1/$2" -O tmp/$2
}
download "v2"  "copying.dj"
download "v2"  "djdev205.zip"
download "v2"  "readme.1st"

download "v2gnu"   "bnu2351b.zip"
download "v2gnu"   "gcc930b.zip"
download "v2gnu"   "mak44b.zip"
download "v2misc"  "csdpmi7b.zip"

download "v2tk/allegro"  "all422ar2.zip"
download "v2tk/allegro"  "all422br2.zip"

mkdir -p djgpp
cd       djgpp

unzip "../tmp/djdev205.zip"
unzip "../tmp/bnu2351b.zip"
unzip "../tmp/gcc930b.zip"
unzip "../tmp/mak44b.zip"
unzip "../tmp/csdpmi7b.zip"
unzip "../tmp/all422ar2.zip"
unzip "../tmp/all422br2.zip"
