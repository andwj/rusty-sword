// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "main.h"

#if defined(_WIN32) || defined(__DOS__)
#define PATH_SEPARATOR  '\\'
#define PATH_SEP_STR    "\\"
#else
#define PATH_SEPARATOR  '/'
#define PATH_SEP_STR    "/"
#endif

#ifndef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#endif

// this prototype seems to be absent or skipped, even though the
// function *does* exist in the DJGPP libc library.
#ifdef __DOS__
int mkdir (const char * _path, int _mode);
#endif


void * UT_Alloc (size_t size)
{
	void * p = malloc (size);
	if (p == NULL)
		Fatal_Error ("out of memory\n");
	return p;
}

bool UT_DeleteFile (const char * name)
{
#ifdef _WIN32
	return (DeleteFile (name) != 0);

#else // UNIX or MACOSX or DOS

	return (remove (name) == 0);
#endif
}

bool UT_MakeDir (const char * name)
{
#ifdef _WIN32
	return (CreateDirectory (name, NULL) != 0);

#else // UNIX or MACOSX or DOS

	return (mkdir (name, 0777) == 0);
#endif
}

int UT_Compare (const char * A, const char * B)
{
	for (;;)
	{
		int AC = tolower ((unsigned char) *A);
		int BC = tolower ((unsigned char) *B);

		if (AC != BC) { return AC - BC; }
		if (AC ==  0) { return 0;       }

		A++; B++;
	}
}

char * UT_Strdup (const char * s)
{
	size_t slen   = strlen (s);
	char * result = UT_Alloc (slen + 1);
	memcpy (result, s, slen + 1);
	return result;
}

char * UT_JoinPath (const char * dir, const char * rest)
{
	size_t len1   = strlen (dir);
	size_t len2   = strlen (rest);
	char * result = UT_Alloc (len1 + len2 + 2);

	memcpy (result, dir, len1);
	result[len1] = PATH_SEPARATOR;
	memcpy (result + len1 + 1, rest, len2 + 1);

	return result;
}

char * UT_GetDir (const char * path)
{
	int len = (int)strlen (path);

	if (len == 0)
		return ".";

	len--;

	while (len > 0)
	{
		char ch = path[len];

		if (ch == '/')
			break;

#if defined(_WIN32) || defined(__DOS__)
		if (ch == '\\')
			break;

		if (ch == ':')
			break;
#endif
		len--;
	}

	if (len == 0)
		return ".";

	char * result = UT_Alloc (len + 1);

	memcpy (result, path, (size_t)len);
	result[len] = 0;

	return result;
}

bool UT_MatchArg (const char * arg, char shortname, const char * longname)
{
	// a short option can only have a single `-` dash
	if (arg[1] != 0 && arg[2] == 0)
	{
		if (arg[1] == shortname)
			return true;
	}

	// a long option may have either one or two `-` dashes
	arg++;
	if (*arg == '-')
		arg++;

	if (longname != NULL)
	{
		if (strcmp (arg, longname) == 0)
			return true;
	}

	return false;
}

//----------------------------------------------------------------------

static unsigned int rand_state;

void Rand_Begin (int seed)
{
	rand_state = (unsigned int)seed & 0x7FFFFFFFul;

	// warm up the generator
	int i;
	for (i = 0 ; i < 100 ; i++)
		Rand_Value (100);
}

int Rand_Value (int modulo)
{
	Assert (modulo > 0);
	Assert (modulo <= 1000000);

	// a classic 32-bit LCG type PRNG
	rand_state = rand_state * 1103515245ul + 12345ul;

	// use the highest bits we can (the lowest ones are the least random)

	if (modulo <= 1024)
		return (int)(rand_state >> 20) % modulo;

	return (int)(rand_state >> 10) % modulo;
}

int Rand_Range (int low, int high)
{
	if (low >= high)
		return low;

	return low + Rand_Value (high - low + 1);
}

bool Rand_Odds (int percent)
{
	return Rand_Value (100) < percent;
}
