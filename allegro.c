// Copyright 2025 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* Allegro 4.x */

// this was originally needed for the DOS version (I was getting a
// large amount of duplicate symbol errors at link time), but it
// seems correct for Linux and Windows too.
#define AL_INLINE(type, name, args, code)  \
    static inline type name args;          \
    static inline type name args code

#define ALLEGRO_NO_KEY_DEFINES  1

#include "allegro.h"

#include "main.h"

//----------------------------------------------------------------------------
//  INPUT HANDLING
//----------------------------------------------------------------------------

// FIXME
extern volatile bool v_switched;
extern volatile bool v_want_close;

#define MAX_AL_KEY  93

// keyboard mapping table.
// this depends on the order of keys in Allegro 4.x.
// entry [0] is not used.
static const enum RawKey  raw_key_mapping[1+MAX_AL_KEY] =
{
	0,

	KEY_A, KEY_B, KEY_C, KEY_D, KEY_E, KEY_F,
	KEY_G, KEY_H, KEY_I, KEY_J, KEY_K, KEY_L,
	KEY_M, KEY_N, KEY_O, KEY_P, KEY_Q, KEY_R,
	KEY_S, KEY_T, KEY_U, KEY_V, KEY_W, KEY_X,
	KEY_Y, KEY_Z,

	KEY_0, KEY_1, KEY_2, KEY_3, KEY_4,
	KEY_5, KEY_6, KEY_7, KEY_8, KEY_9,

	/* numeric keypad 0..9 : mapped to the their function */

	KEY_INS,
	KEY_END,  KEY_DOWN,  KEY_PGDN,
	KEY_LEFT, KEY_SPACE, KEY_RIGHT,
	KEY_HOME, KEY_UP,    KEY_PGUP,

	/* function keys : 47 .. 58 */

	KEY_F1, KEY_F2,  KEY_F3,  KEY_F4,
	KEY_F5, KEY_F6,  KEY_F7,  KEY_F8,
	KEY_F9, KEY_F10, KEY_F11, KEY_F12,

	/* main area : 59 .. 75 */

	KEY_ESC,           // KEY_ESC
	KEY_TILDE,         // KEY_TILDE
	KEY_MINUS,         // KEY_MINUS
	KEY_PLUS,          // KEY_EQUALS
	KEY_BS,            // KEY_BACKSPACE
	KEY_TAB,           // KEY_TAB
	KEY_OPENBRACE,     // KEY_OPENBRACE
	KEY_CLOSEBRACE,    // KEY_CLOSEBRACE
	KEY_ENTER,         // KEY_ENTER
	KEY_SEMICOLON,     // KEY_COLON
	KEY_QUOTE,         // KEY_QUOTE
	KEY_BACKSLASH,     // KEY_BACKSLASH
	KEY_BACKSLASH,     // KEY_BACKSLASH2
	KEY_COMMA,         // KEY_COMMA
	KEY_DOT,           // KEY_STOP
	KEY_SLASH,         // KEY_SLASH
	KEY_SPACE,         // KEY_SPACE

	/* cursor keys : 76 .. 85 */

	KEY_INS,           // KEY_INSERT
	KEY_DEL,           // KEY_DEL
	KEY_HOME,          // KEY_HOME
	KEY_END,           // KEY_END
	KEY_PGUP,          // KEY_PGUP
	KEY_PGDN,          // KEY_PGDN
	KEY_LEFT,          // KEY_LEFT
	KEY_RIGHT,         // KEY_RIGHT
	KEY_UP,            // KEY_UP
	KEY_DOWN,          // KEY_DOWN

	/* miscellaneous : 86 .. 93 */

	KEY_SLASH,         // KEY_SLASH_PAD
	KEY_STAR,          // KEY_ASTERISK
	KEY_MINUS,         // KEY_MINUS_PAD
	KEY_PLUS,          // KEY_PLUS_PAD
	KEY_DEL,           // KEY_DEL_PAD
	KEY_ENTER,         // KEY_ENTER_PAD
	KEY_PRINT,         // KEY_PRTSCR
	KEY_PAUSE          // KEY_PAUSE
};

int B_InputKey (void)
{
	if (v_switched)
	{
		clear_keybuf ();
		return NO_KEY;
	}

	if (v_want_close)
	{
		clear_keybuf ();
		return EVENT_CLOSE_WINDOW;
	}

	if (! keypressed ())
		return NO_KEY;

	int key = readkey ();

	// an ALT key combo?
	if ((key & 255) == 0)
	{
		// ignore ALT + letter
		if (__allegro_KEY_A <= (key >> 8) && (key >> 8) <= __allegro_KEY_Z)
			return NO_KEY;
	}

	// a CTRL key combo?
	switch (key & 255)
	{
		case 3: /* CTRL-C */
			fprintf (stderr, "^C\n");
//FIXME			v_want_close = true;
			return EVENT_CLOSE_WINDOW;

		default:
			// handle other stuff normally (e.g. ESCAPE key)
			break;
	}

	// drop the ASCII, keep the code
	key = key >> 8;

	if (key < 1 || key > MAX_AL_KEY)
		return NO_KEY;

	return raw_key_mapping[key];
}

void B_Delay (int milliseconds)
{
	rest (milliseconds);
}

//----------------------------------------------------------------------

void CB_display_switch (void)
{
	v_switched = true;
}
END_OF_FUNCTION(CB_display_switch)


void CB_close_button (void)
{
	v_want_close = true;
}
END_OF_FUNCTION(CB_close_button)


bool B_InitInput (void)
{
	if (install_keyboard () != 0)
	{
		M_LogPrint ("failed to initialize keyboard.\n");
		return false;
	}

#ifndef __DOS__
	set_display_switch_callback (SWITCH_IN, CB_display_switch);
	set_close_button_callback   (CB_close_button);
#endif

	// disable key repeat (WISH: make this optional or controllable)
	set_keyboard_rate (0, 0);

	return true;
}

//----------------------------------------------------------------------------
//  GRAPHICS SET-UP
//----------------------------------------------------------------------------

int UNSCALED_W = 1024;  // FIXME: determine properly
int UNSCALED_H =  768;

void B_SetPalette (void)
{
	PALETTE pal;

	int i;
	for (i = 0 ; i < 256 ; i++)
	{
		// account for 6-bit precision by dividing by 4
		pal[i].r = rusty_palette[i*3 + 0] / 4;
		pal[i].g = rusty_palette[i*3 + 1] / 4;
		pal[i].b = rusty_palette[i*3 + 2] / 4;
	}

	set_palette (pal);
}

bool B_InitVideo (void)
{
	set_color_depth (8);

	if (opt.fullscreen || set_gfx_mode (GFX_AUTODETECT_WINDOWED, 1024, 768, 0, 0) != 0)
	{
		if (set_gfx_mode (GFX_AUTODETECT_FULLSCREEN, 1024, 768, 0, 0) != 0)
		{
			set_gfx_mode (GFX_TEXT, 0, 0, 0, 0);
			M_LogPrint ("unable to set graphics mode: %s\n", allegro_error);

			allegro_message ("Unable to set graphics mode!\nError: %s\n", allegro_error);
			return false;
		}
	}

	// no version number in title (it looks nicer without it)
	set_window_title ("Rusty Sword");

	// ensure that image loading does no conversion
	set_color_conversion (COLORCONV_NONE);

	return true;
}

void B_Update (void)
{
	// nothing needed
}

//----------------------------------------------------------------------------
//  IMAGES and DRAWING
//----------------------------------------------------------------------------

// NOTE:
//    struct Image is really a BITMAP
//    struct SubImage is really a RLE_SPRITE

#define AS_BITMAP(img)   ((BITMAP *) (img))
#define AS_IMAGE(bm)     ((struct Image *) (bm))

#define AS_RLE(img)        ((RLE_SPRITE *) (img))
#define AS_SUB_IMAGE(rle)  ((struct SubImage *) (rle))

struct Image * B_CreateImage (int w, int h)
{
	return AS_IMAGE (create_bitmap (w, h));
}

void B_Clear (struct Image * dest, int color)
{
	if (dest == NULL)
		clear_to_color (screen, color);
	else
		clear_to_color (AS_BITMAP (dest), color);
}

void B_Fill (struct Image * dest, int x, int y, int w, int h, int color)
{
	int x2 = x + w - 1;
	int y2 = y + h - 1;

	if (dest == NULL)
		rectfill (screen, x, y, x2, y2, color);
	else
		rectfill (AS_BITMAP (dest), x, y, x2, y2, color);
}

void B_ImageSize (struct Image * img, int * w, int * h)
{
	struct BITMAP * bm = AS_BITMAP (img);

	*w = bm->w;
	*h = bm->h;
}

void B_DrawImage (struct Image * img, int x, int y, bool masked)
{
	struct BITMAP * bm = AS_BITMAP (img);

	if (masked)
		masked_blit (bm, screen, 0, 0, x, y, bm->w, bm->h);
	else
		blit (bm, screen, 0, 0, x, y, bm->w, bm->h);
}

struct SubImage * B_CreateSubImage (struct Image * src, int x, int y, int w, int h)
{
	BITMAP * temp = create_sub_bitmap (AS_BITMAP (src), x, y, w, h);

	if (temp == NULL)
		return false;

	RLE_SPRITE * sprite = get_rle_sprite (temp);

	destroy_bitmap (temp);

	return AS_SUB_IMAGE (sprite);
}

void B_DrawSubImage (struct SubImage * sprite, int x, int y, struct Image * dest)
{
	if (dest == NULL)
		draw_rle_sprite (screen, AS_RLE (sprite), x, y);
	else
		draw_rle_sprite (AS_BITMAP (dest), AS_RLE (sprite), x, y);
}

void B_ClipRect (int x, int y, int w, int h)
{
	set_clip_rect (screen, x, y, x + w-1, y + h-1);
}

struct Image * B_LoadImage (const char * name, int font_color)
{
	char * fullname = UT_JoinPath (opt.data_dir, name);

	PALETTE  pal;
	BITMAP * bmp = load_tga (fullname, pal);

	if (bmp == NULL)
	{
		M_LogPrint ("failed to load image: %s\n", name);
		return NULL;
	}

	free (fullname);

	if (bitmap_color_depth (bmp) != 8)
	{
		M_LogPrint ("image is not 8-bit: %s\n", name);
		destroy_bitmap (bmp);
		return NULL;
	}

	// mapping from image palette to global palette
	pixel_t mapping[256];

	int i;
	for (i = 0 ; i < 256 ; i++)
	{
		int r = pal[i].r << 2;
		int g = pal[i].g << 2;
		int b = pal[i].b << 2;

		mapping[i] = (pixel_t) V_MapColor (r, g, b, font_color);
	}

	// update the image
	int y;
	for (y = 0 ; y < bmp->h ; y++)
	{
		unsigned char * pos = bmp->line[y];
		unsigned char * end = pos + (size_t)bmp->w;

		for (; pos < end ; pos++)
		{
			*pos = mapping[*pos];
		}
	}

	return AS_IMAGE (bmp);
}

//----------------------------------------------------------------------------
//  MAIN PROGRAM
//----------------------------------------------------------------------------

bool B_Init (void)
{
	if (allegro_init () != 0)
	{
		M_LogPrint ("failed to initialize allegro.\n");
		return false;
	}

	install_timer ();

	M_LogPrint ("allegro initialized ok.\n");
	return true;
}

void B_Shutdown (void)
{
	// TODO
}

int main (int argc, char * argv[])
{
	M_Main (argc, argv);
	return 0;
}
END_OF_MAIN()
